package com.hpe.usps.solavi.helpers.modules;

import com.hpe.usps.solavi.BaseWatchApplication;
import com.hpe.usps.solavi.MainActivity;
import com.hpe.usps.solavi.helpers.AndroidGeoHelper;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andy Jones
 */
@Module(
        injects = {
                MainActivity.class
        },
        library = true,
        complete = false
)
public class GeoHelperModule {

    private BaseWatchApplication mApp;

    public GeoHelperModule(BaseWatchApplication app) {
        mApp = app;
    }

    @Provides
    @Named("geoHelper")
    GeoHelper providesGeoHelper() {
        return new AndroidGeoHelper(mApp);
    }
}
