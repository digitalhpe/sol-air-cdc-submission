package com.hpe.usps.solavi.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;

/**
 * Created by Andy Jones
 */
public class AndroidPreferenceHelper implements PreferenceHelper {

    private static final String PREF_GCM_PUSH_TOKEN = "PREF_GCM_PUSH_TOKEN";
    private Context mContext;

    public AndroidPreferenceHelper(Context context) {
        mContext = context;
    }

    /**
     * Helper method for getting the GCM push token from shared preferences
     * @return GCM push token
     */
    @Override
    public String getPushToken() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        return sharedPreferences.getString(PREF_GCM_PUSH_TOKEN, "");
    }

    /**
     * Helper method for storing the GCM push token within shared preferences
     * @param pushToken GCM push token
     */
    @Override
    public void setPushToken(String pushToken) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_GCM_PUSH_TOKEN, pushToken);
        editor.apply();
    }
}
