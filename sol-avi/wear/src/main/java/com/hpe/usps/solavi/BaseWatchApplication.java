package com.hpe.usps.solavi;

import android.app.Application;

import com.hpe.usps.solavi.helpers.modules.GeoHelperModule;
import com.hpe.usps.solavi.helpers.modules.PreferenceHelperModule;
import com.hpe.usps.solavi.helpers.modules.RestServiceModule;

/**
 * Created by chishobr
 */
public class BaseWatchApplication extends Application {

    public static final String AQI_WIDTH = "AQI_WIDTH";
    public static final String UV_WIDTH = "UV_WIDTH";
    private static BaseWatchApplication instance;

    public BaseWatchApplication() {
        instance = this;
    }

    public void onCreate() {
        super.onCreate();

        WatchInjector.init(new RestServiceModule(), new GeoHelperModule(this), new PreferenceHelperModule(this));
    }

    public static BaseWatchApplication getInstance() {
        return instance;
    }


}
