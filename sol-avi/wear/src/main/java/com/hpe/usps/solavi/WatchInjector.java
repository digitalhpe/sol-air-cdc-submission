package com.hpe.usps.solavi;

/**
 * Created by chishobr
 */
import dagger.ObjectGraph;

public final class WatchInjector {
    private static ObjectGraph objectGraph = null;

    private WatchInjector() {

    }

    public static synchronized void init(Object... rootModules) {
        objectGraph = objectGraph == null ? ObjectGraph.create(rootModules) : objectGraph.plus(rootModules);
    }

    public static void inject(final Object target) {
        objectGraph.inject(target);
    }

    public static void add(Object... object) {
        objectGraph = ObjectGraph.create(object);
    }
}
