package com.hpe.usps.solavi.helpers;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;


import com.hpe.usps.solavi.R;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Func0;

/**
 * Helper class for Android geolocation functions.
 */
public class AndroidGeoHelper implements GeoHelper {

    private static final int MAX_LOCATION_RESULTS = 1;
    private Context mContext;
    Map<String, String> states;

    public AndroidGeoHelper(Context context) {
        mContext = context;
        initStateMap();
    }

    private void initStateMap() {
        if (states == null){
            states = new HashMap<String, String>();
        }
        states.clear();
        states.put("Alabama","AL");
        states.put("Alaska","AK");
        states.put("Alberta","AB");
        states.put("American Samoa","AS");
        states.put("Arizona","AZ");
        states.put("Arkansas","AR");
        states.put("Armed Forces (AE)","AE");
        states.put("Armed Forces Americas","AA");
        states.put("Armed Forces Pacific","AP");
        states.put("British Columbia","BC");
        states.put("California","CA");
        states.put("Colorado","CO");
        states.put("Connecticut","CT");
        states.put("Delaware","DE");
        states.put("District Of Columbia","DC");
        states.put("Florida","FL");
        states.put("Georgia","GA");
        states.put("Guam","GU");
        states.put("Hawaii","HI");
        states.put("Idaho","ID");
        states.put("Illinois","IL");
        states.put("Indiana","IN");
        states.put("Iowa","IA");
        states.put("Kansas","KS");
        states.put("Kentucky","KY");
        states.put("Louisiana","LA");
        states.put("Maine","ME");
        states.put("Manitoba","MB");
        states.put("Maryland","MD");
        states.put("Massachusetts","MA");
        states.put("Michigan","MI");
        states.put("Minnesota","MN");
        states.put("Mississippi","MS");
        states.put("Missouri","MO");
        states.put("Montana","MT");
        states.put("Nebraska","NE");
        states.put("Nevada","NV");
        states.put("New Brunswick","NB");
        states.put("New Hampshire","NH");
        states.put("New Jersey","NJ");
        states.put("New Mexico","NM");
        states.put("New York","NY");
        states.put("Newfoundland","NF");
        states.put("North Carolina","NC");
        states.put("North Dakota","ND");
        states.put("Northwest Territories","NT");
        states.put("Nova Scotia","NS");
        states.put("Nunavut","NU");
        states.put("Ohio","OH");
        states.put("Oklahoma","OK");
        states.put("Ontario","ON");
        states.put("Oregon","OR");
        states.put("Pennsylvania","PA");
        states.put("Prince Edward Island","PE");
        states.put("Puerto Rico","PR");
        states.put("Quebec","PQ");
        states.put("Rhode Island","RI");
        states.put("Saskatchewan","SK");
        states.put("South Carolina","SC");
        states.put("South Dakota","SD");
        states.put("Tennessee","TN");
        states.put("Texas","TX");
        states.put("Utah","UT");
        states.put("Vermont","VT");
        states.put("Virgin Islands","VI");
        states.put("Virginia","VA");
        states.put("Washington","WA");
        states.put("West Virginia","WV");
        states.put("Wisconsin","WI");
        states.put("Wyoming","WY");
        states.put("Yukon Territory","YT");
    }

    /**
     * Blocking method which returns a zip code for a given lat / long
     * @param latitude
     * @param longitude
     * @return zip code
     */
    private String getZipFromLatLongBlocking(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(mContext);

        String zipCode = "";

        List<Address> addressList;

        try {
            addressList = geocoder.getFromLocation(latitude, longitude, MAX_LOCATION_RESULTS);

            if (!addressList.isEmpty()) {
                zipCode = addressList.get(0).getPostalCode();
            }
        } catch (IOException e) {
            Log.e(this.getClass().getName(), e.getMessage(), e);
        }

        return zipCode;
    }

    private String getLocationAddressBlocking(Double latitude, Double longitude, boolean justLocation) {
        final Geocoder geocoder = new Geocoder(mContext);
        String result = mContext.getResources().getString(R.string.you_are_here);

        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                String addressLine = address.getAddressLine(Math.max(address.getMaxAddressLineIndex()-1,0));
                if (addressLine != null && !addressLine.isEmpty() && addressLine.lastIndexOf(' ') >= 0){
                    int lastSpace = addressLine.lastIndexOf(' ');
                    addressLine = addressLine.substring(0,lastSpace>0?lastSpace:0);
                }
                if (justLocation){
                    return addressLine;
                }
                return result + " - " + addressLine;

            }
        } catch (IOException e) {
            Log.e(this.getClass().getName(), e.getMessage(), e);
            Toast.makeText(mContext, R.string.error_finding_address, Toast.LENGTH_LONG).show();
        }
        return result;
    }

    /**
     * This method wraps the getZipFromLatLongBlocking method using rxjava and returns an observable
     * @param latitude
     * @param longitude
     * @return Observable containing the zip code
     */
    @Override
    public Observable<String> getZipFromLatLong(final double latitude, final double longitude) {
        return Observable.defer(new Func0<Observable<String>>() {
            @Override
            public Observable<String> call() {
                return Observable.just(getZipFromLatLongBlocking(latitude, longitude));
            }
        });
    }

    @Override
    public Observable<String> getLocationAddress(final double latitude, final double longitude, final boolean justLocation) {
        return Observable.defer(new Func0<Observable<String>>() {
            @Override
            public Observable<String> call() {
                return Observable.just(getLocationAddressBlocking(latitude, longitude, justLocation));
            }
        });
    }

    @Override
    public String getStateAbbv(String stateString){
        return states.get(stateString);
    }

    public boolean hasInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

    }
}
