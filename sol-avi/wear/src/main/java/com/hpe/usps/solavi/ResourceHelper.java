package com.hpe.usps.solavi;

/**
 * A helper class to easily return index values instead of full strings
 * Created by chishobr
 */
public class ResourceHelper {

    private ResourceHelper() {
    }

    @java.lang.SuppressWarnings("squid:MethodCyclomaticComplexity")
    /**
     * @param airQualityIndex
     * @return The Appropriate AQI color by resource index ID based on a given Air Quality Index value
     */
    public static int getAqiColorIdByIndex(int airQualityIndex) {
        int result = R.color.aqi_magenta;
        if (airQualityIndex <= 50){
            result = R.color.aqi_green;
        }
        else if (airQualityIndex <= 100){
            result = R.color.aqi_yellow;
        }
        else if (airQualityIndex <= 150){
            result = R.color.aqi_orange;
        }
        else if (airQualityIndex <= 200){
            result = R.color.aqi_red;
        }
        else if (airQualityIndex <= 300){
            result = R.color.aqi_purple;
        }
        return result;
    }

    @java.lang.SuppressWarnings("squid:MethodCyclomaticComplexity")
    /**
     * @param airQualityIndex
     * @return The Appropriate AQI pastel color by resource index ID based on a given Air Quality Index value
     */
    public static int getAqiPastelColorIdByIndex(int airQualityIndex) {
        int result = R.color.pastel_aqi_magenta;
        if (airQualityIndex <= 50){
            result = R.color.pastel_aqi_green;
        }
        else if (airQualityIndex <= 100){
            result = R.color.pastel_aqi_yellow;
        }
        else if (airQualityIndex <= 150){
            result = R.color.pastel_aqi_orange;
        }
        else if (airQualityIndex <= 200){
            result = R.color.pastel_aqi_red;
        }
        else if (airQualityIndex <= 300){
            result = R.color.pastel_aqi_purple;
        }
        return result;
    }

    @java.lang.SuppressWarnings("squid:MethodCyclomaticComplexity")
    /**
     * @param uvIndex
     * @return The Appropriate UV color by resource index ID based on a given UV Index value
     */
    public static int getUVColorIdByIndex(int uvIndex) {
        int result;
        switch(uvIndex){
            case 1:
            case 2:
                result =  R.color.uv_green;
                break;
            case 3:
            case 4:
            case 5:
                result =  R.color.uv_yellow;
                break;
            case 6:
            case 7:
                result =  R.color.uv_orange;
                break;
            case 8:
            case 9:
            case 10:
                result =  R.color.uv_red;
                break;
            case 11:
            default:
                result =  R.color.uv_purple;
        }
        return result;
    }

    @java.lang.SuppressWarnings("squid:MethodCyclomaticComplexity")
    /**
     * @param uvIndex
     * @return The Appropriate UV pastel color by resource index ID based on a given UV Index value
     */
    public static int getUVPastelColorIdByIndex(int uvIndex) {
        int result;
        switch(uvIndex){
            case 1:
            case 2:
                result =  R.color.pastel_uv_green;
                break;
            case 3:
            case 4:
            case 5:
                result =  R.color.pastel_uv_yellow;
                break;
            case 6:
            case 7:
                result =  R.color.pastel_uv_orange;
                break;
            case 8:
            case 9:
            case 10:
                result =  R.color.pastel_uv_red;
                break;
            case 11:
            default:
                result =  R.color.pastel_uv_purple;
        }
        return result;
    }


    @java.lang.SuppressWarnings("squid:MethodCyclomaticComplexity")
    /**
     * @param uvIndex
     * @return The Appropriate UV pastel color by resource index ID based on a given UV Index value
     */
    public static int getUVRatingIdByIndex(int uvIndex) {
        int result;
        switch(uvIndex){
            case 1:
            case 2:
                result =  R.drawable.low;
                break;
            case 3:
            case 4:
            case 5:
                result =  R.drawable.moderate;
                break;
            case 6:
            case 7:
                result =  R.drawable.high;
                break;
            case 8:
            case 9:
            case 10:
                result =  R.drawable.very_high;
                break;
            case 11:
            default:
                result =  R.drawable.extreme;
        }
        return result;
    }

    @java.lang.SuppressWarnings("squid:MethodCyclomaticComplexity")
    /**
     * @param airQualityIndex
     * @return The Appropriate AQI pastel color by resource index ID based on a given Air Quality Index value
     */
    public static int getAQIRatingIdByIndex(int airQualityIndex) {
        int result = R.drawable.hazardous;
        if (airQualityIndex <= 50){
            result = R.drawable.good;
        }
        else if (airQualityIndex <= 100){
            result = R.drawable.moderate;
        }
        else if (airQualityIndex <= 150){
            result = R.drawable.unhealthy_for_sensitive_groups;
        }
        else if (airQualityIndex <= 200){
            result = R.drawable.unhealthy;
        }
        else if (airQualityIndex <= 300){
            result = R.drawable.very_unhealthy;
        }
        return result;
    }
}
