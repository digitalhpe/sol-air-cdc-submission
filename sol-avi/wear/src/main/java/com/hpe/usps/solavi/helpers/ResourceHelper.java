package com.hpe.usps.solavi.helpers;

import com.hpe.usps.solavi.R;

/**
 * A helper class to easily return index values instead of full strings
 * Created by chishobr
 */
public class ResourceHelper {

    private ResourceHelper() {

    }

    public static int getAqiPlacement( int epaValue) {
        if (epaValue < 0){
            return -1;
        }

        if (epaValue <= 50){
            return 0;
        }

        if (epaValue <= 100){
            return 1;
        }

        if (epaValue <= 150){
            return 2;
        }

        if (epaValue <= 200){
            return 3;
        }

        if (epaValue <= 300){
            return 4;
        }
        return 5;
    }





    /**
     * @param airQualityIndex
     * @return The Appropriate AQI color by resource index ID based on a given Air Quality Index value
     */
    public static int getAqiColorIdByIndex(int airQualityIndex) {
        if (airQualityIndex <= 50){
            return R.color.aqi_green;
        }

        if (airQualityIndex <= 100){
            return R.color.aqi_yellow;
        }

        if (airQualityIndex <= 150){
            return R.color.aqi_orange;
        }

        if (airQualityIndex <= 200){
            return R.color.aqi_red;
        }

        if (airQualityIndex <= 300){
            return R.color.aqi_purple;
        }

        return R.color.aqi_magenta;
    }

    /**
     * @param airQualityIndex
     * @return The Appropriate AQI pastel color by resource index ID based on a given Air Quality Index value
     */
    public static int getAqiPastelColorIdByIndex(int airQualityIndex) {
        if (airQualityIndex <= 50){
            return R.color.pastel_aqi_green;
        }

        if (airQualityIndex <= 100){
            return R.color.pastel_aqi_yellow;
        }

        if (airQualityIndex <= 150){
            return R.color.pastel_aqi_orange;
        }

        if (airQualityIndex <= 200){
            return R.color.pastel_aqi_red;
        }

        if (airQualityIndex <= 300){
            return R.color.pastel_aqi_purple;
        }

        return R.color.pastel_aqi_magenta;
    }

    /**
     * @param uvIndex
     * @return The Appropriate UV color by resource index ID based on a given UV Index value
     */
    public static int getUVColorIdByIndex(int uvIndex) {

        int uvColorId;

        switch(uvIndex){
            case 0:
            case 1:
            case 2:
                uvColorId = R.color.uv_green;
                break;
            case 3:
            case 4:
            case 5:
                uvColorId = R.color.uv_yellow;
                break;
            case 6:
            case 7:
                uvColorId = R.color.uv_orange;
                break;
            case 8:
            case 9:
            case 10:
                uvColorId = R.color.uv_red;
                break;
            case 11:
                uvColorId = R.color.uv_purple;
                break;
            default:
                uvColorId = R.color.uv_purple;
        }

        return uvColorId;
    }

    /**
     * @param uvIndex
     * @return The Appropriate UV pastel color by resource index ID based on a given UV Index value
     */
    public static int getUVPastelColorIdByIndex(int uvIndex) {

        int uvColorId;

        switch(uvIndex){
            case 0:
            case 1:
            case 2:
                uvColorId = R.color.pastel_uv_green;
                break;
            case 3:
            case 4:
            case 5:
                uvColorId = R.color.pastel_uv_yellow;
                break;
            case 6:
            case 7:
                uvColorId = R.color.pastel_uv_orange;
                break;
            case 8:
            case 9:
            case 10:
                uvColorId = R.color.pastel_uv_red;
                break;
            case 11:
                uvColorId = R.color.pastel_uv_purple;
                break;
            default:
                uvColorId = R.color.pastel_disabled;
        }

        return uvColorId;
    }

    public static int getAqiPastelColorIdByPosition(int i) {
        switch(i){
            case 0:
                return R.color.pastel_aqi_green;
            case 1:
                return R.color.pastel_aqi_yellow;
            case 2:
                return R.color.pastel_aqi_orange;
            case 3:
                return R.color.pastel_aqi_red;
            case 4:
                return R.color.pastel_aqi_purple;
            case 5:
                return R.color.pastel_aqi_magenta;
            default:
                return R.color.pastel_disabled;
        }
    }

    public static int getAqiColorIdByPosition(int i) {
        switch(i){
            case 0:
                return R.color.aqi_green;
            case 1:
                return R.color.aqi_yellow;
            case 2:
                return R.color.aqi_orange;
            case 3:
                return R.color.aqi_red;
            case 4:
                return R.color.aqi_purple;
            case 5:
                return R.color.aqi_magenta;
            default:
                return R.color.aqi_green;
        }
    }

    public static int getAqiPastelDarkColorIdByPosition(int i) {
        switch(i){
            case 0:
                return R.color.pastel_aqi_dark_green;
            case 1:
                return R.color.pastel_aqi_dark_yellow;
            case 2:
                return R.color.pastel_aqi_dark_orange;
            case 3:
                return R.color.pastel_aqi_dark_red;
            case 4:
                return R.color.pastel_aqi_dark_purple;
            case 5:
                return R.color.pastel_aqi_dark_magenta;
            default:
                return R.color.pastel_dark_disabled;
        }
    }

    public static int getUVDarkPastelColorIdByIndex(int uvIndex) {
        int uvColorId;

        switch(uvIndex){
            case 0:
            case 1:
            case 2:
                uvColorId = R.color.pastel_uv_dark_green;
                break;
            case 3:
            case 4:
            case 5:
                uvColorId = R.color.pastel_uv_dark_yellow;
                break;
            case 6:
            case 7:
                uvColorId = R.color.pastel_uv_dark_orange;
                break;
            case 8:
            case 9:
            case 10:
                uvColorId = R.color.pastel_uv_dark_red;
                break;
            case 11:
                uvColorId = R.color.pastel_uv_dark_purple;
                break;
            default:
                uvColorId = R.color.pastel_dark_disabled;
        }

        return uvColorId;
    }

    public static int getUVPosition(int uvIndex) {
        switch(uvIndex) {
            case 0:
            case 1:
            case 2:
                return 0;
            case 3:
            case 4:
            case 5:
                return 1;
            case 6:
            case 7:
                return 2;
            case 8:
            case 9:
            case 10:
                return 3;
            case 11:
                return 4;
            default:
                return -1;
        }
    }

    public static int getAQIPosition(int aqiIndex) {
        if (aqiIndex <= 50){
            return 0;
        }

        if (aqiIndex <= 100){
            return 1;
        }

        if (aqiIndex <= 150){
            return 2;
        }

        if (aqiIndex <= 200){
            return 3;
        }

        if (aqiIndex <= 300){
            return 4;
        }

        if(aqiIndex <=500){
            return 5;
        }

        return -1;
    }

}
