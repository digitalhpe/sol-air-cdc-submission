package com.hpe.usps.solavi.helpers.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hpe.usps.solavi.MainActivity;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.SolaviService;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Andy Jones
 *
 * This module contains retrofit rest services to be injected via dagger
 */
@Module(
        injects = {
                MainActivity.class
        },
        library = true,
        complete = false
)
public class RestServiceModule {

    private static final String SOLAVI_SERVICE_ENDPOINT = "https://solavi.digitalhpe.com";
    private static final String AIRNOW_SERVICE_ENDPOINT = "http://www.airnowapi.org";
    private static final String EPA_SERVICE_ENDPOINT = "https://iaspub.epa.gov/enviro/efservice";

    @Provides
    @Named("solaviService")
    SolaviService provideSolaviWebService() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return new RestAdapter.Builder()
                .setEndpoint(SOLAVI_SERVICE_ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(SolaviService.class);
    }

    @Provides
    @Named("airNowService")
    AirNowService provideAirNowWebService() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return new RestAdapter.Builder()
                .setEndpoint(AIRNOW_SERVICE_ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(AirNowService.class);
    }

    @Provides
    @Named("epaService")
    EPAService provideEpaWebService() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return new RestAdapter.Builder()
                .setEndpoint(EPA_SERVICE_ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(EPAService.class);
    }
}