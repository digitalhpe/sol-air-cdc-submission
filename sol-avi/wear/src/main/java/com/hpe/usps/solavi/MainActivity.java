package com.hpe.usps.solavi;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.BoxInsetLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;
import com.hpe.usps.solavilibrary.helpers.LocationHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class MainActivity extends WearableActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener, DataApi.DataListener, MessageApi.MessageListener, NodeApi.NodeListener{

    private final SimpleDateFormat AMBIENT_DATE_FORMAT = new SimpleDateFormat("HH:mm", Locale.US);
    private BoxInsetLayout containterView;
    private TextView locationText, lastUpdatedText, ozoneTitle, uviTitle;
    private View uviColor, ozoneColor;
    private ImageView uviText, ozoneText;
    private LinearLayout mainBackground;
    private Location lastLocation;
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private LocationHelper locationHelper;
    private CompositeSubscription subscriptions = new CompositeSubscription();

    private static final long LOCATION_INTERVAL = 30l * 1000;
    private String locationTextString = "";
    private static final String MOBILE_ACTIVITY_PATH = "/mobile";
    private boolean leftFocus = false;
    private boolean ignoreFirst = true;

    @Inject
    @Named("solaviService")
    SolaviService solaviService;

    @Inject
    @Named("airNowService")
    AirNowService airNowService;

    @Inject
    @Named("placesService")
    PlacesService placesService;

    @Inject
    @Named("epaService")
    EPAService epaService;

    @Inject
    @Named("geoHelper")
    GeoHelper geoHelper;

    @Inject
    @Named("preferenceHelper")
    PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WatchInjector.inject(this);
        locationHelper = new LocationHelper(preferenceHelper, geoHelper, epaService, airNowService, solaviService, placesService);

        setContentView(R.layout.activity_main);
        setAmbientEnabled();
        setViews();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .addApi(LocationServices.API)
                .build();
        leftFocus = false;
        Bundle extras = getIntent().getExtras();
        if (extras!= null){
            getLocationInfoFromString(extras.getString("location"));
        }

        pullLocationInfo();
    }

    private void setViews() {
        containterView = (BoxInsetLayout) findViewById(R.id.container);
        locationText = (TextView) findViewById(R.id.locationText);
        uviColor = findViewById(R.id.uviColor);
        ozoneColor = findViewById(R.id.o3Color);
        ozoneText = (ImageView) findViewById(R.id.o3Rating);
        uviText = (ImageView) findViewById(R.id.uviRating);
        ozoneTitle = (TextView) findViewById(R.id.o3Title);
        uviTitle = (TextView) findViewById(R.id.uviTitle);
        lastUpdatedText = (TextView) findViewById(R.id.lastUpdatedText);
        mainBackground = (LinearLayout) findViewById(R.id.main_background);
    }


    @Override
    public void onResume() {
        super.onResume();
        googleApiClient.connect();
    }

    @Override
    protected void onPause() {
        Wearable.DataApi.removeListener(googleApiClient, this);
        Wearable.MessageApi.removeListener(googleApiClient, this);
        Wearable.NodeApi.removeListener(googleApiClient, this);
        googleApiClient.disconnect();
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);
        leftFocus = true;
        updateDisplay();

    }

    @Override
    public void onUpdateAmbient() {
        super.onUpdateAmbient();
    }

    @Override
    public void onExitAmbient() {
        super.onExitAmbient();
        updateDisplay();
        leftFocus = false;
    }

    /**
     * Pulls the location information from the mobile application
     */
    private void pullLocationInfo(){
        //message the phone

//        if (hasWifi()){
//            callEpaLibrary();
//        }else{
            new StartMobileActivityTask().execute();
//        }
        leftFocus = false;
    }

    private boolean hasWifi() {
        ConnectivityManager connManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo current = connManager.getActiveNetworkInfo();
        return current != null && current.getType() == ConnectivityManager.TYPE_WIFI;
    }

    /**
     * Get details from EPA library if last location exists
     */
    private void callEpaLibrary() {
        if (lastLocation !=null) {
            getDetails(lastLocation.getLatitude(), lastLocation.getLongitude());
        }
    }


    private Collection<String> getNodes() {
        HashSet<String> results = new HashSet<>();
        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(googleApiClient).await();

        for (Node node : nodes.getNodes()) {
            results.add(node.getId());
        }

        return results;
    }

    private class StartMobileActivityTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... args) {
            Collection<String> nodes = getNodes();
            for (String node : nodes) {
                sendStartActivityMessage(node, "" );
            }
            return null;
        }
    }


    private void sendStartActivityMessage(String node, String data) {
        Wearable.MessageApi.sendMessage(
                googleApiClient, node, MOBILE_ACTIVITY_PATH, data.getBytes())
                .setResultCallback(
                        new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                //This doesn't matter so much as just sending it.
                            }
                        }
                );

    }



    private void updateDisplay() {
        //call for current location info
        Log.d("display","update");
        Resources res = getResources();
        int blackColor = res.getColor(R.color.black);
        if (isAmbient()) {
            int lightGrey = res.getColor(R.color.light_grey);
            locationText.setTextColor(lightGrey);
            ozoneTitle.setTextColor(lightGrey);
            uviTitle.setTextColor(lightGrey);
            mainBackground.setBackgroundColor(blackColor);
        } else {

            locationText.setTextColor(blackColor);
            ozoneTitle.setTextColor(blackColor);
            uviTitle.setTextColor(blackColor);
            locationText.setText(getString(R.string.current_location));
            if (leftFocus){
                pullLocationInfo();
            }
            locationText.setText(locationTextString);
            mainBackground.setBackground(res.getDrawable(R.drawable.watch_background));
        }
    }



    @Override
    public void onConnected(Bundle bundle) {
        Wearable.DataApi.addListener(googleApiClient, this);
        Wearable.MessageApi.addListener(googleApiClient, this);
        Wearable.NodeApi.addListener(googleApiClient, this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(LOCATION_INTERVAL);
        locationRequest.setFastestInterval(LOCATION_INTERVAL);
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

        if (lastLocation != null){
            getLocationAddress(lastLocation.getLatitude(), lastLocation.getLongitude());
        }
        pullLocationInfo();

    }

    /*
    * Use Geocoder to pull location from long/lat and format the string to City, ST
     */
    private void getLocationAddress(double latitude, double longitude) {
        final Geocoder geocoder = new Geocoder(this);

        locationTextString = getResources().getString(R.string.current_location);
        try {

            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                //String addressLine = address.getAddressLine(Math.max(address.getMaxAddressLineIndex()-1,0));
                String addressLine = address.getLocality();
//                if (addressLine != null && !addressLine.isEmpty()){
//                    int lastSpace = addressLine.lastIndexOf(' ');
//                    addressLine = addressLine.substring(0,lastSpace>0?lastSpace:0);
//                }
                locationTextString =  addressLine;

            }
        } catch (IOException e) {
            Log.e(MainActivity.class.getName(), e.getMessage(), e);
        }
        locationText.setText(locationTextString);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("connection","suspended");
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        //location listeners will trigger this
        lastLocation = location;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //result will tell you why the client failed
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        //unused
    }

    @Override
    public void onPeerConnected(Node node) {
        //unused
    }

    @Override
    public void onPeerDisconnected(Node node) {
        //unused
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.d("messageMain", "onMessageReceived: " + messageEvent +ignoreFirst);
        if (!messageEvent.getPath().equals(MOBILE_ACTIVITY_PATH)||ignoreFirst){
            ignoreFirst = false;
            return;
        }

        Log.d("node", "onDataChanged: " + messageEvent);
            byte [] data = messageEvent.getData();
            String decoded = "oops";
            try {
                decoded = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Log.e(MainActivity.class.getName(), e.getMessage(), e);
            }

        getLocationInfoFromString(decoded);

    }

    private void getLocationInfoFromString(String decoded) {
        String latStr, lngStr, ozoneStr, uvStr;
        double lat, lng;
        if (decoded.isEmpty()){
            locationText.setText(getString(R.string.loading));

            return;
        }
        List<String> list = Arrays.asList(decoded.split("\\s*,\\s*"));
        latStr = list.get(0);
        lngStr = list.get(1);
        ozoneStr = list.get(2);
        uvStr = list.get(3);
        Log.d("node",decoded);
        int o3Value = Integer.valueOf(ozoneStr);
        int uvValue = Integer.valueOf(uvStr);
        lat = Double.parseDouble(latStr);
        lng = Double.parseDouble(lngStr);
        ozoneText.setImageResource(ResourceHelper.getAQIRatingIdByIndex(o3Value));
        ozoneColor.setBackgroundColor(getResources().getColor(ResourceHelper.getAqiColorIdByIndex(o3Value)));
        uviText.setImageResource(ResourceHelper.getUVRatingIdByIndex(uvValue));
        uviColor.setBackgroundColor(getResources().getColor(ResourceHelper.getUVColorIdByIndex(uvValue)));
        setTextColors(o3Value,uvValue);
        getLocationAddress(lat, lng);
        Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("M'/'d'/'y h:mma");
        lastUpdatedText.setText(getString(R.string.last_updated)+"\n"+formatter.format(now.getTime()));
    }

    private void getDetails(double latitude, double longitude){
        subscriptions.add(
                locationHelper.getIndexesForLocation(latitude, longitude)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<com.hpe.usps.solavilibrary.models.Location>() {
                            @Override
                            public final void onCompleted() {
                                //unused
                            }

                            @Override
                            public final void onError(Throwable e) {
                                //unused
                            }

                            @Override
                            public final void onNext(com.hpe.usps.solavilibrary.models.Location response) {
                                loadDetails(response);
                            }
                        })
        );
    }

    private void loadDetails(com.hpe.usps.solavilibrary.models.Location response) {
        int o3Value, uvValue;

        o3Value = response.getOzone();
        uvValue = response.getUv();

        ozoneText.setImageResource(ResourceHelper.getAQIRatingIdByIndex(o3Value));
        ozoneColor.setBackgroundColor(getResources().getColor(ResourceHelper.getAqiColorIdByIndex(o3Value)));
        uviText.setImageResource(ResourceHelper.getUVRatingIdByIndex(uvValue));
        uviColor.setBackgroundColor(getResources().getColor(ResourceHelper.getUVColorIdByIndex(uvValue)));
        setTextColors(o3Value,uvValue);
        getLocationAddress(lastLocation.getLatitude(), lastLocation.getLongitude());
        Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("M'/'d'/'y h:mma");
        lastUpdatedText.setText(getString(R.string.last_updated)+"\n"+formatter.format(now.getTime()));
    }

    private void setTextColors(int o3Value, int uvValue) {
        if (o3Value>100) {
            ozoneText.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        }else{
            ozoneText.setColorFilter(ContextCompat.getColor(this, R.color.black), PorterDuff.Mode.SRC_ATOP);
        }
        if (uvValue>5) {
            uviText.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        }else{
            uviText.setColorFilter(ContextCompat.getColor(this, R.color.black), PorterDuff.Mode.SRC_ATOP);
        }
    }

}
