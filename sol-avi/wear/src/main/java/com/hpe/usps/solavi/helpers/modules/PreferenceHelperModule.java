package com.hpe.usps.solavi.helpers.modules;

import com.hpe.usps.solavi.BaseWatchApplication;
import com.hpe.usps.solavi.MainActivity;
import com.hpe.usps.solavi.helpers.AndroidPreferenceHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andy Jones
 */
@Module(
        injects = {
                MainActivity.class
        },
        library = true,
        complete = false
)
public class PreferenceHelperModule {

    private BaseWatchApplication mApp;

    public PreferenceHelperModule(BaseWatchApplication app) {
        mApp = app;
    }

    @Provides
    @Named("preferenceHelper")
    PreferenceHelper providesPreferenceHelper() {
        return new AndroidPreferenceHelper(mApp);
    }
}
