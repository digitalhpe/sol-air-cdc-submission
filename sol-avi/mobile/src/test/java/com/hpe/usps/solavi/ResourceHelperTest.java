package com.hpe.usps.solavi;

import com.hpe.usps.solavi.helpers.ResourceHelper;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ResourceHelperTest {

    @Test
    public void testGetAQIDescriptionIDGood() throws IOException {

        assertEquals(R.string.aqi_good, ResourceHelper.getAQIDescriptionID(1));
    }

    @Test
    public void testGetAQIDescriptionIDModerate() throws IOException {

        assertEquals(R.string.aqi_moderate, ResourceHelper.getAQIDescriptionID(99));
    }

    @Test
    public void testGetAQIDescriptionIDUnhealthySensitive() throws IOException {

        assertEquals(R.string.aqi_unhealthy_sensitive, ResourceHelper.getAQIDescriptionID(149));
    }
}