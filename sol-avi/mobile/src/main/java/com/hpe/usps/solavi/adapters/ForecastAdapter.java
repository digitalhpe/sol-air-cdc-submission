package com.hpe.usps.solavi.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.zetterstrom.com.forecast.models.DataPoint;

import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.MainActivity;
import com.hpe.usps.solavi.helpers.ResourceHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by chishobr
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>{

    private List<DataPoint> forecastList;

    public ForecastAdapter(Context activity, List<DataPoint> forecasts) {
        forecastList = forecasts;
    }

    @Override
    public ForecastAdapter.ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.forecast_cell, parent, false);
        ForecastViewHolder fvh = new ForecastViewHolder(v);
        return fvh;
    }

    @Override
    public void onBindViewHolder(ForecastAdapter.ForecastViewHolder holder, int position) {
        DataPoint forecast = forecastList.get(position);
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        holder.getFCDateTextView().setText(format.format(forecast.getTime()));
        holder.getFCHighLowTextView().setText((int) Math.round(forecast.getTemperatureMax())+"\u00B0"+" / "+(int) Math.round(forecast.getTemperatureMin())+"\u00B0");
        holder.getFCIconImageView().setImageResource(ResourceHelper.getWeatherIconId(forecast.getIcon(), true));
    }

    @Override
    public int getItemCount() {
        return forecastList.size();
    }

    public static class ForecastViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView fcDate, fcHighLow;
        private ImageView fcIcon;
        private ForecastViewHolder(View v) {
            super(v);
            fcDate = (TextView) v.findViewById(R.id.fc_date);
            fcHighLow = (TextView) v.findViewById(R.id.fc_high_low);
            fcIcon = (ImageView) v.findViewById(R.id.fc_icon);
        }
        public TextView getFCDateTextView(){
            return fcDate;
        }
        public TextView getFCHighLowTextView(){
            return fcHighLow;
        }
        public ImageView getFCIconImageView(){
            return fcIcon;
        }
    }
}
