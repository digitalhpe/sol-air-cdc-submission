package com.hpe.usps.solavi.views;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.MainActivity;

/**
 * Created by chishobr on 8/16/2017.
 */

public class ParkBenefitDialog  extends DialogFragment {

    private TextView parkBenefitText;
    private TextView parkPdfLink;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.park_benefit_layout, container,
                false);
        parkBenefitText = (TextView) rootView.findViewById(R.id.park_benefit_html);
        parkBenefitText.setText(Html.fromHtml(getString(R.string.park_benefits_text)));

        parkPdfLink = (TextView) rootView.findViewById(R.id.tfpl_link);
        parkPdfLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).showParkBenefitsPDF();
                dismiss();
            }
        });

        return rootView;
    }
}
