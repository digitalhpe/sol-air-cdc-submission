package com.hpe.usps.solavi;

import android.app.Application;
import android.content.SharedPreferences;

import com.hpe.usps.solavi.helpers.modules.GeoHelperModule;
import com.hpe.usps.solavi.helpers.modules.PreferenceHelperModule;
import com.hpe.usps.solavilibrary.models.Location;
import com.hpe.usps.solavi.rest.mock.RestServiceModule;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andy Jones on 12/8/2015.
 */
public class BaseApplication extends Application {

    public static final String AQI_WIDTH = "AQI_WIDTH";
    public static final String UV_WIDTH = "UV_WIDTH";
    public static final String DENSITY = "DENSITY";
    private static BaseApplication instance;
    private List<Location> savedLocations = new ArrayList<>();
    private float density = -1f;

    private SharedPreferences preferences;

    public SharedPreferences getSharedPrefs(){
        if(preferences == null){
            preferences = getSharedPreferences( getPackageName() + "_preferences", MODE_PRIVATE);
        }
        return preferences;
    }

    public BaseApplication() {
        instance = this;
    }

    public void onCreate() {
        super.onCreate();

        Injector.init(new RestServiceModule(), new GeoHelperModule(this), new PreferenceHelperModule(this));
    }

    public static BaseApplication getInstance() {
        return instance;
    }

    public List<Location> getSavedLocations() {
        return savedLocations;
    }

    public void setSavedLocations(List<Location> savedLocations) {
        this.savedLocations = savedLocations;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

}
