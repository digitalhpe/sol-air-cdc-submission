package com.hpe.usps.solavi.rest.mock;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hpe.usps.solavi.activities.MainActivity;
import com.hpe.usps.solavi.fragments.LocationInfoFragment;
import com.hpe.usps.solavi.fragments.LocationMapFragment;
import com.hpe.usps.solavi.fragments.MainActivityFragment;
import com.hpe.usps.solavi.services.WeatherService;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.GoogleLocationService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;
import com.hpe.usps.solavi.views.LocationRecyclerAdapter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 *
 * This module contains retrofit rest services to be injected via dagger
 */
@Module(
        injects = {
                MainActivity.class,
                MainActivityFragment.class,
                LocationMapFragment.class,
                LocationInfoFragment.class,
                WeatherService.class,
                LocationRecyclerAdapter.class
        },
        library = true,
        complete = false
)
public class RestServiceModule {

    //private static final String SOLAVI_SERVICE_ENDPOINT = "https://solavi.digitalhpe.com";
    private static final String SOLAIR_CDC_ENDPOINT = "https://solair.digitaldxc.com";
    private static final String GOOGLE_API_ENDPOINT = "http://maps.googleapis.com/maps/api/geocode";
    private static final String AIRNOW_SERVICE_ENDPOINT = "http://www.airnowapi.org";
    private static final String EPA_SERVICE_ENDPOINT = "https://iaspub.epa.gov/enviro/efservice";
    private static final String GOOGLE_PLACES_ENDPOINT = "https://maps.googleapis.com/maps/api/place/nearbysearch";

    @Provides
    @Named("googleLocationService")
    GoogleLocationService provideGoogleLocationService() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return new RestAdapter.Builder()
                .setEndpoint(GOOGLE_API_ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(GoogleLocationService.class);
    }

    @Provides
    @Named("solaviService")
    SolaviService provideSolaviWebService() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return new RestAdapter.Builder()
                .setEndpoint(SOLAIR_CDC_ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(SolaviService.class);
    }

    @Provides
    @Named("airNowService")
    AirNowService provideAirNowWebService() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return new RestAdapter.Builder()
                .setEndpoint(AIRNOW_SERVICE_ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(AirNowService.class);
    }

    @Provides
    @Named("epaService")
    EPAService provideEpaWebService() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return new RestAdapter.Builder()
                .setEndpoint(EPA_SERVICE_ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(EPAService.class);
    }

    @Provides
    @Named("placesService")
    PlacesService providePlacesService() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return new RestAdapter.Builder()
                .setEndpoint(GOOGLE_PLACES_ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(PlacesService.class);
    }
}