package com.hpe.usps.solavi.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hpe.usps.solavi.R;

/**
 * The Card Layout Holder for a favorite location
 * Created by chishobr
 */
class FavoriteLocationCardHolder extends RecyclerView.ViewHolder{

    protected ImageView favoriteNotified, crosshairs, errorIcon;
    protected TextView favoriteName, errorText;
    protected RelativeLayout uvGridLayout, ozGridLayout, favoriteLayout, loadingLayout, locationOffLayout;
    protected CustomAQIScale customAqiScale;
    protected CustomUVScale customUvScale;

    public FavoriteLocationCardHolder(View v) {
        super(v);
        if (v.findViewById(R.id.cloud) != null)  {
            return;
        }
        favoriteLayout =  (RelativeLayout) v.findViewById(R.id.favorite_layout);
        favoriteName =  (TextView) v.findViewById(R.id.favorite_name);
        favoriteNotified = (ImageView)  v.findViewById(R.id.notified_favorite_icon);
        crosshairs = (ImageView) v.findViewById(R.id.crosshairsOrX);
        locationOffLayout = (RelativeLayout) v.findViewById(R.id.location_off_layout);
        loadingLayout = (RelativeLayout) v.findViewById(R.id.progressBar_layout);
        uvGridLayout = (RelativeLayout) v.findViewById(R.id.uv_grid_layout);
        ozGridLayout = (RelativeLayout) v.findViewById(R.id.aqi_grid_layout);
        errorIcon = (ImageView)v.findViewById(R.id.no_location_icon);
        errorText = (TextView)v.findViewById(R.id.error_text);

        setCustomLayouts(v);
    }

    /**
     * set the CustomScales inside the inflated view
     * @param v
     */
    private void setCustomLayouts(View v) {
        customAqiScale = (CustomAQIScale) v.findViewById(R.id.custom_aqi_scale);
        customUvScale = (CustomUVScale) v.findViewById(R.id.custom_uv_scale);
    }

    public void loading() {
        customAqiScale.loadingAQIScale();
        customUvScale.loadingUVScale();
    }

    public void setErrorIconResource(int resId, int stringId, Context context){
        errorIcon.setImageResource(resId);
        errorText.setText(stringId);
        errorIcon.setColorFilter(ContextCompat.getColor(context, R.color.icon_gray), PorterDuff.Mode.SRC_ATOP);
    }


}
