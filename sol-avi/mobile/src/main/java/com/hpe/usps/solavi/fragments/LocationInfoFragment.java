package com.hpe.usps.solavi.fragments;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.SortedList;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.models.DataBlock;
import android.zetterstrom.com.forecast.models.DataPoint;
import android.zetterstrom.com.forecast.models.Forecast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.hpe.usps.solavi.views.ParkRecyclerAdapter;
import com.hpe.usps.solavilibrary.models.aggregate.AggregateResult;
import com.hpe.usps.solavi.views.ArcedEPAGauge;
import com.hpe.usps.solavi.adapters.ForecastAdapter;

import com.hpe.usps.solavilibrary.models.aggregate.Measure;
import com.hpe.usps.solavilibrary.models.googlelocation.AddressComponent;
import com.hpe.usps.solavilibrary.models.googlelocation.LatLng;
import com.hpe.usps.solavilibrary.models.googlelocation.Result;
import com.hpe.usps.solavilibrary.rest.GoogleLocationService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;
import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.Constants;
import com.hpe.usps.solavi.Injector;
import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.MainActivity;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;
import com.hpe.usps.solavilibrary.helpers.LocationHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;
import com.hpe.usps.solavi.helpers.ResourceHelper;
import com.hpe.usps.solavilibrary.models.AddLocationRequest;
import com.hpe.usps.solavilibrary.models.AddLocationResponse;
import com.hpe.usps.solavilibrary.models.DatePickerEvent;
import com.hpe.usps.solavilibrary.models.Location;
import com.hpe.usps.solavilibrary.models.LocationDetail;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by chishobr on 12/11/2015.
 */
public class LocationInfoFragment extends Fragment{

    @Inject
    @Named("solaviService")
    SolaviService solaviService;

    @Inject
    @Named("googleLocationService")
    GoogleLocationService googleLocationService;

    @Inject
    @Named("airNowService")
    AirNowService airnowService;

    @Inject
    @Named("placesService")
    PlacesService placesService;

    @Inject
    @Named("epaService")
    EPAService epaService;

    @Inject
    @Named("geoHelper")
    GeoHelper geoHelper;

    @Inject
    @Named("preferenceHelper")
    PreferenceHelper preferenceHelper;

    private LocationHelper locationHelper;

    private LocationDetail locationDetail;
    private Location location;

    boolean nameSet = false;

    private Calendar chosenTime;

    private CompositeSubscription subscriptions = new CompositeSubscription();

    private SimpleDateFormat format, tinyFormat;
    private ImageView aqiInfoIcon, calendarIcon, currentConditionIcon, parkInfoIcon;
    private MainActivity context;
    private TextView locationName, chosenDateText;
    private LineChart ozoneChart, asthmaChart;

    ArcedEPAGauge aqiO3, aqiPm10, aqiPm25, uvGauge;
    private String dateValuePast = "", dateValueNow = "" , dateValueFuture = "";
    private String geoCodeName = "";
    private boolean isNotified = false;
    private int infoRadius;

    private Menu menu;
    private CardView dataUnavailable, asthmaCard, parkCard;
    private RecyclerView forecastRecycler;
    private List<DataPoint> forecastList = new ArrayList<>();

    private TextView currentConditionsHighLow, currentConditionTime, shadowHtml, parkPercentage, floodPercentage;
    private LinearLayoutManager layoutManager;
    private ImageView arrowLeft, arrowRight;

    private RelativeLayout weatherProgress, weatherLayout, parkComparisons;
    private static final String ADMIN_LEVEL_1 = "administrative_area_level_1";
    private static final String ADMIN_LEVEL_2 = "administrative_area_level_2";

    ArrayList<Location> comparisonLocations = new ArrayList<>();
    private RecyclerView recyclerView;
    private ParkRecyclerAdapter recyclerAdapter;

    @Override
    public void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop(){
        EventBus.getDefault().unregister(this);

        super.onStop();
    }

    /**
     * A new Date was set in the DatePickerDialog
     * @param event
     */
    public void onEvent(DatePickerEvent event){
        chosenTime = event.getDate();

        setDate();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Injector.inject(this);
        locationHelper = new LocationHelper(preferenceHelper, geoHelper, epaService, airnowService, solaviService, placesService);
        //solAviServiceHelper = new SolAviServiceHelper();
        View view = inflater.inflate(R.layout.info_fragment, container, false);

        format = new SimpleDateFormat("MM/dd/yyyy");
        tinyFormat = new SimpleDateFormat("MM/dd");
        context = (MainActivity) getActivity();


        setHasOptionsMenu(true);
        setViews(view);
        //setWeather();

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_details, menu);

        this.menu = menu;
        this.menu.findItem(R.id.action_save).setEnabled(false);
        this.menu.findItem(R.id.action_overflow).setEnabled(false);
        if (location != null && location.getLocationid() !=null && "0".equals(location.getLocationid())){
            this.menu.findItem(R.id.action_save).setIcon(R.drawable.save_icon);
        }else{
            this.menu.findItem(R.id.action_save).setIcon(R.drawable.ic_save_white);
        }
        this.menu.findItem(R.id.action_save).getIcon().setColorFilter(ContextCompat.getColor(getActivity(), R.color.hpe_grey), PorterDuff.Mode.SRC_ATOP);
        this.menu.findItem(R.id.action_overflow).getIcon().setColorFilter(ContextCompat.getColor(getActivity(), R.color.hpe_grey), PorterDuff.Mode.SRC_ATOP);
        this.menu.findItem(R.id.action_share).getIcon().setColorFilter(ContextCompat.getColor(getActivity(), R.color.icon_gray), PorterDuff.Mode.SRC_ATOP);
        this.menu.findItem(R.id.action_notification_toggle).getIcon().setColorFilter(ContextCompat.getColor(getActivity(), R.color.icon_gray), PorterDuff.Mode.SRC_ATOP);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            saveLocation();
        }
        else if (id == R.id.action_share){
            context.setShareIntent();
        }else if (id == R.id.action_notification_toggle){
            if (isNotified){
                isNotified = false;
                Toast.makeText(getActivity(), getString(R.string.alerts_off),Toast.LENGTH_LONG).show();
            }else{
                isNotified = true;
                Toast.makeText(getActivity(), getString(R.string.alerts_on),Toast.LENGTH_LONG).show();
            }
            setNotificationToggle();
        }

        return super.onOptionsItemSelected(item);
    }
    /**
     * Set the views within the fragment
     * @param view the inflated layout from OnCreateView
     */
    public void setViews(View view) {
        locationName = (EditText)view.findViewById(R.id.location_name_edit);
        aqiInfoIcon = (ImageView)view.findViewById(R.id.aqi_info_icon);
        setScaledInfoIconSize();
        aqiInfoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.showAqiInfo();
            }
        });
        calendarIcon = (ImageView)view.findViewById(R.id.date_icon);
        calendarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.showDatePicker();
            }
        });

        chosenDateText = (TextView)view.findViewById(R.id.chosen_date_text);

        ozoneChart = (LineChart) view.findViewById(R.id.chart);

        aqiO3 = (ArcedEPAGauge) view.findViewById(R.id.aqi_ozone);
        aqiPm10 = (ArcedEPAGauge) view.findViewById(R.id.aqi_pm10);
        aqiPm25 = (ArcedEPAGauge) view.findViewById(R.id.aqi_pm25);
        uvGauge = (ArcedEPAGauge) view.findViewById(R.id.uv_gauge);

        dataUnavailable = (CardView)view.findViewById(R.id.data_unavailable_card);
        asthmaCard = (CardView)view.findViewById(R.id.asthma_card);
        parkCard = (CardView)view.findViewById(R.id.park_card);
        parkInfoIcon = (ImageView)view.findViewById(R.id.info_icon_park);
        parkInfoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.showParkBenefitsDialog();
            }
        });

        parkPercentage = (TextView)view.findViewById(R.id.park_percentage);

        //currentConditionsCity = (TextView)view.findViewById(R.id.city_name);
        currentConditionsHighLow = (TextView)view.findViewById(R.id.high_low);
        currentConditionIcon = (ImageView)view.findViewById(R.id.condition_icon);
        currentConditionTime = (TextView)view.findViewById(R.id.current_time);

        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        forecastRecycler = (RecyclerView) view.findViewById(R.id.forecast_recycler);
        forecastRecycler.setLayoutManager(layoutManager);

        arrowLeft = (ImageView)view.findViewById(R.id.more_left);
        arrowRight = (ImageView)view.findViewById(R.id.more_right);

        forecastRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                showArrows();

            }
        });

        weatherProgress = (RelativeLayout)view.findViewById(R.id.weather_progress_bar);
        weatherLayout = (RelativeLayout)view.findViewById(R.id.weather_layout);

        setDate();
        setDetails();

        asthmaChart = (LineChart) view.findViewById(R.id.asthmaChart);

        recyclerView = (RecyclerView) view.findViewById(R.id.parkList);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        parkComparisons = (RelativeLayout) view.findViewById(R.id.park_comparison_layout);

    }



    private void showArrows() {
        boolean seeTop = false, seeBottom = false;
        if(layoutManager.findFirstCompletelyVisibleItemPosition()==0){
            seeTop = true;
        }

        if(layoutManager.findLastCompletelyVisibleItemPosition()==forecastList.size()-1){
            seeBottom = true;
        }
        if (!seeTop){
            arrowLeft.setVisibility(View.VISIBLE);
        }else{
            arrowLeft.setVisibility(View.INVISIBLE);
        }
        if (!seeBottom){
            arrowRight.setVisibility(View.VISIBLE);
        }else{
            arrowRight.setVisibility(View.INVISIBLE);
        }
    }

    private void setScaledInfoIconSize() {
        ViewGroup.LayoutParams lp = aqiInfoIcon.getLayoutParams();
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager w = getActivity().getWindowManager();
        w.getDefaultDisplay().getMetrics(metrics);
        infoRadius = (int)(Math.min(metrics.widthPixels, metrics.heightPixels)/7.3f);
        lp.height = infoRadius;
        lp.width = infoRadius;
    }

    /**
     * Changes the TextViews to match the selected Date accordingly
     */
    private void setDate() {
        if (chosenTime == null){
            chosenTime = Calendar.getInstance();
            chosenTime.set(Calendar.YEAR, chosenTime.get(Calendar.YEAR)-1);
        }
        chosenDateText.setText(format.format(chosenTime.getTime()));
        dateValuePast = tinyFormat.format(new Date(chosenTime.getTimeInMillis()-MainActivity.ONE_WEEK_IN_MILLIS));
        dateValueNow = tinyFormat.format(chosenTime.getTime());
        dateValueFuture = tinyFormat.format(new Date(chosenTime.getTimeInMillis()+MainActivity.ONE_WEEK_IN_MILLIS));
        if (locationDetail != null) {
            loadHistory(chosenTime.getTime().getTime() / DateUtils.SECOND_IN_MILLIS);
        }
    }

    /**
     * Retrieves air quality and uv index history from web service for a given date.
     * @param epochDate Number of seconds in epoch time
     */
    private void loadHistory(long epochDate) {
        ((MainActivity)getActivity()).showProgressSpinner();

        subscriptions.add(
                solaviService.getLocationHistory(locationDetail.getZip(), epochDate)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<LocationDetail.History>() {
                            @Override
                            public final void onCompleted() {
                                //unused
                            }

                            @Override
                            public final void onError(Throwable e) {
                                Toast.makeText(BaseApplication.getInstance(), R.string.failed_to_save_location, Toast.LENGTH_SHORT).show();
                                ((MainActivity) getActivity()).hideProgressSpinner();
                            }

                            @Override
                            public final void onNext(LocationDetail.History history) {
                                setAqiHistoryValues(history);
                                ((MainActivity) getActivity()).hideProgressSpinner();
                            }
                        })
        );
    }



    /**
     * Retrieves location details for the given location id (if a saved location) or latitude / longitude.
     * When the location details are retrieved, then the views are updated with the data that has been loaded.
     */
    private void setDetails() {

        ((MainActivity)getActivity()).showProgressSpinner();

        if (location == null){
            return;
        }else if (!("0").equals(location.getLocationid())) {
            locationName.setText(location.getName());


            nameSet = true;
        }else if (!geoCodeName.isEmpty()){
            locationName.setText(geoCodeName);

            nameSet = true;
        }



        subscriptions.add(
                locationHelper.getDetailForLocation(location.getLocationid(), location.getLatitude(), location.getLongitude())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<LocationDetail>() {
                            @Override
                            public final void onCompleted() {
                                //unused
                            }

                            @Override
                            public final void onError(Throwable e) {
                                Toast.makeText(BaseApplication.getInstance(), R.string.failed_to_load_location_details, Toast.LENGTH_SHORT).show();
                                ((MainActivity) getActivity()).hideProgressSpinner();
                                context.setShareText("");
                            }

                            @Override
                            public final void onNext(LocationDetail locationDetail) {
                                String shareText;

                                LocationInfoFragment.this.locationDetail = locationDetail;
                                isNotified = LocationInfoFragment.this.locationDetail.getNotify();
                                setNotificationToggle();
                                if (!nameSet && !("0").equals(location.getLocationid())) {
                                    locationName.setText(locationDetail.getName());
                                } else if (!geoCodeName.isEmpty()) {
                                    locationName.setText(geoCodeName);
                                }
                                shareText =
                                        getString(R.string.solavi_data_for_location)+
                                        " "+ locationName.getText().toString()+"\n\n"+
                                        getString(R.string.ozone_title)+" "+
                                        locationDetail.getAqi().getO3()+
                                        " ("+getString(ResourceHelper.getAQIDescriptionID(locationDetail.getAqi().getO3()))+")\n"+
                                                getString(R.string.pm25_title)+" "+
                                                locationDetail.getAqi().getPm25()+
                                                " ("+getString(ResourceHelper.getAQIDescriptionID(locationDetail.getAqi().getPm25()))+")\n"+
                                                getString(R.string.pm10_title)+" "+
                                                locationDetail.getAqi().getPm10()+
                                                " ("+getString(ResourceHelper.getAQIDescriptionID(locationDetail.getAqi().getPm10()))+")\n"+
                                        getString(R.string.uv_title) +" "+
                                        locationDetail.getUv() +
                                        " (" + getString(ResourceHelper.getUVDescriptionID(locationDetail.getUv())) + ")"+
                                                "\n\n"+getString(R.string.sent_from_solair);
                                ;

                                int o3Value, pm10value, pm25value, uvValue;
                                o3Value = locationDetail.getAqi().getO3();
                                pm10value = locationDetail.getAqi().getPm10();
                                pm25value = locationDetail.getAqi().getPm25();
                                uvValue = locationDetail.getUv();

                                if (o3Value < 0 || pm10value < 0 || pm25value < 0 || uvValue < 0){
                                    dataUnavailable.setVisibility(View.VISIBLE);
                                }else{
                                    dataUnavailable.setVisibility(View.GONE);
                                }

                                aqiO3.setValue(o3Value);
                                aqiPm10.setValue(pm10value);
                                aqiPm25.setValue(pm25value);
                                uvGauge.setValue(uvValue);

                                setAqiHistoryValues(locationDetail.getHistory());
                                ((MainActivity) getActivity()).hideProgressSpinner();
                                context.setShareText(shareText);
                                menu.findItem(R.id.action_save).setEnabled(true);
                                menu.findItem(R.id.action_save).getIcon().setColorFilter(ContextCompat.getColor(getActivity(), R.color.icon_gray), PorterDuff.Mode.SRC_ATOP);
                                menu.findItem(R.id.action_overflow).setEnabled(true);
                                menu.findItem(R.id.action_overflow).getIcon().setColorFilter(ContextCompat.getColor(getActivity(), R.color.icon_gray), PorterDuff.Mode.SRC_ATOP);

                            }
                        })
        );
//        subscriptions.add(
//                geoHelper.getLocationAddress(location.getLatitude(), location.getLongitude(), true)
//                        .subscribeOn(Schedulers.newThread())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribe(new Subscriber<String>() {
//                            @Override
//                            public void onCompleted() {
//                                //unused
//                            }
//
//                            @Override
//                            public void onError(Throwable e) {
//                                //unused
//                            }
//
//                            @Override
//                            public void onNext(String s) {
                                loadForecast(location.getLatitude(), location.getLongitude());
//                            }
//                        })
//        );
        subscriptions.add(
                googleLocationService.getLocationByCoordinates(new LatLng(location.getLatitude(), location.getLongitude()).toString(), true)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<com.hpe.usps.solavilibrary.models.googlelocation.Location>() {
                                       @Override
                                       public void onCompleted() {

                                       }

                                       @Override
                                       public void onError(Throwable e) {

                                       }

                                       @Override
                                       public void onNext(com.hpe.usps.solavilibrary.models.googlelocation.Location location) {
                                           getCDCResults(location);
                                       }
                                   }
                        )
        );

    }


    private void getCDCResults(com.hpe.usps.solavilibrary.models.googlelocation.Location googleLocation){
        String stateIdString = location.getStateId();
        Integer stateId =-1;
        String countyName = location.getCounty();
        if (countyName == null){
            List<Result> results = googleLocation.getResults();
            if (results.size()>0){
                List<AddressComponent> addyComponents = results.get(0).getAddressComponents();
                for (AddressComponent addressComponent: addyComponents){
                    if (addressComponent.getTypes().contains(ADMIN_LEVEL_1)){
                        stateId = locationHelper.getStateIDByAbbv(addressComponent.getShortName());
                        stateIdString = String.valueOf(stateId);
                    }
                    if (addressComponent.getTypes().contains(ADMIN_LEVEL_2)){
                        countyName = addressComponent.getLongName().replace(" County", "");
                    }
                }
                if (stateIdString != null && countyName != null && !countyName.isEmpty()) {
                    location.setCounty(countyName);
                    location.setStateId(String.valueOf(stateId));
                    subscriptions.add(
                            solaviService.getAggregateData(stateId, countyName)
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Subscriber<AggregateResult>() {
                                                   @Override
                                                   public void onCompleted() {

                                                   }

                                                   @Override
                                                   public void onError(Throwable e) {

                                                   }

                                                   @Override
                                                   public void onNext(AggregateResult aggregateResult) {
                                                       location.setMeasures(aggregateResult.getMeasures());
                                                       location.setFlood(aggregateResult.getFlood());
                                                       location.setPark(aggregateResult.getPark());
                                                       if (BaseApplication.getInstance().getSavedLocations().size()>0){
                                                           for (com.hpe.usps.solavilibrary.models.Location savedLocation : BaseApplication.getInstance().getSavedLocations()){
                                                               if (savedLocation.getLocationid().equals(location.getLocationid()))continue;
                                                               comparisonLocations.add(savedLocation);
                                                           }
                                                       }
                                                       populateGraph(aggregateResult.getMeasures());
                                                       populatePark(aggregateResult.getPark());
                                                   }
                                               }
                                    )
                    );
                }
            }
        }else{
            if (BaseApplication.getInstance().getSavedLocations().size()>0){
                for (com.hpe.usps.solavilibrary.models.Location savedLocation : BaseApplication.getInstance().getSavedLocations()){
                    if (savedLocation.getLocationid().equals(location.getLocationid()))continue;
                    comparisonLocations.add(savedLocation);
                }
            }
            populateGraph(location.getMeasures());
            populatePark(location.getPark());
        }
    }

    private void populatePark(Double park) {
        if (park == null){
            parkCard.setVisibility(View.GONE);
            return;
        }
        parkPercentage.setText(String.valueOf(park)+"% *");
        parkCard.setVisibility(View.VISIBLE);
        addParkComparison();
    }

    private void addParkComparison() {
        if (comparisonLocations.size()>0) {
            parkComparisons.setVisibility(View.VISIBLE);
            recyclerAdapter = new ParkRecyclerAdapter(comparisonLocations, getActivity());
            recyclerView.setAdapter(recyclerAdapter);
        }else{
            parkComparisons.setVisibility(View.GONE);
        }
    }

    private void populateGraph(List<Measure> measures) {

        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<Entry> valsHosp = new ArrayList<>();
        ArrayList<Entry> valsER = new ArrayList<>();

        for(Measure yearInfo : measures){
            xVals.add(String.valueOf(yearInfo.getYear()));
            int index = xVals.indexOf(String.valueOf(yearInfo.getYear()));
            if (yearInfo.getHospital()!=null){
                valsHosp.add(new Entry(yearInfo.getHospital(), index ) );
            }
            if (yearInfo.getEr()!=null){
                valsER.add(new Entry(yearInfo.getEr(), index ) );
            }
        }
        if (valsHosp.size() == 0 && valsER.size() == 0){
            hideAsthmaCard();
            return;
        }
        LineDataSet setHosp = new LineDataSet(valsHosp, "Hospitalizations");
        setHosp.setColor(getResources().getColor(R.color.aqi_red));
        setHosp.setCircleColor(getResources().getColor(R.color.aqi_red));
        setHosp.setLineWidth(3.0f);
        setHosp.setCircleSize(5.0f);
        setHosp.setAxisDependency(YAxis.AxisDependency.LEFT);
        LineDataSet setER = new LineDataSet(valsER, "ER Visits");
        setER.setColor(getResources().getColor(R.color.aqi_green));
        setER.setCircleColor(getResources().getColor(R.color.aqi_green));
        setER.setAxisDependency(YAxis.AxisDependency.LEFT);
        setER.setLineWidth(3.0f);
        setER.setCircleSize(5.0f);


        ArrayList<LineDataSet> dataSets = new ArrayList<>();
        dataSets.add(setHosp);
        dataSets.add(setER);

        LineData data = new LineData(xVals, dataSets);
        asthmaChart.setData(data);
        asthmaChart.setDescription("");
        asthmaChart.setPinchZoom(true);

        asthmaChart.animateY(600);

    }

    private void hideAsthmaCard() {
        asthmaCard.setVisibility(View.GONE);
    }

    private void setNotificationToggle() {
        if (!isNotified){
            menu.findItem(R.id.action_notification_toggle).setIcon(getResources().getDrawable(R.drawable.ic_notifications_off_white));
            menu.findItem(R.id.action_notification_toggle).getIcon().setColorFilter(ContextCompat.getColor(getActivity(), R.color.icon_gray), PorterDuff.Mode.SRC_ATOP);
            menu.findItem(R.id.action_notification_toggle).setTitle(R.string.notification_off);
        }else{
            menu.findItem(R.id.action_notification_toggle).setIcon(getResources().getDrawable(R.drawable.ic_notifications_active_white));
            menu.findItem(R.id.action_notification_toggle).getIcon().setColorFilter(ContextCompat.getColor(getActivity(), R.color.icon_gray), PorterDuff.Mode.SRC_ATOP);
            menu.findItem(R.id.action_notification_toggle).setTitle(R.string.notification_on);
        }
    }


    /**

     * @param history
     */
    private void setAqiHistoryValues(LocationDetail.History history) {
        int o3past = history.getAqiminus().getO3(), o3now= history.getAqi().getO3(), o3future = history.getAqiplus().getO3();
        int pm10past = history.getAqiminus().getPm10(), pm10now = history.getAqi().getPm10(), pm10future = history.getAqiplus().getPm10();
        int pm25past = history.getAqiminus().getPm25(), pm25now = history.getAqi().getPm25(), pm25future = history.getAqiplus().getPm25();

        ArrayList<LineDataSet> dataSets = new ArrayList<>();

        LineDataSet o3Data, pm10Data, pm25Data;
        o3Data = addDataLine(o3past, o3now, o3future, getString(R.string.ozone_verbose));
        pm10Data = addDataLine(pm10past, pm10now, pm10future, getString(R.string.pm10_verbose));
        pm25Data = addDataLine(pm25past, pm25now, pm25future, getString(R.string.pm25_verbose));

        if (o3Data != null) {
            dataSets.add(o3Data);
        }
        if (pm10Data != null) {
            dataSets.add(pm10Data);
        }
        if (pm25Data != null) {
            dataSets.add(pm25Data);
        }

        ArrayList<String> xVals = new ArrayList<>();
        xVals.add(dateValuePast); xVals.add(dateValueNow); xVals.add(dateValueFuture);

        LineData data = new LineData(xVals, dataSets);
        ozoneChart.setData(data);
        ozoneChart.setDescription("");
        ozoneChart.setPinchZoom(true);

        ozoneChart.animateY(600);

    }

    private LineDataSet addDataLine(float past, float now, float future, String label){
        if (past > -1 && now > -1 && future > -1){

        }else if (past == -1 && now == -1 && future == -1){
            return null;
        }else if (past == -1 && now > -1 && future > -1){
            past = now - (future - now);
        }else if (past > -1 && now == -1 && future > -1){
            now = (future + past) /2;
        }else if (past > -1 && now > -1 && future == -1){
            future = now + (now - past);
        }else if (past > -1 && now == -1 && future == -1){
            now = (future + past) /2;
        }else if (past > -1 && now == -1 && future == -1){
            now = future = past;
        }else if (past == -1 && now > -1 && future == -1){
            future = past = now;
        }else if (past == -1 && now == -1 && future > -1){
            now = past = future;
        }

        ArrayList<Entry> vals = new ArrayList<>();
        Entry pastEntry = new Entry(past, 0);
        Entry nowEntry = new Entry(now, 1);
        Entry futureEntry = new Entry(future, 2);
        vals.add(pastEntry);
        vals.add(nowEntry);
        vals.add(futureEntry);

        LineDataSet dataSet = new LineDataSet(vals, label);
        if (label.equals(getString(R.string.ozone_verbose))) {
            dataSet.setColor(getResources().getColor(R.color.aqi_red));
            dataSet.setCircleColor(getResources().getColor(R.color.aqi_red));
        }else if (label.equals(getString(R.string.pm10_verbose))) {
            dataSet.setColor(getResources().getColor(R.color.aqi_green));
            dataSet.setCircleColor(getResources().getColor(R.color.aqi_green));
        }else if (label.equals(getString(R.string.pm25_verbose))) {
            dataSet.setColor(getResources().getColor(R.color.aqi_purple));
            dataSet.setCircleColor(getResources().getColor(R.color.aqi_purple));
        }

        dataSet.setLineWidth(3.0f);
        dataSet.setCircleSize(5.0f);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

        return dataSet;
    }

    /**
     * saves location
     */
    public void saveLocation() {

        if (location.getCounty() == null){
            Toast.makeText(getContext(), getString(R.string.loading_data_please_wait), Toast.LENGTH_LONG).show();
        }

        String locationName = this.locationName.getEditableText().toString().trim();
        String pushToken = preferenceHelper.getPushToken();

        //TODO: crashes
        locationDetail.setName(locationName);
        locationDetail.setNotify(isNotified);

        AddLocationRequest addLocationRequest = new AddLocationRequest(location.getLocationid(), pushToken, location.getLatitude(),
                location.getLongitude(), locationDetail.getZip(), locationDetail.getName(), locationDetail.getNotify(), location.getStateId(), location.getCounty());

        ((MainActivity)getActivity()).showProgressSpinner();

        subscriptions.add(
                solaviService.addLocation(addLocationRequest)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<AddLocationResponse>() {
                            @Override
                            public final void onCompleted() {
                                //unused
                            }

                            @Override
                            public final void onError(Throwable e) {
                                Toast.makeText(BaseApplication.getInstance(), R.string.failed_to_save_location, Toast.LENGTH_SHORT).show();
                                ((MainActivity) getActivity()).hideProgressSpinner();
                                View view = getActivity().getCurrentFocus();
                                if (view != null) {
                                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                }
                            }

                            @Override
                            public final void onNext(AddLocationResponse addLocationResponse) {
                                if (addLocationResponse.getResult().equals(Constants.RESPONSE_OK)) {
                                    ((MainActivity) getActivity()).hideProgressSpinner();
                                    ((MainActivity) getActivity()).showLocationList();
                                    View view = getActivity().getCurrentFocus();
                                    if (view != null) {
                                        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                    }
                                } else {
                                    Toast.makeText(BaseApplication.getInstance(), R.string.failed_to_save_location, Toast.LENGTH_SHORT).show();
                                    ((MainActivity) getActivity()).hideProgressSpinner();
                                    View view = getActivity().getCurrentFocus();
                                    if (view != null) {
                                        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                    }
                                }
                            }
                        })
        );
    }

    /**
     * sets the location to display information for
     * @param location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        subscriptions.unsubscribe();
    }

    public void setGeoCodeName(String geoCodeName) {
        this.geoCodeName = geoCodeName;
    }

    private void loadForecast(double latitude, double longitude){
        ForecastClient.getInstance()
                .getForecast(latitude, longitude, new Callback<Forecast>() {
                    @Override
                    public void onResponse(Call<Forecast> forecastCall, Response<Forecast> response) {
                        if (response.isSuccessful()) {
                            Forecast forecast = response.body();
                            populateWeather(forecast);
                        }
                    }

                    @Override
                    public void onFailure(Call<Forecast> forecastCall, Throwable t) {

                    }
                });
    }

    private void populateWeather(Forecast forecast) {
        DataPoint current = forecast.getCurrently();
        DataBlock dailyForecast = forecast.getDaily();
        ArrayList<DataPoint> dataPoints = dailyForecast.getDataPoints();
        weatherProgress.setVisibility(View.GONE);
        weatherLayout.setVisibility(View.VISIBLE);

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        currentConditionTime.setText(format.format(current.getTime()));
        currentConditionIcon.setImageResource(ResourceHelper.getWeatherIconId(current.getIcon(),false));
        currentConditionsHighLow.setText((int) Math.round(current.getTemperature())+"\u00B0");

        forecastList.clear();
        forecastList.addAll(dataPoints);
        ForecastAdapter forecastAdapter = new ForecastAdapter(getActivity(), forecastList);
        forecastRecycler.setAdapter(forecastAdapter);
        showArrows();

    }


}
