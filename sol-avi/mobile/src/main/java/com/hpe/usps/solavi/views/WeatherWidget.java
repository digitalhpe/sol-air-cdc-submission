package com.hpe.usps.solavi.views;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.SplashActivity;
import com.hpe.usps.solavi.services.WeatherService;

/**
 * Created by chishobr
 */
public class WeatherWidget extends AppWidgetProvider {
    public final static String NUM_ROWS = "NUM_ROWS";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        for (int appwidget_id : appWidgetIds) {
            setRefresh(appwidget_id, context, appWidgetManager);
        }
        startUpService(context, appWidgetManager);

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onAppWidgetOptionsChanged(Context context,
                                          AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {


        // See the dimensions and
        Bundle options = appWidgetManager.getAppWidgetOptions(appWidgetId);

        // Get min width and height.
        int minHeight = options
                .getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT);


        // Obtain appropriate widget and update it.
        appWidgetManager.updateAppWidget(appWidgetId,
                getRemoteViews(context, minHeight));

        setRefresh(appWidgetId, context, appWidgetManager);
        startUpService(context,appWidgetManager );

        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId,
                newOptions);
    }

    private void setRefresh(int appWidgetId, Context context, AppWidgetManager appWidgetManager) {

        RemoteViews remoteViews;
        int rows = BaseApplication.getInstance().getSharedPrefs().getInt(WeatherWidget.NUM_ROWS, 2);
        if (rows > 1) {
            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        } else {
            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout_small);
        }
        Intent intent = new Intent(context.getApplicationContext(),
                WeatherWidget.class);
        int [] widgetIds = {appWidgetId};
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                context.getApplicationContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.refresh, pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }

    private void startUpService(Context context, AppWidgetManager appWidgetManager) {
        ComponentName thisWidget = new ComponentName(context,
                WeatherWidget.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

        // Build the intent to call the service
        Intent intent = new Intent(context.getApplicationContext(),
                WeatherService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

        // Update the widgets via the service
        context.startService(intent);
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {

        // v1.5 fix that doesn't call onDelete Action
        final String action = intent.getAction();
        if (AppWidgetManager.ACTION_APPWIDGET_DELETED.equals(action)) {
            final int appWidgetId = intent.getExtras().getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
                this.onDeleted(context, new int[] { appWidgetId });
                BaseApplication.getInstance().getSharedPrefs().edit().putInt(NUM_ROWS,2).commit();
            }
        }
        else
        {
            super.onReceive(context, intent);
        }
        return;
    }

    /**
     * Determine appropriate view based on width provided.
     *
     * @param minHeight
     * @return
     */
    private RemoteViews getRemoteViews(Context context,
                                       int minHeight) {
        // First find out rows and columns based on width provided.
        int rows = getCellsForSize(minHeight);

        BaseApplication.getInstance().getSharedPrefs().edit().putInt(NUM_ROWS,rows).commit();
        if (rows >1) {
            return new RemoteViews(context.getPackageName(),
                    R.layout.widget_layout);
        } else {
            return new RemoteViews(context.getPackageName(),
                    R.layout.widget_layout_small);
        }
    }

    /**
     * Returns number of cells needed for given size of the widget.
     *
     * @param size Widget size in dp.
     * @return Size in number of cells.
     */
    private static int getCellsForSize(int size) {
        return (size+30)/70;
    }

}

