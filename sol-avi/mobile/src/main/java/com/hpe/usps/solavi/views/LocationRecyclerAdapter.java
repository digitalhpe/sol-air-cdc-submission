package com.hpe.usps.solavi.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hpe.usps.solavi.Injector;
import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.helpers.GeneralHelper;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;
import com.hpe.usps.solavilibrary.helpers.LocationHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;
import com.hpe.usps.solavi.activities.MainActivity;
import com.hpe.usps.solavilibrary.models.Location;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * The Main location list's RecyclerView's adapter
 * Created by chishobr on 12/9/2015.
 */
public class LocationRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @Inject
    @Named("airNowService")
    AirNowService airnowService;

    @Inject
    @Named("epaService")
    EPAService epaService;

    @Inject
    @Named("placesService")
    PlacesService placesService;

    @Inject
    @Named("solaviService")
    SolaviService solaviService;

    @Inject
    @Named("geoHelper")
    GeoHelper geoHelper;

    @Inject
    @Named("preferenceHelper")
    PreferenceHelper preferenceHelper;
    private final static int CURRENT_LOCATION = 0;
    private static final int SAVED_LOCATION = 1;
    private static final int FOOTER_CLOUD = 2;
    private List<Location> locationList;
    private MainActivity context;
    private CompositeSubscription subscriptions = new CompositeSubscription();
    private LocationHelper locationHelper;
    private boolean firstLoad;

    public LocationRecyclerAdapter(List<Location> locationList, Context context) {
        this.locationList = locationList;
        this.context = (MainActivity) context;
        Injector.inject(this);
        locationHelper = new LocationHelper(preferenceHelper, geoHelper, epaService, airnowService, solaviService, placesService);
        firstLoad = true;
    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder locationHolder, int position) {

        if (locationHolder.getItemViewType() == CURRENT_LOCATION) {
            final Location location = locationList.get(position);
            FavoriteLocationCardHolder current = (FavoriteLocationCardHolder) locationHolder;
            configureCurrent(current, location);
        }
        else if (locationHolder.getItemViewType() == SAVED_LOCATION){
        //else{
            final Location location = locationList.get(position);
            FavoriteLocationCardHolder favorite = (FavoriteLocationCardHolder) locationHolder;
            configureFavorite(favorite, location);
        }

    }


    private void configureCurrent(final FavoriteLocationCardHolder current, final Location location) {
        current.crosshairs.setVisibility(View.VISIBLE);
        if (location.getLatitudeRaw()==null || location.getLongitudeRaw()==null){
            current.favoriteName.setText(R.string.current_location);
            current.locationOffLayout.setVisibility(View.VISIBLE);
            if (!GeneralHelper.isNetworkAvailable(context)){
                current.setErrorIconResource(R.drawable.no_cell_service, R.string.no_internet, context);

            }else{
                current.setErrorIconResource(R.drawable.ic_location_off_white, R.string.location_services_not_found, context);
            }
            current.loadingLayout.setVisibility(View.INVISIBLE);
            current.loading();

        }else {
            current.locationOffLayout.setVisibility(View.GONE);
            current.favoriteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.callInfoPage(null, location.getLatitude(), location.getLongitude(), current.favoriteName.getText().toString());
                }
            });

            subscriptions.add(
                    geoHelper.getLocationAddress(location.getLatitude(), location.getLongitude(), true)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<String>() {
                                @Override
                                public void onCompleted() {
                                    //unused
                                }

                                @Override
                                public void onError(Throwable e) {
                                    //unused
                                }

                                @Override
                                public void onNext(String s) {
                                    current.favoriteName.setText(s);
                                }
                            })
            );

            //loading instead of data unavailable

            if (firstLoad) {
                current.loading();
                current.loadingLayout.setVisibility(View.VISIBLE);
                firstLoad = false;
            }
            subscriptions.add(
                    locationHelper.getIndexesForLocation(location.getLatitude(), location.getLongitude())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<Location>() {
                                @Override
                                public final void onCompleted() {
                                    //unused
                                }

                                @Override
                                public final void onError(Throwable e) {
                                    //unknown data
                                }

                                @Override
                                public final void onNext(Location response) {
                                    location.setAqialert(LocationHelper.isUnhealtyAqi(response.getOzone()));
                                    location.setUvalert(LocationHelper.isUnhealtyUv(response.getUv()));
                                    location.setOzone(response.getOzone());
                                    location.setUv(response.getUv());

                                    if (location.getAqialert() || location.getUvalert()) {
                                        current.favoriteNotified.setVisibility(View.VISIBLE);
                                    } else {
                                        current.favoriteNotified.setVisibility(View.INVISIBLE);
                                    }

                                    setScales(location, current);

                                }
                            })
            );
        }
    }


    /**
     * Configure the views for a {@link com.hpe.usps.solavi.views.FavoriteLocationCardHolder}
     * @param favorite
     * @param location
     */
    private void configureFavorite(FavoriteLocationCardHolder favorite, final Location location) {
        favorite.favoriteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.callInfoPage(location.getLocationid(), location.getLatitude(), location.getLongitude(), "");
            }
        });
        favorite.crosshairs.setImageResource(R.drawable.close_x);
        favorite.crosshairs.setColorFilter(ContextCompat.getColor(context, R.color.icon_gray), PorterDuff.Mode.SRC_ATOP);
        favorite.crosshairs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.deleteLocation(location.getLocationid(), location.getName());
            }
        });

        favorite.favoriteName.setText(location.getName());
        if (location.getAqialert() || location.getUvalert()) {
            favorite.favoriteNotified.setVisibility(View.VISIBLE);
        } else {
            favorite.favoriteNotified.setVisibility(View.INVISIBLE);
        }

        setScales(location, favorite);
    }

    /**
     * Sets both the UV and AQI scales
     * @param location
     * @param favoriteHolder
     */
    private void setScales(Location location, FavoriteLocationCardHolder favoriteHolder) {
        setUVScales(location.getUv(), favoriteHolder);
        setOZScales(location.getOzone(), favoriteHolder);
        favoriteHolder.loadingLayout.setVisibility(View.GONE);
    }

    /**
     * Sets the decription and the selects the correct cell of the {@link com.hpe.usps.solavi.views.CustomAQIScale}
     * @param airQualityIndex
     * @param locationHolder
     */
    private void setOZScales(int airQualityIndex, FavoriteLocationCardHolder locationHolder) {

        locationHolder.customAqiScale.selectAqiScale(locationHolder.customAqiScale.getScaleWidth(),airQualityIndex);
    }

    /**
     * Sets the decription and the selects the correct cell of the {@link com.hpe.usps.solavi.views.CustomUVScale}
     * @param uvIndex
     * @param locationHolder
     */
    private void setUVScales(int uvIndex, FavoriteLocationCardHolder locationHolder) {

        locationHolder.customUvScale.selectUVScale(locationHolder.customUvScale.getScaleWidth(),uvIndex);
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0 ){
            return CURRENT_LOCATION;
        }else if (position < locationList.size()-1){
            return SAVED_LOCATION;
        }else{
            return FOOTER_CLOUD;
        }

    }

    public void setLocations(List<Location> locations){
        locationList = locations;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType==CURRENT_LOCATION || viewType == SAVED_LOCATION) {
            View favoriteView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.favorite_layout, parent, false);
            return new FavoriteLocationCardHolder(favoriteView);
        }
        else{
            View cloudView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.cloud_layout, parent, false);
            return new CloudHolder(cloudView);
        }
    }

}
