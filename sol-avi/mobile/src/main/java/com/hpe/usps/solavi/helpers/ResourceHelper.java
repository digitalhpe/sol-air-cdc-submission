package com.hpe.usps.solavi.helpers;

import android.zetterstrom.com.forecast.models.Icon;

import com.hpe.usps.solavi.R;

/**
 * A helper class to easily return index values instead of full strings
 * Created by chishobr on 12/11/2015.
 */
public class ResourceHelper {

    private ResourceHelper() {

    }

    public static int getAqiPlacement( int epaValue) {
        if (epaValue < 0){
            return -1;
        }

        if (epaValue <= 50){
            return 0;
        }

        if (epaValue <= 100){
            return 1;
        }

        if (epaValue <= 150){
            return 2;
        }

        if (epaValue <= 200){
            return 3;
        }

        if (epaValue <= 300){
            return 4;
        }
        return 5;
    }

    /**
     * @param aqiIndex
     * @return the appropriate resource index ID of the description string given an Air Quality Index value
     */
    public static int getAQIDescriptionID(int aqiIndex){

        if (aqiIndex < 0){
            return R.string.data_unavailable;
        }

        if (aqiIndex <= 50){
            return R.string.aqi_good;
        }

        if (aqiIndex <= 100){
            return R.string.aqi_moderate;
        }

        if (aqiIndex <= 150){
            return R.string.aqi_unhealthy_sensitive;
        }

        if (aqiIndex <= 200){
            return R.string.aqi_unhealthy;
        }

        if (aqiIndex <= 300){
            return R.string.aqi_very_unhealthy;
        }

        return R.string.aqi_hazardous;
    }

    /**
     * @param uvLevel
     * @return the appropriate resource index ID of the description string given a UV Index value
     */
    public static int getUVDescriptionID(int uvLevel){
        int uvDescriptionId;

        switch(uvLevel){
            case -1:
                uvDescriptionId = R.string.data_unavailable;
                break;
            case 0:
            case 1:
            case 2:
                uvDescriptionId = R.string.uv_low;
                break;
            case 3:
            case 4:
            case 5:
                uvDescriptionId = R.string.uv_moderate;
                break;
            case 6:
            case 7:
                uvDescriptionId = R.string.uv_high;
                break;
            case 8:
            case 9:
            case 10:
                uvDescriptionId = R.string.uv_very_high;
                break;
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
                uvDescriptionId = R.string.uv_extreme;
                break;
            default:
                uvDescriptionId = R.string.uv_low;
        }

        return uvDescriptionId;
    }

    /**
     * @param airQualityIndex
     * @return The Appropriate AQI color by resource index ID based on a given Air Quality Index value
     */
    public static int getAqiColorIdByIndex(int airQualityIndex) {
        if (airQualityIndex <= 50){
            return R.color.aqi_green;
        }

        if (airQualityIndex <= 100){
            return R.color.aqi_yellow;
        }

        if (airQualityIndex <= 150){
            return R.color.aqi_orange;
        }

        if (airQualityIndex <= 200){
            return R.color.aqi_red;
        }

        if (airQualityIndex <= 300){
            return R.color.aqi_purple;
        }

        return R.color.aqi_magenta;
    }

    /**
     * @param airQualityIndex
     * @return The Appropriate AQI pastel color by resource index ID based on a given Air Quality Index value
     */
    public static int getAqiPastelColorIdByIndex(int airQualityIndex) {
        if (airQualityIndex <= 50){
            return R.color.pastel_aqi_green;
        }

        if (airQualityIndex <= 100){
            return R.color.pastel_aqi_yellow;
        }

        if (airQualityIndex <= 150){
            return R.color.pastel_aqi_orange;
        }

        if (airQualityIndex <= 200){
            return R.color.pastel_aqi_red;
        }

        if (airQualityIndex <= 300){
            return R.color.pastel_aqi_purple;
        }

        return R.color.pastel_aqi_magenta;
    }

    /**
     * @param uvIndex
     * @return The Appropriate UV color by resource index ID based on a given UV Index value
     */
    public static int getUVColorIdByIndex(int uvIndex) {

        int uvColorId;

        switch(uvIndex){
            case 0:
            case 1:
            case 2:
                uvColorId = R.color.uv_green;
                break;
            case 3:
            case 4:
            case 5:
                uvColorId = R.color.uv_yellow;
                break;
            case 6:
            case 7:
                uvColorId = R.color.uv_orange;
                break;
            case 8:
            case 9:
            case 10:
                uvColorId = R.color.uv_red;
                break;
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
                uvColorId = R.color.uv_purple;
                break;
            default:
                uvColorId = R.color.uv_purple;
        }

        return uvColorId;
    }

    /**
     * @param uvIndex
     * @return The Appropriate UV pastel color by resource index ID based on a given UV Index value
     */
    public static int getUVPastelColorIdByIndex(int uvIndex) {

        int uvColorId;

        switch(uvIndex){
            case 0:
            case 1:
            case 2:
                uvColorId = R.color.pastel_uv_green;
                break;
            case 3:
            case 4:
            case 5:
                uvColorId = R.color.pastel_uv_yellow;
                break;
            case 6:
            case 7:
                uvColorId = R.color.pastel_uv_orange;
                break;
            case 8:
            case 9:
            case 10:
                uvColorId = R.color.pastel_uv_red;
                break;
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
                uvColorId = R.color.pastel_uv_purple;
                break;
            default:
                uvColorId = R.color.pastel_disabled;
        }

        return uvColorId;
    }

    public static int getAqiPastelColorIdByPosition(int i) {
        switch(i){
            case 0:
                return R.color.pastel_aqi_green;
            case 1:
                return R.color.pastel_aqi_yellow;
            case 2:
                return R.color.pastel_aqi_orange;
            case 3:
                return R.color.pastel_aqi_red;
            case 4:
                return R.color.pastel_aqi_purple;
            case 5:
                return R.color.pastel_aqi_magenta;
            default:
                return R.color.pastel_disabled;
        }
    }

    public static int getAqiColorIdByPosition(int i) {
        switch(i){
            case 0:
                return R.color.aqi_green;
            case 1:
                return R.color.aqi_yellow;
            case 2:
                return R.color.aqi_orange;
            case 3:
                return R.color.aqi_red;
            case 4:
                return R.color.aqi_purple;
            case 5:
                return R.color.aqi_magenta;
            default:
                return R.color.aqi_green;
        }
    }

    public static int getAqiPastelDarkColorIdByPosition(int i) {
        switch(i){
            case 0:
                return R.color.pastel_aqi_dark_green;
            case 1:
                return R.color.pastel_aqi_dark_yellow;
            case 2:
                return R.color.pastel_aqi_dark_orange;
            case 3:
                return R.color.pastel_aqi_dark_red;
            case 4:
                return R.color.pastel_aqi_dark_purple;
            case 5:
                return R.color.pastel_aqi_dark_magenta;
            default:
                return R.color.pastel_dark_disabled;
        }
    }

    public static int getUVDarkPastelColorIdByIndex(int uvIndex) {
        int uvColorId;

        switch(uvIndex){
            case 0:
            case 1:
            case 2:
                uvColorId = R.color.pastel_uv_dark_green;
                break;
            case 3:
            case 4:
            case 5:
                uvColorId = R.color.pastel_uv_dark_yellow;
                break;
            case 6:
            case 7:
                uvColorId = R.color.pastel_uv_dark_orange;
                break;
            case 8:
            case 9:
            case 10:
                uvColorId = R.color.pastel_uv_dark_red;
                break;
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
                uvColorId = R.color.pastel_uv_dark_purple;
                break;
            default:
                uvColorId = R.color.pastel_dark_disabled;
        }

        return uvColorId;
    }

    public static int getUVPosition(int uvIndex) {
        switch(uvIndex) {
            case 0:
            case 1:
            case 2:
                return 0;
            case 3:
            case 4:
            case 5:
                return 1;
            case 6:
            case 7:
                return 2;
            case 8:
            case 9:
            case 10:
                return 3;
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
                return 4;
            default:
                return -1;
        }

    }

    public static int getAQIPosition(int aqiIndex) {
        if (aqiIndex <= 50){
            return 0;
        }

        if (aqiIndex <= 100){
            return 1;
        }

        if (aqiIndex <= 150){
            return 2;
        }

        if (aqiIndex <= 200){
            return 3;
        }

        if (aqiIndex <= 300){
            return 4;
        }

        if(aqiIndex <=500){
            return 5;
        }

        return -1;
    }

    public static int getAqiDrawableId(int aqiIndex) {
        if (aqiIndex <= 50){
            return R.drawable.good;
        }

        if (aqiIndex <= 100){
            return R.drawable.moderate;
        }

        if (aqiIndex <= 150){
            return R.drawable.unhealthy_sensitive;
        }

        if (aqiIndex <= 200){
            return R.drawable.unhealthy;
        }

        if (aqiIndex <= 300){
            return R.drawable.very_unhealthy;
        }

        if(aqiIndex <=500){
            return R.drawable.hazardous;
        }

        return R.drawable.hazardous;
    }

    public static int getUVDrawableId(int uvIndex) {
        switch(uvIndex) {
            case 0:
            case 1:
            case 2:
                return R.drawable.low;
            case 3:
            case 4:
            case 5:
                return R.drawable.moderate;
            case 6:
            case 7:
                return R.drawable.high;
            case 8:
            case 9:
            case 10:
                return R.drawable.very_high;
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
                return R.drawable.extreme;
            default:
                return R.drawable.low;
        }
    }

    public static int getWeatherIconId(Icon icon, boolean isForecast) {

        switch (icon) {
            case CLEAR_DAY:
                return R.drawable.yw_34_fair_day;
            case CLEAR_NIGHT://tropical storm
                return isForecast ? R.drawable.yw_32_sunny : R.drawable.yw_31_clear_night;
            case RAIN:
                return R.drawable.yw_12_showers;
            case SNOW://severe_thunderstorms
                return R.drawable.yw_13_snow;
            case SLEET://thunderstorms
                return R.drawable.yw_18_sleet;
            case WIND:
                return R.drawable.yw_24_windy;
            case FOG://mixed rain and sleet
                return R.drawable.yw_20_foggy;
            case CLOUDY://mixed snow and sleet
                return R.drawable.yw_26_cloudy;
            case PARTLY_CLOUDY_DAY://freezing drizzle
                return R.drawable.yw_30_partly_cloudy_day;
            case PARTLY_CLOUDY_NIGHT:
                return isForecast ? R.drawable.yw_30_partly_cloudy_day : R.drawable.yw_29_partly_cloudy_night;
            case HAIL:
                return R.drawable.yw_17_hail;
            case THUNDERSTORM://showers
                return R.drawable.yw_04_thunderstorms;
            case TORNADO://showers
                return R.drawable.yw_00_tornado;
            default:
                return R.drawable.ic_launcher;
        }
    }
}
