package com.hpe.usps.solavi.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.R;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by chishobr
 */
public class SplashActivity extends Activity {

    private static final String ALIVE = "ALIVE";
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferences.Editor editor = BaseApplication.getInstance().getSharedPrefs().edit();
        editor.putBoolean(ALIVE, true);
        editor.commit();

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                boolean alive = BaseApplication.getInstance().getSharedPrefs().getBoolean(ALIVE, false);
                if (alive) {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                }

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


    @Override
    public void onPause(){
        SharedPreferences.Editor editor = BaseApplication.getInstance().getSharedPrefs().edit();
        editor.putBoolean(ALIVE, false);
        editor.commit();
        super.onPause();
    }

    @Override
    public void onStop(){
        SharedPreferences.Editor editor = BaseApplication.getInstance().getSharedPrefs().edit();
        editor.putBoolean(ALIVE, false);
        editor.commit();
        super.onStop();
    }

}
