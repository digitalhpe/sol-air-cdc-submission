package com.hpe.usps.solavi.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.helpers.ResourceHelper;

/**
 * Created by chishobr
 */
public class SelectedCell extends RelativeLayout {

    private final ImageView background;
    private final TextView valueText;
    private final ImageView descriptionIcon;

    public SelectedCell(Context context) {
        this(context,null);
    }
    public SelectedCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        setGravity(Gravity.CENTER);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_selected_cell, this, true);

        background = (ImageView) getChildAt(0);

        valueText = (TextView) getChildAt(1);
        descriptionIcon = (ImageView) getChildAt(5);
    }

    public void setColorAndValue(int index, boolean isAqi) {
        if (isAqi){
            background.setBackgroundColor(getResources().getColor(ResourceHelper.getAqiColorIdByIndex(index)));
            descriptionIcon.setImageDrawable(getResources().getDrawable(ResourceHelper.getAqiDrawableId(index)));
            if (index<=100){
                valueText.setTextColor(getResources().getColor(R.color.black));
            }else{
                valueText.setTextColor(getResources().getColor(R.color.white));
            }
            valueText.setText(Integer.toString(index));
        }else {
            background.setBackgroundColor(getResources().getColor(ResourceHelper.getUVColorIdByIndex(index)));
            descriptionIcon.setImageDrawable(getResources().getDrawable(ResourceHelper.getUVDrawableId(index)));
            if (index<=5){
                valueText.setTextColor(getResources().getColor(R.color.black));
            }else{
                valueText.setTextColor(getResources().getColor(R.color.white));
            }
            valueText.setText(Integer.toString(index));
        }

    }
}
