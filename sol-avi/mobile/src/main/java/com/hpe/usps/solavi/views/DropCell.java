package com.hpe.usps.solavi.views;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.Html;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hpe.usps.solavi.R;

/**
 * Created by chishobr
 */
public class DropCell extends RelativeLayout {

    private TextView titleTv, valueTv;
    private ImageView chevronImage;
    private RelativeLayout dropLayout;

    public DropCell(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.DropCell, 0, 0);
        String titleText = a.getString(R.styleable.DropCell_custom_title);
        String valueText = a.getString(R.styleable.DropCell_custom_value);
        a.recycle();

        setGravity(Gravity.CENTER);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.view_drop_cell, this, true);

        titleTv = (TextView) v.findViewById(R.id.title_tv);
        titleTv.setText(titleText);
        chevronImage = (ImageView) v.findViewById(R.id.chevron);
        dropLayout = (RelativeLayout) v.findViewById(R.id.drop_layout);
        dropLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleChevron();
            }
        });


        valueTv = (TextView) v.findViewById(R.id.value_tv);
        valueTv.setText(Html.fromHtml(valueText));
        setTopColor(titleText);

    }

    public DropCell(Context context) {
        this(context, null);
    }

    /**
     *
     */
    public void toggleChevron() {
        if (valueTv.getVisibility() == View.VISIBLE){
            valueTv.setVisibility(View.GONE);
            chevronImage.setRotation(0);
        }else{
            valueTv.setVisibility(View.VISIBLE);
            chevronImage.setRotation(180);
        }
    }

    public void setTopColor(String titleText) {
        Resources res = getResources();
        if (titleText.equals(res.getString(R.string.aqi_good_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.aqi_green));
            titleTv.setTextColor(res.getColor(R.color.black));
        }else if (titleText.equals(res.getString(R.string.aqi_moderate_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.aqi_yellow));
            titleTv.setTextColor(res.getColor(R.color.black));
        }else if (titleText.equals(res.getString(R.string.aqi_unhealthy_sensitive_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.aqi_orange));
            titleTv.setTextColor(res.getColor(R.color.white));
        }else if (titleText.equals(res.getString(R.string.aqi_unhealthy_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.aqi_red));
            titleTv.setTextColor(res.getColor(R.color.white));
        }else if (titleText.equals(res.getString(R.string.aqi_very_unhealthy_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.aqi_purple));
            titleTv.setTextColor(res.getColor(R.color.white));
        }else if (titleText.equals(res.getString(R.string.aqi_hazardous_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.aqi_magenta));
            titleTv.setTextColor(res.getColor(R.color.white));
        }else if (titleText.equals(res.getString(R.string.uv_low_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.uv_green));
            titleTv.setTextColor(res.getColor(R.color.black));
        }else if (titleText.equals(res.getString(R.string.uv_moderate_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.uv_yellow));
            titleTv.setTextColor(res.getColor(R.color.black));
        }else if (titleText.equals(res.getString(R.string.uv_high_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.uv_orange));
            titleTv.setTextColor(res.getColor(R.color.white));
        }else if (titleText.equals(res.getString(R.string.uv_very_high_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.uv_red));
            titleTv.setTextColor(res.getColor(R.color.white));
        }else if (titleText.equals(res.getString(R.string.uv_extreme_info))){
            dropLayout.setBackgroundColor(res.getColor(R.color.uv_purple));
            titleTv.setTextColor(res.getColor(R.color.white));
        }else if (titleText.equals(res.getString(R.string.the_shadow_rule))){
            dropLayout.setBackgroundColor(res.getColor(R.color.icon_gray));
            titleTv.setTextColor(res.getColor(R.color.white));
        }
    }
}
