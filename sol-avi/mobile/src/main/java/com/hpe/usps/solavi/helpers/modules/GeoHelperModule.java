package com.hpe.usps.solavi.helpers.modules;

import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.fragments.LocationInfoFragment;
import com.hpe.usps.solavi.fragments.LocationMapFragment;
import com.hpe.usps.solavi.fragments.MainActivityFragment;
import com.hpe.usps.solavi.helpers.AndroidGeoHelper;
import com.hpe.usps.solavi.services.WeatherService;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andy Jones on 12/10/2015.
 */
@Module(
        injects = {
                MainActivityFragment.class,
                LocationMapFragment.class,
                LocationInfoFragment.class,
                WeatherService.class
        },
        library = true,
        complete = false
)
public class GeoHelperModule {

    private BaseApplication mApp;

    public GeoHelperModule( BaseApplication app) {
        mApp = app;
    }

    @Provides
    @Named("geoHelper")
    GeoHelper providesGeoHelper() {
        return new AndroidGeoHelper( mApp);
    }
}
