package com.hpe.usps.solavi.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.helpers.ResourceHelper;

/**
 * Created by chishobr
 */
public class InfoCell extends RelativeLayout {

    private final ImageView background;
    private final ImageView arrow;

    public InfoCell(Context context) {
        this(context,null);
    }
    public InfoCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        setGravity(Gravity.CENTER);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_info_cell, this, true);

        background = (ImageView) getChildAt(0);

        arrow = (ImageView) getChildAt(4);
    }

    public void setColor(int index, boolean isAqi) {
        int colorId = 0;
        if (isAqi){
            colorId = ResourceHelper.getAqiColorIdByIndex(index);
        }else {
            colorId = ResourceHelper.getUVColorIdByIndex(index);
        }
        background.setBackgroundColor(getResources().getColor(colorId));
        arrow.setColorFilter(getResources().getColor(colorId), PorterDuff.Mode.SRC_ATOP);
    }
}
