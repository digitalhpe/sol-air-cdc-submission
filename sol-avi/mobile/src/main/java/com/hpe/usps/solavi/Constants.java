package com.hpe.usps.solavi;

/**
 * Created by Andy Jones on 12/9/2015.
 */
public class Constants {
    //GCM_SENDER_ID is acquired by configuring a project with Cloud Messaging for Android using
    //the Google Developer Console: https://console.developers.google.com
    public static final String DARK_SKY_API_KEY = "DARK_SKY_API_KEY";
    public static final String GCM_SENDER_ID = "GCM_SENDER_ID";
    public static final String RESPONSE_OK = "OK";

    private Constants() {}
}
