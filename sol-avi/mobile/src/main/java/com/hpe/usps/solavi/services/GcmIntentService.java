package com.hpe.usps.solavi.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hpe.usps.solavi.receivers.GcmBroadcastReceiver;
import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.MainActivity;

/**
 * Handles the receiving of push notifications and displaying app notifications.
 *
 * Created by Andy Jones on 12/9/15.
 */
public class GcmIntentService extends IntentService {

    private static final String TAG = GcmIntentService.class.getSimpleName();

    private static final String GCM_NOTIFICATION_TITLE = "gcm.notification.title";
    private static final String GCM_NOTIFICATION_BODY = "gcm.notification.body";
    public static final int NOTIFICATION_ID = 1000;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        if (!extras.isEmpty()) {  // has effect of unpacking Bundle
            switch (messageType) {
                case GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR:
                    sendNotification("Send error: " + extras.toString(), "");
                    break;
                case GoogleCloudMessaging.MESSAGE_TYPE_DELETED:
                    sendNotification("Deleted messages on server: " + extras.toString(), "");
                    // If it's a regular GCM message, do some work.
                    break;
                case GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE:
                    // Post notification of received message.
                    String title = null;
                    String body = null;

                    if (extras.containsKey(GCM_NOTIFICATION_TITLE)) {
                        title = extras.getString(GCM_NOTIFICATION_TITLE);
                    }

                    if (extras.containsKey(GCM_NOTIFICATION_BODY)) {
                        body = extras.getString(GCM_NOTIFICATION_BODY);
                    }

                    sendNotification(title, body);
                    Log.i(TAG, "Received: " + extras.toString());
                    break;
                default:
                    break;
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    /**
     * Displays a notification when a GCM message is received
     * @param title message ticker text to display
     * @param body message text to display
     */
    private void sendNotification(String title, String body) {
        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent;
        Class clazz = MainActivity.class;

        Intent i = new Intent(this, clazz);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        contentIntent = PendingIntent.getActivity(this, 0, i, 0);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notify)
                        .setTicker(title)
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(body))
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(alarmSound);

        builder.setContentIntent(contentIntent);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}