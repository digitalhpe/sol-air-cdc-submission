package com.hpe.usps.solavi.views;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hpe.usps.solavi.R;

/**
 * Displays a helpful table of rating titles and the matching descriptions or instructions per the EPA guidelines
 * Created by chishobr on 12/16/2015.
 */
public class AqiInfoAlertDialog  extends DialogFragment {

    CustomScaleLayout aqiGood, aqiModerate, aqiUnhealthySg, aqiUnhealthy, aqiVeryUnhealthy, aqiHazardous;
    CustomScaleLayout uvLow, uvModerate, uvHigh, uvVeryHigh, uvExtreme;
    InfoCell uvInfoCell, aqiInfoCell;
    LinearLayout uvScale, aqiScale;
    TextView uvDescTitle, uvDescValue, aqiDescTitle, aqiDescValue, shadowHtml;
    int width = 0;
    private int lastUvPosition = 0, lastAqiPosition = 0;
    private float scale;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.aqi_info_layout, container,
                false);


        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        uvInfoCell = (InfoCell)rootView.findViewById(R.id.uv_info_cell);
        uvInfoCell.setColor(1,false);
        aqiInfoCell = (InfoCell)rootView.findViewById(R.id.aqi_info_cell);
        aqiInfoCell.setColor(1,true);
        uvScale = (LinearLayout)rootView.findViewById(R.id.uv_gauge);
        aqiScale = (LinearLayout)rootView.findViewById(R.id.aqi_gauge);
        scale = getContext().getResources().getDisplayMetrics().density;
        shadowHtml = (TextView)rootView.findViewById(R.id.shadow_text_easier_way);

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int padding = (int) (40 * scale + 0.5f);
                width = rootView.getWidth()-padding;
                inflateAQIScale();
                inflateUVScale();
                setCells(rootView);
                setDefaultInstructions();
            }
        });
        return rootView;
    }

    private void setDefaultInstructions() {
        uvDescTitle.setText(R.string.uv_low_info);
        uvDescValue.setText(Html.fromHtml(getString(R.string.uv_instructions_low)));
        aqiDescTitle.setText(R.string.aqi_good_info);
        aqiDescValue.setText(Html.fromHtml(getString(R.string.aqi_instructions_good)));

        shadowHtml.setText(Html.fromHtml(getString(R.string.uv_instructions_shadow)));
    }




    private void setCells(View rootView) {

        aqiGood = (CustomScaleLayout) rootView.findViewById(R.id.csl_aqi_level_1);
        aqiGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqiInfoCell.setColor(1,true);
                slideAQI(0);
                aqiDescTitle.setText(R.string.aqi_good_info);
                aqiDescValue.setText(Html.fromHtml(getString(R.string.aqi_instructions_good)));
            }
        });
        aqiModerate = (CustomScaleLayout) rootView.findViewById(R.id.csl_aqi_level_2);
        aqiModerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqiInfoCell.setColor(51,true);
                slideAQI(1);
                aqiDescTitle.setText(R.string.aqi_moderate_info);
                aqiDescValue.setText(Html.fromHtml(getString(R.string.aqi_instructions_moderate)));
            }
        });
        aqiUnhealthySg = (CustomScaleLayout) rootView.findViewById(R.id.csl_aqi_level_3);
        aqiUnhealthySg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqiInfoCell.setColor(101,true);
                slideAQI(2);
                aqiDescTitle.setText(R.string.aqi_unhealthy_sensitive_info);
                aqiDescValue.setText(Html.fromHtml(getString(R.string.aqi_instructions_unhealthy_sensitive)));
            }
        });
        aqiUnhealthy = (CustomScaleLayout) rootView.findViewById(R.id.csl_aqi_level_4);
        aqiUnhealthy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqiInfoCell.setColor(151,true);
                slideAQI(3);
                aqiDescTitle.setText(R.string.aqi_unhealthy_info);
                aqiDescValue.setText(Html.fromHtml(getString(R.string.aqi_instructions_unhealthy)));
            }
        });
        aqiVeryUnhealthy = (CustomScaleLayout) rootView.findViewById(R.id.csl_aqi_level_5);
        aqiVeryUnhealthy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqiInfoCell.setColor(201,true);
                slideAQI(4);
                aqiDescTitle.setText(R.string.aqi_very_unhealthy_info);
                aqiDescValue.setText(Html.fromHtml(getString(R.string.aqi_instructions_very_unhealthy)));
            }
        });
        aqiHazardous = (CustomScaleLayout) rootView.findViewById(R.id.csl_aqi_level_6);
        aqiHazardous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aqiInfoCell.setColor(301,true);
                slideAQI(5);
                aqiDescTitle.setText(R.string.aqi_hazardous_info);
                aqiDescValue.setText(Html.fromHtml(getString(R.string.aqi_instructions_hazardous)));
            }
        });


        uvLow = (CustomScaleLayout) rootView.findViewById(R.id.csl_uv_low);
        uvLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uvInfoCell.setColor(1,false);
                slideUV(0);
                uvDescTitle.setText(R.string.uv_low_info);
                uvDescValue.setText(Html.fromHtml(getString(R.string.uv_instructions_low)));
            }
        });
        uvModerate = (CustomScaleLayout) rootView.findViewById(R.id.csl_uv_moderate);
        uvModerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uvInfoCell.setColor(4,false);
                slideUV(1);
                uvDescTitle.setText(R.string.uv_moderate_info);
                uvDescValue.setText(Html.fromHtml(getString(R.string.uv_instructions_moderate)));
            }
        });
        uvHigh = (CustomScaleLayout) rootView.findViewById(R.id.csl_uv_high);
        uvHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uvInfoCell.setColor(6,false);
                slideUV(2);
                uvDescTitle.setText(R.string.uv_high_info);
                uvDescValue.setText(Html.fromHtml(getString(R.string.uv_instructions_high)));
            }
        });
        uvVeryHigh = (CustomScaleLayout) rootView.findViewById(R.id.csl_uv_very_high);
        uvVeryHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uvInfoCell.setColor(9,false);
                slideUV(3);
                uvDescTitle.setText(R.string.uv_very_high_info);
                uvDescValue.setText(Html.fromHtml(getString(R.string.uv_instructions_very_high)));
            }
        });
        uvExtreme = (CustomScaleLayout) rootView.findViewById(R.id.csl_uv_extreme);
        uvExtreme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uvInfoCell.setColor(11,false);
                slideUV(4);
                uvDescTitle.setText(R.string.uv_extreme_info);
                uvDescValue.setText(Html.fromHtml(getString(R.string.uv_instructions_extreme)));
            }
        });

        //TODO: shadow rule?

        uvDescTitle = (TextView)rootView.findViewById(R.id.title_uv_description);
        uvDescValue = (TextView)rootView.findViewById(R.id.value_uv_description);
        aqiDescTitle = (TextView)rootView.findViewById(R.id.title_aqi_description);
        aqiDescValue = (TextView)rootView.findViewById(R.id.value_aqi_description);
    }

    public void inflateAQIScale() {

        RelativeLayout.LayoutParams layoutParams;
        layoutParams = (RelativeLayout.LayoutParams) aqiInfoCell.getLayoutParams();
        int scaleWidth = width/6;
        layoutParams.width = (int)(scaleWidth+33*scale+0.5f);
        aqiInfoCell.setLayoutParams(layoutParams);

    }

    public void inflateUVScale() {

        RelativeLayout.LayoutParams layoutParams;
        layoutParams = (RelativeLayout.LayoutParams) uvInfoCell.getLayoutParams();
        int scaleWidth = width/5;
        layoutParams.width = (int)(scaleWidth+33*scale+0.5f);
        uvInfoCell.setLayoutParams(layoutParams);

    }
    public void slideAQI(int position){

        int aqiCellWidth = width/6;
        int pixelsOver = (position-lastAqiPosition)*aqiCellWidth;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)aqiInfoCell.getLayoutParams();
        params.leftMargin += pixelsOver;
        aqiInfoCell.setLayoutParams(params);
        lastAqiPosition = position;

    }

    public void slideUV(int position){
        int uvCellWidth = width/5;
        int pixelsOver = (position-lastUvPosition)*uvCellWidth;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)uvInfoCell.getLayoutParams();
        params.leftMargin += pixelsOver;
        uvInfoCell.setLayoutParams(params);
        lastUvPosition = position;
    }
}
