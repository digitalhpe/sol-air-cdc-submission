package com.hpe.usps.solavi.services;

import android.Manifest;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.models.DataPoint;
import android.zetterstrom.com.forecast.models.Forecast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.Injector;
import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.SplashActivity;
import com.hpe.usps.solavi.helpers.ResourceHelper;
import com.hpe.usps.solavi.views.WeatherWidget;

import com.hpe.usps.solavilibrary.helpers.GeoHelper;
import com.hpe.usps.solavilibrary.helpers.LocationHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by chishobr
 */
public class WeatherService extends Service implements LocationListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    @Inject
    @Named("airNowService")
    AirNowService airNowService;

    @Inject
    @Named("epaService")
    EPAService epaService;

    @Inject
    @Named("placesService")
    PlacesService placesService;

    @Inject
    @Named("solaviService")
    SolaviService solaviService;

    @Inject
    @Named("geoHelper")
    GeoHelper geoHelper;

    @Inject
    @Named("preferenceHelper")
    PreferenceHelper preferenceHelper;

    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private int[] widgetIds;

    private CompositeSubscription subscriptions = new CompositeSubscription();
    private LocationHelper locationHelper;

    private static boolean epaSet, weatherSet, citySet;

    @Override
    public void onCreate() {
        super.onCreate();
        //Initialize Google API client build and connect
        buildGoogleApiClient();
        Injector.inject(this);
        epaSet = weatherSet = citySet = false;
        locationHelper = new LocationHelper(preferenceHelper, geoHelper, epaService, airNowService, solaviService, placesService);


        googleApiClient.connect();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        widgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

        if (widgetIds != null) {
            buildUpdate(widgetIds);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void buildUpdate(int... appwidget_ids) {
        final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());


        for (final int appwidget_id : appwidget_ids) {
            if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    ) {

            }else {
                showUpdating(appwidget_id);
                if (getNumRows() > 1) {
                    final RemoteViews remoteViews = new RemoteViews(WeatherService.this.getApplicationContext().getPackageName(), R.layout.widget_layout);
                        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

                        if (location != null) {
                            subscriptions.add(
                                    geoHelper.getLocationAddress(location.getLatitude(), location.getLongitude(), true)
                                            .subscribeOn(Schedulers.newThread())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new Subscriber<String>() {
                                                @Override
                                                public void onCompleted() {
                                                    //unused
                                                }

                                                @Override
                                                public void onError(Throwable e) {
                                                    if (getNumRows() > 1) {
                                                        remoteViews.setTextViewText(R.id.city_name, getString(R.string.error_finding_address));
                                                        appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                                                    }
                                                }

                                                @Override
                                                public void onNext(String s) {
                                                    if (getNumRows() > 1) {
                                                        remoteViews.setTextViewText(R.id.city_name, s);
                                                        citySet = true;
                                                        appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                                                        loadForecast(location.getLatitude(), location.getLongitude());
                                                    }
                                                }
                                            })
                            );
                            subscriptions.add(
                                    locationHelper.getIndexesForLocation(location.getLatitude(), location.getLongitude())
                                            .subscribeOn(Schedulers.newThread())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new Subscriber<com.hpe.usps.solavilibrary.models.Location>() {
                                                @Override
                                                public final void onCompleted() {
                                                    //unused
                                                }

                                                @Override
                                                public final void onError(Throwable e) {
                                                    if (getNumRows() > 1) {
                                                        remoteViews.setViewVisibility(R.id.epa_info_progress, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_layout, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_missing_layout, View.VISIBLE);
                                                        if (!weatherSet) {
                                                            remoteViews.setViewVisibility(R.id.weather_progress, View.GONE);
                                                            remoteViews.setViewVisibility(R.id.weather_missing_layout, View.VISIBLE);
                                                            remoteViews.setViewVisibility(R.id.weather_info_layout, View.GONE);
                                                        }
                                                        appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                                                    }
                                                }

                                                @Override
                                                public final void onNext(com.hpe.usps.solavilibrary.models.Location response) {
                                                    if (getNumRows() > 1) {
                                                        int ozone = response.getOzone();
                                                        int uvi = response.getUv();

                                                        //turn off progress bars
                                                        remoteViews.setViewVisibility(R.id.epa_info_progress, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_missing_layout, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_layout, View.VISIBLE);

                                                        //fill values
                                                        remoteViews.setTextViewText(R.id.ozone_title, Html.fromHtml("O<sub><small><small>3</small></small></sub>"));

                                                        remoteViews.setTextViewText(R.id.ozone_value, String.valueOf(ozone));
                                                        remoteViews.setTextViewText(R.id.uvi_value, String.valueOf(uvi));

                                                        remoteViews.setInt(R.id.ozone_value, "setBackgroundColor",
                                                                ContextCompat.getColor(WeatherService.this.getApplicationContext(), ResourceHelper.getAqiPastelColorIdByIndex(ozone)));
                                                        remoteViews.setInt(R.id.uvi_value, "setBackgroundColor",
                                                                ContextCompat.getColor(WeatherService.this.getApplicationContext(), ResourceHelper.getUVPastelColorIdByIndex(uvi)));

                                                        if (ozone >= 150) {
                                                            remoteViews.setTextColor(R.id.ozone_value,
                                                                    ContextCompat.getColor(WeatherService.this.getApplicationContext(), R.color.white));
                                                        } else {
                                                            remoteViews.setTextColor(R.id.ozone_value,
                                                                    ContextCompat.getColor(WeatherService.this.getApplicationContext(), R.color.black));
                                                        }

                                                        if (uvi >= 2) {
                                                            remoteViews.setTextColor(R.id.uvi_value,
                                                                    ContextCompat.getColor(WeatherService.this.getApplicationContext(), R.color.white));
                                                        } else {
                                                            remoteViews.setTextColor(R.id.uvi_value,
                                                                    ContextCompat.getColor(WeatherService.this.getApplicationContext(), R.color.black));
                                                        }

                                                        //Descriptions
                                                        remoteViews.setImageViewResource(R.id.ozone_description, ResourceHelper.getAqiDrawableId(ozone));
                                                        remoteViews.setImageViewResource(R.id.uvi_description, ResourceHelper.getUVDrawableId(uvi));
                                                        epaSet = true;

                                                        appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                                                    }
                                                }
                                            })
                            );
                        }

                    Intent intent = new Intent(this.getApplicationContext(), SplashActivity.class);
                    PendingIntent pendingLaunchIntent = PendingIntent.getActivity(this.getApplicationContext(), 0, intent, 0);

                    remoteViews.setOnClickPendingIntent(R.id.widget_container, pendingLaunchIntent);

                    appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                } else {
                    final RemoteViews remoteViews = new RemoteViews(WeatherService.this.getApplicationContext().getPackageName(), R.layout.widget_layout_small);
                        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                        if (location != null) {
                            subscriptions.add(
                                    geoHelper.getLocationAddress(location.getLatitude(), location.getLongitude(), true)
                                            .subscribeOn(Schedulers.newThread())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new Subscriber<String>() {
                                                @Override
                                                public void onCompleted() {
                                                    //unused
                                                }

                                                @Override
                                                public void onError(Throwable e) {
                                                    if (getNumRows() == 1) {
                                                        remoteViews.setTextViewText(R.id.city_name, getString(R.string.error_finding_address));
                                                        appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                                                    }
                                                }

                                                @Override
                                                public void onNext(String s) {
                                                    if (getNumRows() == 1) {
                                                        remoteViews.setTextViewText(R.id.city_name, s);
                                                        citySet = true;
                                                        appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                                                    }
                                                }
                                            })
                            );
                            subscriptions.add(
                                    locationHelper.getIndexesForLocation(location.getLatitude(), location.getLongitude())
                                            .subscribeOn(Schedulers.newThread())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new Subscriber<com.hpe.usps.solavilibrary.models.Location>() {
                                                @Override
                                                public final void onCompleted() {
                                                    //unused
                                                }

                                                @Override
                                                public final void onError(Throwable e) {
                                                    if (getNumRows() == 1) {
                                                        remoteViews.setViewVisibility(R.id.epa_info_uv_progress, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_progress, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_uvi_layout, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_ozone_layout, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_uv_unavail, View.VISIBLE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_aqi_unavail, View.VISIBLE);
                                                        appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                                                    }
                                                }

                                                @Override
                                                public final void onNext(com.hpe.usps.solavilibrary.models.Location response) {
                                                    if (getNumRows() == 1) {
                                                        int ozone = response.getOzone();
                                                        int uvi = response.getUv();

                                                        //turn off progress bars
                                                        remoteViews.setViewVisibility(R.id.epa_info_uv_progress, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_progress, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_uvi_layout, View.VISIBLE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_ozone_layout, View.VISIBLE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_uv_unavail, View.GONE);
                                                        remoteViews.setViewVisibility(R.id.epa_info_aqi_unavail, View.GONE);

                                                        //fill values
                                                        remoteViews.setTextViewText(R.id.ozone_title, Html.fromHtml("O<sub><small><small>3</small></small></sub>"));

                                                        remoteViews.setTextViewText(R.id.ozone_value, String.valueOf(ozone));
                                                        remoteViews.setTextViewText(R.id.uvi_value, String.valueOf(uvi));

                                                        remoteViews.setInt(R.id.ozone_value, "setBackgroundColor",
                                                                ContextCompat.getColor(WeatherService.this.getApplicationContext(), ResourceHelper.getAqiPastelColorIdByIndex(ozone)));
                                                        remoteViews.setInt(R.id.uvi_value, "setBackgroundColor",
                                                                ContextCompat.getColor(WeatherService.this.getApplicationContext(), ResourceHelper.getUVPastelColorIdByIndex(uvi)));

                                                        if (ozone >= 150) {
                                                            remoteViews.setTextColor(R.id.ozone_value,
                                                                    ContextCompat.getColor(WeatherService.this.getApplicationContext(), R.color.white));
                                                        } else {
                                                            remoteViews.setTextColor(R.id.ozone_value,
                                                                    ContextCompat.getColor(WeatherService.this.getApplicationContext(), R.color.black));
                                                        }

                                                        if (uvi >= 2) {
                                                            remoteViews.setTextColor(R.id.uvi_value,
                                                                    ContextCompat.getColor(WeatherService.this.getApplicationContext(), R.color.white));
                                                        } else {
                                                            remoteViews.setTextColor(R.id.uvi_value,
                                                                    ContextCompat.getColor(WeatherService.this.getApplicationContext(), R.color.black));
                                                        }
                                                        epaSet = true;

                                                        appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                                                    }
                                                }

                                            })
                            );
                        }
                    Intent intent = new Intent(this.getApplicationContext(), SplashActivity.class);
                    PendingIntent pendingLaunchIntent = PendingIntent.getActivity(this.getApplicationContext(), 0, intent, 0);

                    remoteViews.setOnClickPendingIntent(R.id.widget_container, pendingLaunchIntent);

                    appWidgetManager.updateAppWidget(appwidget_id, remoteViews);


                }
            }

        }
        stopSelf();

    }



    private void showUpdating(int appWidgetId) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());
        RemoteViews remoteViews;
        if (getNumRows()>1){
            //normal
            remoteViews = new RemoteViews(WeatherService.this.getApplicationContext().getPackageName(), R.layout.widget_layout);
            if (!epaSet) {
                remoteViews.setViewVisibility(R.id.epa_info_progress, View.VISIBLE);
                remoteViews.setViewVisibility(R.id.epa_info_layout, View.GONE);
                remoteViews.setViewVisibility(R.id.epa_missing_layout, View.GONE);
            }
            if (!weatherSet) {
                remoteViews.setViewVisibility(R.id.weather_progress, View.VISIBLE);
                remoteViews.setViewVisibility(R.id.weather_missing_layout, View.GONE);
                remoteViews.setViewVisibility(R.id.weather_info_layout, View.GONE);
            }
        }else{
            //small
            remoteViews = new RemoteViews(WeatherService.this.getApplicationContext().getPackageName(), R.layout.widget_layout_small);
            if (!epaSet) {
                remoteViews.setViewVisibility(R.id.epa_info_uv_progress, View.VISIBLE);
                remoteViews.setViewVisibility(R.id.epa_info_progress, View.VISIBLE);
                remoteViews.setViewVisibility(R.id.epa_info_uvi_layout, View.GONE);
                remoteViews.setViewVisibility(R.id.epa_info_ozone_layout, View.GONE);
                remoteViews.setViewVisibility(R.id.epa_info_uv_unavail, View.GONE);
                remoteViews.setViewVisibility(R.id.epa_info_aqi_unavail, View.GONE);
            }
        }
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }


    private int getNumRows() {
        return BaseApplication.getInstance().getSharedPrefs().getInt(WeatherWidget.NUM_ROWS,2);
    }

    private void loadForecast(double latitude, double longitude){
        ForecastClient.getInstance()
                .getForecast(latitude, longitude, new Callback<Forecast>() {
                    @Override
                    public void onResponse(Call<Forecast> forecastCall, Response<Forecast> response) {
                        if (response.isSuccessful()) {
                            Forecast forecast = response.body();
                            populateWeather(forecast);
                        }
                    }

                    @Override
                    public void onFailure(Call<Forecast> forecastCall, Throwable t) {

                    }
                });
    }

    private void populateWeather(Forecast forecast) {

        DataPoint current = forecast.getCurrently();
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());

        if (widgetIds != null) {
            weatherSet = true;
            for (final int appwidget_id : widgetIds) {
                RemoteViews remoteViews = new RemoteViews(WeatherService.this.getApplicationContext().getPackageName(), R.layout.widget_layout);

                remoteViews.setViewVisibility(R.id.weather_progress, View.GONE);
                remoteViews.setViewVisibility(R.id.weather_missing_layout, View.GONE);
                remoteViews.setViewVisibility(R.id.weather_info_layout, View.VISIBLE);
                remoteViews.setImageViewResource(R.id.weather_icon, ResourceHelper.getWeatherIconId(current.getIcon(), false));
                remoteViews.setTextViewText(R.id.weather_temp, (int) Math.round(current.getTemperature()) + "\u00B0");
                appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
            }

        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void startLocationUpdates() {
        createLocationRequest();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    protected void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Get Last Location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location loc = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        //Start getting periodic location updates
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        showNoConnection(getString(R.string.no_internet), R.drawable.ic_portable_wifi_off_white);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (widgetIds != null) {
            buildUpdate(widgetIds);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        showNoConnection(getString(R.string.no_internet), R.drawable.ic_portable_wifi_off_white);
    }

    private void showNoConnection(String error, int errorId){
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());

        if (widgetIds != null) {
            for (final int appwidget_id : widgetIds) {


                if (getNumRows()>1) {

                    RemoteViews remoteViews = new RemoteViews(WeatherService.this.getApplicationContext().getPackageName(), R.layout.widget_layout);
                    if (!weatherSet){
                        remoteViews.setViewVisibility(R.id.weather_progress, View.GONE);
                        remoteViews.setViewVisibility(R.id.weather_info_layout, View.GONE);
                        remoteViews.setViewVisibility(R.id.weather_missing_layout, View.VISIBLE);
                        remoteViews.setImageViewResource(R.id.weather_missing_layout, errorId);
                    }
                    if (!epaSet){
                        remoteViews.setImageViewResource(R.id.epa_missing_layout, errorId);
                        remoteViews.setViewVisibility(R.id.epa_missing_layout, View.VISIBLE);
                        remoteViews.setViewVisibility(R.id.epa_info_layout, View.GONE);
                        remoteViews.setViewVisibility(R.id.epa_info_progress, View.GONE);
                    }
                    if (!citySet){
                        remoteViews.setTextViewText(R.id.city_name, error);
                    }
                    appWidgetManager.updateAppWidget(appwidget_id, remoteViews);

                }else{
                    RemoteViews remoteViews = new RemoteViews(WeatherService.this.getApplicationContext().getPackageName(), R.layout.widget_layout_small);
                    if (!epaSet) {
                        remoteViews.setViewVisibility(R.id.epa_info_uv_unavail, View.VISIBLE);
                        remoteViews.setViewVisibility(R.id.epa_info_aqi_unavail, View.VISIBLE);
                        remoteViews.setImageViewResource(R.id.epa_info_aqi_unavail, errorId);
                        remoteViews.setImageViewResource(R.id.epa_info_uv_unavail, errorId);
                        remoteViews.setViewVisibility(R.id.epa_info_uv_progress, View.GONE);
                        remoteViews.setViewVisibility(R.id.epa_info_progress, View.GONE);
                        remoteViews.setViewVisibility(R.id.epa_info_ozone_layout, View.GONE);
                        remoteViews.setViewVisibility(R.id.epa_info_uvi_layout, View.GONE);
                    }
                    if (!citySet) {
                        remoteViews.setTextViewText(R.id.city_name, error);
                    }
                    appWidgetManager.updateAppWidget(appwidget_id, remoteViews);
                }

            }

        }
    }
}
