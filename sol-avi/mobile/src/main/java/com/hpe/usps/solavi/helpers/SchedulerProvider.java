package com.hpe.usps.solavi.helpers;

/**
 * Created by chishobr on 6/15/2017.
 */

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public abstract class SchedulerProvider {
    // this default SchedulerProvider does work on the background / IO thread and
    // subscribes on the Android UI thread.  A different SchedulerProvider that runs synchronously
    // can be used for running unit tests.
    public static final SchedulerProvider DEFAULT = new MainThreadSchedulerProvider();

    public abstract <T> Observable.Transformer<T, T> applySchedulers();

    private static class MainThreadSchedulerProvider extends SchedulerProvider {
        @Override
        public <T> Observable.Transformer<T, T> applySchedulers() {
            return observable -> observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    }
}
