package com.hpe.usps.solavi.helpers;

import com.hpe.usps.solavilibrary.models.Parks.ParkLocation;

import rx.Observable;

/**
 * Created by chishobr on 8/14/2017.
 */

public interface PlacesHelper {

    Observable<ParkLocation> getParkLocations(String latLng, int radius);
}
