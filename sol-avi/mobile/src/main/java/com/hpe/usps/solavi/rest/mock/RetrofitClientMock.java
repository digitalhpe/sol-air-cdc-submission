package com.hpe.usps.solavi.rest.mock;

import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hpe.usps.solavilibrary.models.AddLocationResponse;
import com.hpe.usps.solavilibrary.models.GenericResponse;
import com.hpe.usps.solavilibrary.models.LocationSummary;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.List;

import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Andy Jones on 12/8/2015.
 *
 * This mock is useful for development / testing if you wish to mock specific web service calls
 * to provide you with a fixed response.
 */
public class RetrofitClientMock implements Client {

    @Override
    public Response execute(Request request) throws IOException {
        Uri uri = Uri.parse(request.getUrl());

        Log.d("MOCK SERVER", "fetching uri: " + uri.toString());

        int status;
        String responseString;

        String path = uri.getPath();

        if (path.contains("/epa/summary")) {
            status = HttpURLConnection.HTTP_OK;
            LocationSummary response = new LocationSummary();
            response.setResult("ok");
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            responseString = gson.toJson(response);
        } else if (("/epa/location/add").equals(path)) {
            status = HttpURLConnection.HTTP_OK;
            AddLocationResponse response = new AddLocationResponse();
            response.setResult("ok");
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            responseString = gson.toJson(response);
        } else if (("/epa/location/remove").equals(path)) {
            status = HttpURLConnection.HTTP_OK;
            GenericResponse response = new GenericResponse();
            response.setResult("ok");
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            responseString = gson.toJson(response);
        } else {
            status = HttpURLConnection.HTTP_INTERNAL_ERROR;
            GenericResponse response = new GenericResponse();
            response.setResult("error");
            response.setMessage("Server Error");
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            responseString = gson.toJson(response);
        }
        List<Header> emptyList = Collections.emptyList();
        return new Response(request.getUrl(), status, "nothing", emptyList, new TypedByteArray("application/json", responseString.getBytes()));
    }
}
