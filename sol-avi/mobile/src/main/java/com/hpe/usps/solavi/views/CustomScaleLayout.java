package com.hpe.usps.solavi.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hpe.usps.solavi.R;

/**
 * A custom layout for an AQI/UVI cell
 * Created by chishobr on 12/10/2015.
 */
public class CustomScaleLayout extends RelativeLayout {

    private ImageView background;
    private Drawable valueColor;

    public CustomScaleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.CustomScaleLayout, 0, 0);
        valueColor = a.getDrawable(R.styleable.CustomScaleLayout_valueColor);
        a.recycle();

        setGravity(Gravity.CENTER);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_custom_scale_layout, this, true);

        background = (ImageView) getChildAt(0);
        background.setImageDrawable(valueColor);

    }

    public CustomScaleLayout(Context context) {
        this(context, null);
    }

}
