package com.hpe.usps.solavi.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.Constants;
import com.hpe.usps.solavi.Injector;
import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.MainActivity;
import com.hpe.usps.solavi.events.DeleteLocationEvent;
import com.hpe.usps.solavi.events.TokenEvent;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;
import com.hpe.usps.solavilibrary.helpers.LocationHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;
import com.hpe.usps.solavilibrary.models.Location;
import com.hpe.usps.solavilibrary.models.LocationSummary;
import com.hpe.usps.solavilibrary.models.RefreshLocationEvent;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;
import com.hpe.usps.solavi.views.LocationRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    @Inject
    @Named("solaviService")
    SolaviService solaviService;

    @Inject
    @Named("airNowService")
    AirNowService airNowService;

    @Inject
    @Named("placesService")
    PlacesService placesService;

    @Inject
    @Named("epaService")
    EPAService epaService;

    @Inject
    @Named("geoHelper")
    GeoHelper geoHelper;

    @Inject
    @Named("preferenceHelper")
    PreferenceHelper preferenceHelper;

    private LocationHelper locationHelper;
    private RecyclerView recyclerView;
    private LocationRecyclerAdapter recyclerAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private CompositeSubscription subscriptions = new CompositeSubscription();
    private ProgressBar progressBar;
    private boolean fetched = false;
    private ImageView fab;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Injector.inject(this);
        locationHelper = new LocationHelper(preferenceHelper, geoHelper, epaService, airNowService, solaviService, placesService);
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        setViews(view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets the views for the layout
     *
     * @param view The inflated view of the layout
     */
    private void setViews(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.locationList);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        List<Location> locations = new ArrayList<>();
        recyclerAdapter = new LocationRecyclerAdapter(locations, getActivity());
        recyclerView.setAdapter(recyclerAdapter);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        fab = (ImageView) view.findViewById(R.id.add_location);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).viewMap();
            }
        });
    }

    public void onEvent(RefreshLocationEvent event) {
        List<Location> locationsTemp = BaseApplication.getInstance().getSavedLocations();
        ArrayList<Location> locations = new ArrayList<>();
        locations.addAll(locationsTemp);
        android.location.Location loc = ((MainActivity) getActivity()).getLastKnownLocation();
        addCurrentLocation(loc, locations);
        setAdapter(locations);
    }

    public void onEvent(DeleteLocationEvent event) {
        List<Location> locationsTemp = BaseApplication.getInstance().getSavedLocations();
        ArrayList<Location> locations = new ArrayList<>();
        locations.addAll(locationsTemp);
        android.location.Location loc = ((MainActivity) getActivity()).getLastKnownLocation();
        addCurrentLocation(loc, locations);
        recyclerAdapter = null;
        setAdapter(locations);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {

        String pushToken = preferenceHelper.getPushToken();

        if (!pushToken.isEmpty()) {
            fetchData(pushToken);
            fetched = true;
        }
        super.onResume();
    }

    public void onEvent(TokenEvent event) {
        if (!event.getToken().isEmpty() && !fetched) {
            fetchData(event.getToken());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        subscriptions.unsubscribe();
    }

    /**
     * Retrieves the list of saved locations based on the push token generated by GCM when the phone
     * is registered.
     *
     * @param pushToken Google Cloud Messaging push token
     */
    private void fetchData(String pushToken) {

        if (!geoHelper.hasInternet(getActivity())) {
            List<Location> locations = new ArrayList<>();
            android.location.Location loc = ((MainActivity) getActivity()).getLastKnownLocation();
            addCurrentLocation(loc, locations);
            setAdapter(locations);
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        subscriptions.add(
                locationHelper.getLocations(pushToken)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<LocationSummary>() {
                            @Override
                            public final void onCompleted() {
                                //unused
                            }

                            @Override
                            public final void onError(Throwable e) {
                                Toast.makeText(BaseApplication.getInstance(), R.string.failed_to_load_locations, Toast.LENGTH_SHORT).show();
                                List<Location> locations = new ArrayList<>();
                                android.location.Location loc = ((MainActivity) getActivity()).getLastKnownLocation();
                                Log.d("WeatherLocation", "error");
                                addCurrentLocation(loc, locations);
                                setAdapter(locations);
                                progressBar.setVisibility(View.GONE);
                            }


                            @Override
                            public final void onNext(LocationSummary serverLocationSummary) {
                                if (serverLocationSummary.getResult().equals(Constants.RESPONSE_OK)) {
                                    BaseApplication.getInstance().setSavedLocations(serverLocationSummary.getLocations());
                                    List<Location> locations = new ArrayList<>();
                                    locations.addAll(serverLocationSummary.getLocations());
                                    android.location.Location loc = ((MainActivity) getActivity()).getLastKnownLocation();
                                    Log.d("WeatherLocation", "onNext");
                                    addCurrentLocation(loc, locations);
                                    setAdapter(locations);
                                } else {
                                    Toast.makeText(BaseApplication.getInstance(), R.string.failed_to_load_locations, Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
        );
    }



    private void addCurrentLocation(android.location.Location loc, List<Location> locations) {

        Log.d("WeatherLocation", "notnull");
        Location currentLoc = new Location();
        currentLoc.setCurrent();
        if (loc != null) {
            currentLoc.setLatitude(loc.getLatitude());
            currentLoc.setLongitude(loc.getLongitude());
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        locations.add(0, currentLoc);

    }

    private void setAdapter(List<Location> locations) {
        locations.add(null);
        if (recyclerAdapter != null){

            recyclerAdapter.setLocations(locations);
            recyclerAdapter.notifyDataSetChanged();
        }else{
            recyclerAdapter = new LocationRecyclerAdapter(locations, getActivity());
            recyclerView.setAdapter(recyclerAdapter);
        }
        progressBar.setVisibility(View.GONE);
        ((MainActivity)getActivity()).kickOffWatch();
    }

    /**
     * Resets the current local data and calls {@link #fetchData(String)}
     */
    public void refreshLocations() {
        BaseApplication.getInstance().setSavedLocations(null);
        fetchData(preferenceHelper.getPushToken());
    }
}
