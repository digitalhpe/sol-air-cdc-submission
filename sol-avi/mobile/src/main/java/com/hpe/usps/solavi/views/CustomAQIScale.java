package com.hpe.usps.solavi.views;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.MainActivity;
import com.hpe.usps.solavi.helpers.ResourceHelper;

/**
 * Custom scale layout containing six custom cells of {@link com.hpe.usps.solavi.views.CustomScaleLayout}
 * Created by chishobr on 12/14/2015.
 */
public class CustomAQIScale extends LinearLayout {

    private SelectedCell selectedCell;
    private LinearLayout scale;
    private MainActivity context;
    private int initialLeftMargin = -1;

    public CustomAQIScale(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = (MainActivity) context;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.view_custom_aqi_scale, this, true);
        scale = (LinearLayout)v.findViewById(R.id.aqi_gauge);
        selectedCell = (SelectedCell)v.findViewById(R.id.selected_cell_uv);

    }

    public void loadingAQIScale(){
        scale.setVisibility(View.INVISIBLE);
    }

    /**
     * Select the appropriate {@link com.hpe.usps.solavi.views.CustomScaleLayout} based on the AQ index value
     */
    public void selectAqiScale(int scaleWidth, int airQualityIndex) {
        scale.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams layoutParams;
        layoutParams = (RelativeLayout.LayoutParams) selectedCell.getLayoutParams();
        float density = BaseApplication.getInstance().getDensity();
        if (density >0){
            layoutParams.width = (int)(scaleWidth+33*density+0.5f);
        }else{
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager w = context.getWindowManager();
            w.getDefaultDisplay().getMetrics(metrics);
            density = metrics.density;
            BaseApplication.getInstance().setDensity(density);
        }
        layoutParams.width = (int)(scaleWidth+33*density+0.5f);

        selectedCell.setColorAndValue(airQualityIndex, true);

        int pixelsOver = ResourceHelper.getAQIPosition(airQualityIndex)*(scaleWidth);

        slideIt(pixelsOver);

    }

    public int getScaleWidth(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
        int scaleWidth = preferences.getInt(BaseApplication.AQI_WIDTH, 0);

        if (scale.getWidth() != 0){
            scaleWidth = scale.getWidth()/6;
            if (scaleWidth!=0){
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt(BaseApplication.AQI_WIDTH, scaleWidth);
                editor.commit();
            }
        }
        return scaleWidth;
    }

    public void slideIt(final int pixelsOver){

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) selectedCell.getLayoutParams();
        if (initialLeftMargin == -1){
            initialLeftMargin = params.leftMargin;
        }
        params.leftMargin = initialLeftMargin + pixelsOver;
        selectedCell.setLayoutParams(params);
        selectedCell.setVisibility(View.VISIBLE);
    }
}
