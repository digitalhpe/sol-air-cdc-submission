package com.hpe.usps.solavi.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.MainActivity;
import com.hpe.usps.solavi.helpers.ResourceHelper;

/**
 * Created by chishobr
 */
public class ArcedEPAGauge extends View {

    private static final float UV_CELL_COUNT = 11.0f;
    private static final float UV_CELL_ARC = 270.0f / UV_CELL_COUNT;
    private static final float AQI_CELL_COUNT = 6.0f;
    private static final float AQI_CELL_ARC = 270.0f / AQI_CELL_COUNT;
    private static final int DOTS_COUNT = 180;
    private static final float SHADOW_WIDTH = 2f;
    private static final double OZONE_DIVISOR = 7.2;
    private static final double PM10_DIVISOR = 4.5;
    private static final double PM25_DIVISOR = 3.27272727273;
    private static final double UVI_DIVISOR = 2.57142857143;
    private static final double OUTER_RIM_DIVISOR = 2.2;
    private static final double OUTER_DOTS_DIVISOR = 2.0;
    private static float FULL_CIRCLE = 360.0f;
    private static float ARC_START = 135f;
    private static float RIGHT_ANGLE = 90f;
    
    private String label = "";
    private int maxRadius;
    int radius, stroke;
    String ringType = "";
    Context context;
    private int epaValue = -1;
    RectF rectNormal, rectInner, rectOuter;
    Paint innerPaint = new Paint();
    Paint accentPaint = new Paint();
    Paint rimPaint = new Paint();
    Paint separatorPaint = new Paint();
    Paint textPaint = new Paint();
    Paint labelPaint = new Paint();
    private boolean previousWasSelected = false, firstWasSelected = false;
    private float startEdge = -1f, endEdge = -1f;
    private float rotation = 0f;


    public ArcedEPAGauge(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomArcScale);
        ringType = a.getString(R.styleable.CustomArcScale_ringType);
        label = a.getString(R.styleable.CustomArcScale_label);
        this.context = context;
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager w = ((MainActivity) context).getWindowManager();
        w.getDefaultDisplay().getMetrics(metrics);

        maxRadius = Math.min(metrics.widthPixels, metrics.heightPixels);
        stroke = (int) (maxRadius / 13.5);
        a.recycle();
        // smooths
        innerPaint.setAntiAlias(true);
        innerPaint.setStyle(Paint.Style.STROKE);
        innerPaint.setStrokeWidth(stroke);
        // smooths
        accentPaint.setAntiAlias(true);
        accentPaint.setStyle(Paint.Style.STROKE);
        accentPaint.setColor(context.getResources().getColor(R.color.transparent_white));

        // smooths
        rimPaint.setAntiAlias(true);
        rimPaint.setColor(context.getResources().getColor(R.color.hpe_slate));
        rimPaint.setStyle(Paint.Style.STROKE);
        rimPaint.setStrokeWidth(1);

        // smooths
        separatorPaint.setAntiAlias(true);
        separatorPaint.setColor(context.getResources().getColor(R.color.hpe_slate));
        separatorPaint.setStyle(Paint.Style.STROKE);
        separatorPaint.setStrokeWidth(stroke);

        // smooths
        textPaint.setAntiAlias(true);
        textPaint.setColor(context.getResources().getColor(R.color.epa_blue));
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(stroke / 2);

        // labels
        labelPaint.setAntiAlias(true);
        labelPaint.setStyle(Paint.Style.STROKE);
        labelPaint.setColor(context.getResources().getColor(R.color.white));
        labelPaint.setStrokeWidth(stroke);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        canvas.drawColor(Color.TRANSPARENT);

        if ("ozone".equals(ringType)) {
            drawGauge(canvas, AQI_CELL_ARC, AQI_CELL_COUNT);
        } else if ("pm10".equals(ringType)) {
            drawGauge(canvas, AQI_CELL_ARC, AQI_CELL_COUNT);
        } else if ("pm25".equals(ringType)) {
            drawGauge(canvas, AQI_CELL_ARC, AQI_CELL_COUNT);
        } else if ("uv".equals(ringType)) {
            drawGauge(canvas, UV_CELL_ARC, UV_CELL_COUNT);
        } else if ("outer_rim".equals(ringType)) {
            canvas.drawArc(rectNormal, 0f, FULL_CIRCLE, false, accentPaint);
        } else if ("dots".equals(ringType)) {
            for (int i = 0; i < DOTS_COUNT; i++) {
                canvas.drawArc(rectNormal, ARC_START + (2 * (float) i), 1, false, accentPaint);
            }
        }
    }

    private void drawGauge(Canvas canvas, float cellArc, float cellCount) {

        int placement = 0;
        if (Float.compare(cellCount, UV_CELL_COUNT) == 0) {
            placement = epaValue - 1;
        } else {
            placement = ResourceHelper.getAqiPlacement(epaValue);
        }

        for (int i = 0; i < cellCount; i++) {
            if (i == placement) {
                innerPaint.setShader(null);
                if (Float.compare(cellCount, UV_CELL_COUNT)==0) {
                    innerPaint.setColor(context.getResources().getColor(ResourceHelper.getUVColorIdByIndex(i + 1)));
                } else {
                    innerPaint.setColor(context.getResources().getColor(ResourceHelper.getAqiColorIdByPosition(i)));
                }
                Path mArc = new Path();
                mArc.addArc(rectNormal, ARC_START + (cellArc * (float) i), cellArc);
                innerPaint.setStrokeWidth(stroke + 4);
                startEdge = ARC_START + (cellArc * (float) i) - SHADOW_WIDTH;
                endEdge = ARC_START + (cellArc * (float) i) - SHADOW_WIDTH + cellArc + 4;
                canvas.drawArc(rectNormal, ARC_START + (cellArc * (float) i) - SHADOW_WIDTH, cellArc + 4, false, innerPaint);

                if ((i > 5 && Float.compare(cellCount, UV_CELL_COUNT)==0) || (i > 1 && Float.compare(cellCount, AQI_CELL_COUNT)==0)) {
                    textPaint.setColor(context.getResources().getColor(R.color.white));
                } else {
                    textPaint.setColor(context.getResources().getColor(R.color.black));
                }
                mArc.reset();
                mArc.addArc(new RectF(rectNormal.left-10, rectNormal.top-10, rectNormal.right+10, rectNormal.bottom+10), ARC_START + (cellArc * (float) i), cellArc);
                canvas.drawTextOnPath(Integer.toString(epaValue), mArc, 0, 20, textPaint);
                previousWasSelected = true;
                if (i == 0) {
                    firstWasSelected = true;
                }
            } else {
                canvas.save();
                if (Float.compare(cellCount, UV_CELL_COUNT)==0) {
                    setUvGradient(placement < 0? -2: i, ARC_START + cellArc * (float) i, cellArc, canvas);
                } else {
                    setAqiGradient(placement < 0? -2: i, ARC_START + cellArc * (float) i, cellArc, canvas);
                }
                innerPaint.setStrokeWidth(stroke);
                float offset = 0f;
                if (previousWasSelected) {
                    offset = SHADOW_WIDTH;
                }
                canvas.drawArc(rectNormal, ARC_START + (cellArc * (float) i) - rotation + offset, cellArc - offset, false, innerPaint);
                if (!previousWasSelected) {
                    float start = 0f;
                    start = ARC_START + (cellArc * (float) i) - 1;
                    canvas.drawArc(rectNormal, start - rotation, 2, false, separatorPaint);
                }
                previousWasSelected = false;
                canvas.restore();
                rotation = 0;
            }
        }

        float leftOffset = 0f, rightOffset = 0f;
        if (firstWasSelected) {
            leftOffset = SHADOW_WIDTH;
        }
        if (previousWasSelected) {
            rightOffset = SHADOW_WIDTH;
        }
        String missingText = "";
        if (placement < 0){
            labelPaint.setColor(getResources().getColor(R.color.light_gray));
            missingText="*";
        }else{
            labelPaint.setColor(getResources().getColor(R.color.white));
        }
        canvas.drawArc(rectNormal, 45 + rightOffset, RIGHT_ANGLE - leftOffset - rightOffset, false, labelPaint);
        Path mArc = new Path();
        mArc.addArc(rectNormal, ARC_START, -RIGHT_ANGLE);
        textPaint.setColor(context.getResources().getColor(R.color.epa_blue));
        canvas.drawTextOnPath(label+missingText, mArc, 0, 20, textPaint);
        if (startEdge > -1f) {
            separatorPaint.setColor(getResources().getColor(R.color.trans_black));
            canvas.drawArc(rectNormal, startEdge - SHADOW_WIDTH, SHADOW_WIDTH, false, separatorPaint);
            canvas.drawArc(rectNormal, endEdge, SHADOW_WIDTH, false, separatorPaint);
        }
    }


    
    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        if ("ozone".equals(ringType)) {
            radius = (int) (maxRadius / OZONE_DIVISOR);
        } else if ("pm10".equals(ringType)) {
            radius = (int) (maxRadius / PM10_DIVISOR);
        } else if ("pm25".equals(ringType)) {
            radius = (int) (maxRadius / PM25_DIVISOR);
        } else if ("uv".equals(ringType)) {
            radius = (int) (maxRadius / UVI_DIVISOR);
        } else if ("outer_rim".equals(ringType)) {
            accentPaint.setStrokeWidth(stroke / 2);
            radius = (int) (maxRadius / OUTER_RIM_DIVISOR);
        } else if ("dots".equals(ringType)) {
            accentPaint.setStrokeWidth(stroke / 16);
            radius = (int) (maxRadius / OUTER_DOTS_DIVISOR);
        }
        rectNormal = new RectF(maxRadius / 2 - radius, maxRadius / 2 - radius, maxRadius / 2 + radius, maxRadius / 2 + radius);
        rectInner = new RectF(maxRadius / 2 - radius + stroke / 2, maxRadius / 2 - radius + stroke / 2, maxRadius / 2 + radius - stroke / 2, maxRadius / 2 + radius - stroke / 2);
        rectOuter = new RectF(maxRadius / 2 - radius - stroke / 2, maxRadius / 2 - radius - stroke / 2, maxRadius / 2 + radius + stroke / 2, maxRadius / 2 + radius + stroke / 2);

        setMeasuredDimension(maxRadius, maxRadius);
    }

    public void setUvGradient(int i, float startAngle, float cellArc, Canvas canvas) {

        int start = context.getResources().getColor(ResourceHelper.getUVPastelColorIdByIndex(i + 1));
        separatorPaint.setColor(start);
        int end = context.getResources().getColor(ResourceHelper.getUVDarkPastelColorIdByIndex(i + 1));
        int[] colors = {start, end, end};
        float from = startAngle / FULL_CIRCLE;
        float to = (startAngle + cellArc) / FULL_CIRCLE;
        if (from > 1f) {
            from -= 1f;
            to -= 1f;
        }
        float localRot = 0f;
        if (to > 1f) {
            to -= 1f;
            localRot = 1 - from;
            to += localRot;
            from = 0f;
        }

        float[] positions = {from, to, to};
        Shader gradient = new SweepGradient(maxRadius / 2, maxRadius / 2, colors, positions);
        innerPaint.setShader(gradient);
        separatorPaint.setColor(start);
        rotation = -FULL_CIRCLE * localRot;
        canvas.rotate(rotation, maxRadius / 2, maxRadius / 2);
    }

    public void setAqiGradient(int i, float startAngle, float cellArc, Canvas canvas) {

        int start = context.getResources().getColor(ResourceHelper.getAqiPastelColorIdByPosition(i));
        separatorPaint.setColor(start);
        int end = context.getResources().getColor(ResourceHelper.getAqiPastelDarkColorIdByPosition(i));
        int[] colors = {start, end, end};
        float from = startAngle / FULL_CIRCLE;
        float to = (startAngle + cellArc) / FULL_CIRCLE;
        if (from > 1f) {
            from -= 1f;
            to -= 1f;
        }
        float localRot = 0f;
        if (to > 1f) {
            to -= 1f;
            localRot = 1 - from;
            to += localRot;
            from = 0f;
        }
        float[] positions = {from, to, to};
        Shader gradient = new SweepGradient(maxRadius / 2, maxRadius / 2, colors, positions);
        innerPaint.setShader(gradient);
        separatorPaint.setColor(start);
        rotation = -FULL_CIRCLE * localRot;
        canvas.rotate(rotation, maxRadius / 2, maxRadius / 2);
    }


    public void setValue(int epaValue) {
        this.epaValue = epaValue;
        invalidate();
    }
}
