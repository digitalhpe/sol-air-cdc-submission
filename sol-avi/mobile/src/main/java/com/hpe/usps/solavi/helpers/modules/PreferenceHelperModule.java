package com.hpe.usps.solavi.helpers.modules;

import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.activities.MainActivity;
import com.hpe.usps.solavi.fragments.LocationInfoFragment;
import com.hpe.usps.solavi.fragments.LocationMapFragment;
import com.hpe.usps.solavi.fragments.MainActivityFragment;
import com.hpe.usps.solavi.helpers.AndroidPreferenceHelper;
import com.hpe.usps.solavi.services.WeatherService;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andy Jones on 12/10/2015.
 */
@Module(
        injects = {
                MainActivity.class,
                MainActivityFragment.class,
                LocationMapFragment.class,
                LocationInfoFragment.class,
                WeatherService.class
        },
        library = true,
        complete = false
)
public class PreferenceHelperModule {

    private BaseApplication mApp;

    public PreferenceHelperModule(BaseApplication app) {
        mApp = app;
    }

    @Provides
    @Named("preferenceHelper")
    PreferenceHelper providesPreferenceHelper() {
        return new AndroidPreferenceHelper(mApp);
    }
}
