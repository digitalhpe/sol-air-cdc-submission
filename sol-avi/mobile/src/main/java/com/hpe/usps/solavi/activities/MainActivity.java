package com.hpe.usps.solavi.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.ForecastConfiguration;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.Constants;
import com.hpe.usps.solavi.Injector;
import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.events.DeleteLocationEvent;
import com.hpe.usps.solavi.events.TokenEvent;
import com.hpe.usps.solavi.fragments.LocationInfoFragment;
import com.hpe.usps.solavi.fragments.LocationMapFragment;
import com.hpe.usps.solavi.fragments.MainActivityFragment;
import com.hpe.usps.solavi.views.AqiInfoAlertDialog;
import com.hpe.usps.solavi.views.ParkBenefitDialog;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;
import com.hpe.usps.solavilibrary.helpers.LocationHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;
import com.hpe.usps.solavilibrary.models.DatePickerEvent;
import com.hpe.usps.solavilibrary.models.DeleteLocationRequest;
import com.hpe.usps.solavilibrary.models.GenericResponse;
import com.hpe.usps.solavilibrary.models.RefreshLocationEvent;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.GoogleLocationService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;


/**
 * @author chishobr
 */
public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        com.google.android.gms.location.LocationListener,
        DataApi.DataListener,
        MessageApi.MessageListener,
        NodeApi.NodeListener {//

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public final static long ONE_WEEK_IN_MILLIS = 1000l * 60 * 60 * 24 * 7;
    private static final long LOCATION_INTERVAL = 30l * 1000;
    private String shareText = "";

    @Inject
    @Named("solaviService")
    SolaviService solaviService;

    @Inject
    @Named("placesService")
    PlacesService placesService;

    @Inject
    @Named("googleLocationService")
    GoogleLocationService googleLocationService;

    @Inject
    @Named("airNowService")
    AirNowService airNowService;

    @Inject
    @Named("epaService")
    EPAService epaService;

    @Inject
    @Named("geoHelper")
    GeoHelper geoHelper;

    @Inject
    @Named("preferenceHelper")
    PreferenceHelper preferenceHelper;

    private CompositeSubscription subscriptions = new CompositeSubscription();
    public static final String TAG = MainActivity.class.getSimpleName();
    private GoogleApiClient googleApiClient;
    private GoogleCloudMessaging gcm;
    private Location lastLocation;
    private LocationHelper locationHelper;
    private ProgressBar progressSpinner;
    private boolean canUseWearable = true;
    private static final String START_ACTIVITY_PATH = "/start-activity";
    private static final String MOBILE_ACTIVITY_PATH = "/mobile";
    private String path = START_ACTIVITY_PATH;
    private LocationRequest locationRequest;
    private ShareActionProvider shareActionProvider;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2;

    /**
     * Returns the last known location of the user's device.
     *
     * @return lastLocation
     */
    public Location getLastKnownLocation() {

        return lastLocation;
    }


    LocationManager mLocationManager;

    @Override
    protected void onPause() {
        super.onPause();
        if (canUseWearable) {
            Wearable.DataApi.removeListener(googleApiClient, this);
            Wearable.MessageApi.removeListener(googleApiClient, this);
            Wearable.NodeApi.removeListener(googleApiClient, this);
        }
        googleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d("api", "connected");
        if (canUseWearable) {
            Wearable.DataApi.addListener(googleApiClient, this);
            Wearable.MessageApi.addListener(googleApiClient, this);
            Wearable.NodeApi.addListener(googleApiClient, this);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            Log.d("WeatherLocation", "got-it");
        } else {

            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(LOCATION_INTERVAL);
            locationRequest.setFastestInterval(LOCATION_INTERVAL);
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            Log.d("WeatherLocation", "got-it");
            kickOffWatch();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                connectIfAllowed(grantResults);
                break;
            default:
                return;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void connectIfAllowed(int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                Log.d("WeatherLocation", "got-it");
                return;
            }
            googleApiClient.connect();

        } else {

            // permission denied, boo! Disable the
            // functionality that depends on this permission.
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //TODO: check if actually can't use wear...but most likely the reason
        canUseWearable = false;
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
        Log.d("API", "Failed" + connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        EventBus.getDefault().post(new RefreshLocationEvent());
        lastLocation = location;
        kickOffWatch();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //unused
    }

    @Override
    public void onProviderEnabled(String provider) {
        //unused
    }

    @Override
    public void onProviderDisabled(String provider) {
        //unused
    }

    /**
     * Show the Air Quality Index key dialog over the current view
     */
    public void showAqiInfo() {

        FragmentManager manager = getSupportFragmentManager();
        AqiInfoAlertDialog dialogFragment = new AqiInfoAlertDialog();
        dialogFragment.show(manager, "Dialog Fragment");


    }

    /**
     * Show the {@link com.hpe.usps.solavi.views.AqiInfoAlertDialog} over the current view
     */
    public void showDatePicker() {

        final Calendar now = Calendar.getInstance();
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);
        int year = now.get(Calendar.YEAR);
        DatePickerDialog mDatePicker = new DatePickerDialog(this, new mDateSetListener(), year - 1, month, day);
        mDatePicker.getDatePicker().setMaxDate(now.getTimeInMillis() - (ONE_WEEK_IN_MILLIS));
        mDatePicker.show();

    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        //unused
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        path = MOBILE_ACTIVITY_PATH;
        kickOffWatch();
    }

    @Override
    public void onPeerConnected(Node node) {
        //unused
    }

    @Override
    public void onPeerDisconnected(Node node) {
        //unused
    }

    public void showParkBenefitsDialog() {
        FragmentManager manager = getSupportFragmentManager();
        ParkBenefitDialog dialogFragment = new ParkBenefitDialog();
        dialogFragment.show(manager, "Dialog Fragment");
    }

    public void showParkBenefitsPDF() {
        PDFView pdfView = (PDFView) findViewById(R.id.pdfView);
        pdfView.fromAsset("park_benefit.pdf").load();
        pdfView.setVisibility(View.VISIBLE);
        AppBarLayout toolbar = (AppBarLayout)findViewById(R.id.toolbar_layout);
        toolbar.setVisibility(View.INVISIBLE);
    }

    /**
     * Listener for DatePickerDialog object that triggers onDateSet when the user saves a new date from the DatePicker
     */
    class mDateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            Calendar selection = Calendar.getInstance();
            selection.set(Calendar.MONTH, monthOfYear);
            selection.set(Calendar.YEAR, year);
            selection.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            EventBus.getDefault().post(new DatePickerEvent(selection));

        }
    }

    /**
     * Mode of the MainActivity.  Which fragment it is showing.
     */
    private enum eViewMode {
        MAIN,
        MAP,
        INFO
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.inject(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.app_logo);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setTitleTextAppearance(this,R.style.toolbarTitleAppearance);
        toolbar.setNavigationIcon(null);
        setSupportActionBar(toolbar);
        viewLocations();
        Window window = this.getWindow();

        locationHelper = new LocationHelper(preferenceHelper, geoHelper, epaService, airNowService, solaviService, placesService);

        setupGoogleCloudMessaging();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        //canUseWearable = false;

        progressSpinner = (ProgressBar) findViewById(R.id.progressSpinner);

        ForecastConfiguration configuration =
                new ForecastConfiguration.Builder(Constants.DARK_SKY_API_KEY)
                        .setCacheDirectory(getCacheDir())
                        .build();
        ForecastClient.create(configuration);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color

            window.setStatusBarColor(ContextCompat.getColor(this, R.color.black));
        }
    }

    /**
     * Registers device for Google Cloud Messaging.  If device has already been registered, then the
     * push token is stored in shared prefs and retrieved from there.
     */
    private void setupGoogleCloudMessaging() {
        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            String puskToken = preferenceHelper.getPushToken();

            if (puskToken.isEmpty()) {
                Log.d(TAG, "No GCM registration ID found.  Making request.");
                registerInBackground();
            } else {
                Log.d(TAG, "Already have a GCM registration ID of: " + puskToken);
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
            Toast.makeText(BaseApplication.getInstance(), R.string.google_play_services_not_found, Toast.LENGTH_LONG).show();
        }
    }



    public void setShareText(String shareText){
        this.shareText = shareText;
    }

    // Call to update the share intent
    public void setShareIntent() {
        if (shareText.isEmpty()){
            return;
        }
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.select_app_to_share_to)));
        if (shareActionProvider != null) {
            shareActionProvider.setShareIntent(sendIntent);
        }
    }

    public void deleteLocation(final String locationId, String name) {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.delete)+" "+name+"?")
                .setMessage(getString(R.string.are_you_sure))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DeleteLocationRequest deleteLocationRequest = new DeleteLocationRequest(locationId);
                        showProgressSpinner();
                        subscriptions.add(
                                solaviService.deleteLocation(deleteLocationRequest)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Subscriber<GenericResponse>() {
                                            @Override
                                            public final void onCompleted() {
                                                //unused
                                            }

                                            @Override
                                            public final void onError(Throwable e) {
                                                Toast.makeText(BaseApplication.getInstance(), R.string.failed_to_delete_location, Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onNext(GenericResponse genericResponse) {
                                                com.hpe.usps.solavilibrary.models.Location target = null;
                                                for (com.hpe.usps.solavilibrary.models.Location loc : BaseApplication.getInstance().getSavedLocations()) {
                                                    if (loc.getLocationid().equals(locationId)) {
                                                        target = loc;
                                                    }
                                                }
                                                BaseApplication.getInstance().getSavedLocations().remove(target);
                                                hideProgressSpinner();
                                                EventBus.getDefault().post(new DeleteLocationEvent());
                                            }
                                        })
                        );
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(0)
                .show();

    }

    /**
     * Show and animate to the {@link com.hpe.usps.solavi.fragments.LocationMapFragment}
     */
    public void viewMap() {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        LocationMapFragment fragment = new LocationMapFragment();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * Transition and animate to the {@link com.hpe.usps.solavi.fragments.MainActivityFragment} page
     */
    public void viewLocations() {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        MainActivityFragment fragment = new MainActivityFragment();
        transaction.setCustomAnimations(0, 0, R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.replace(R.id.container, fragment, MainActivityFragment.class.getName());
        transaction.commit();
    }

    /**
     * Transition and animate to the {@link com.hpe.usps.solavi.fragments.LocationInfoFragment} page
     */
    public void callInfoPage(String locationId, double latitude, double longitude, String geocodeString) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        LocationInfoFragment fragment = new LocationInfoFragment();
        List<com.hpe.usps.solavilibrary.models.Location> locationList = BaseApplication.getInstance().getSavedLocations();
        com.hpe.usps.solavilibrary.models.Location location = locationHelper.createLocation(locationId, latitude, longitude, locationList);
        fragment.setGeoCodeName(geocodeString);
        fragment.setLocation(location);
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.addToBackStack(null);
        transaction.replace(R.id.container, fragment, LocationInfoFragment.class.getName());
        transaction.commit();
    }

    /**
     * Show the {@link com.hpe.usps.solavi.fragments.MainActivityFragment} page
     */
    public void showLocationList() {
        FragmentManager manager = getSupportFragmentManager();
        MainActivityFragment mainActivityFragment = (MainActivityFragment) manager.findFragmentByTag(MainActivityFragment.class.getName());
        mainActivityFragment.refreshLocations();

        if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
            manager.popBackStack();
        } else if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        googleApiClient.connect();

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        if (findViewById(R.id.pdfView).getVisibility()==View.VISIBLE){
            PDFView pdfView = (PDFView) findViewById(R.id.pdfView);
            pdfView.setVisibility(View.GONE);
            AppBarLayout toolbar = (AppBarLayout)findViewById(R.id.toolbar_layout);
            toolbar.setVisibility(View.VISIBLE);
        }else if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
        }else if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
        }else{
            super.onBackPressed();
        }
    }

    public void kickOffWatch(){
        if (!canUseWearable) {
            return;
        }

        if (lastLocation ==null){
            new StartWearableActivityTask().execute("");
        }else{
            subscriptions.add(
                    locationHelper.getIndexesForLocation(lastLocation.getLatitude(), lastLocation.getLongitude())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<com.hpe.usps.solavilibrary.models.Location>() {
                                @Override
                                public final void onCompleted() {

                                }

                                @Override
                                public final void onError(Throwable e) {
                                    new StartWearableActivityTask().execute("");
                                }

                                @Override
                                public final void onNext(com.hpe.usps.solavilibrary.models.Location response) {
                                    Log.d(MainActivity.class.getName(), "kickoff task");
                                    String locationInfo = response.getLatitude() + "," + response.getLongitude()+"," + response.getOzone()+"," + response.getUv();
                                    new StartWearableActivityTask().execute(locationInfo);
                                }
                            })
            );

        }


    }

    private Collection<String> getNodes() {
        HashSet<String> results = new HashSet<>();
        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(googleApiClient).await();

        for (Node node : nodes.getNodes()) {
            results.add(node.getId());
        }

        return results;
    }
    private class StartWearableActivityTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... args) {
            String locationInfo = args[0];
            Collection<String> nodes = getNodes();
            for (String node : nodes) {
                sendStartActivityMessage(node, locationInfo );
            }
            return null;
        }
    }


    private void sendStartActivityMessage(String node, String data) {
        Log.d(TAG, data + "node: " + node);
        Wearable.MessageApi.sendMessage(
                googleApiClient, node, path, data.getBytes())
                .setResultCallback(
                        new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                if (!sendMessageResult.getStatus().isSuccess()) {
                                    Log.e(TAG, "Failed to send message with status code: "
                                            + sendMessageResult.getStatus().getStatusCode());
                                }
                            }
                        }
                );

    }

    /**
     * Calls {@link LocationInfoFragment#saveLocation()}
     */
    private void saveLocation() {
        FragmentManager manager = getSupportFragmentManager();
        LocationInfoFragment locationInfoFragment = (LocationInfoFragment) manager.findFragmentByTag(LocationInfoFragment.class.getName());
        locationInfoFragment.saveLocation();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
                Toast.makeText(BaseApplication.getInstance(), R.string.update_google_play_services, Toast.LENGTH_LONG).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                Toast.makeText(BaseApplication.getInstance(), R.string.device_not_supported, Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Background task for registering device with GCM / retrieving GCM push token.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
                    }
                    String pushToken = gcm.register(Constants.GCM_SENDER_ID);
                    msg = "Device registered, registration ID=" + pushToken;
                    preferenceHelper.setPushToken(pushToken);

                } catch (IOException ex) {
                    Log.e(TAG, ex.getMessage(), ex);
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (msg.contains("Error")) {
                    Toast.makeText(BaseApplication.getInstance(), R.string.push_notification_registration_error, Toast.LENGTH_LONG).show();
                } else {
                    Log.d(TAG, msg);
                }
                EventBus.getDefault().post(new TokenEvent(preferenceHelper.getPushToken()));
            }
        }.execute(null, null, null);
    }

    /**
     * Shows Progress Spinner
     */
    public void showProgressSpinner() {
        progressSpinner.setVisibility(View.VISIBLE);
    }

    /**
     * Hides Progress Spinner
     */
    public void hideProgressSpinner() {
        progressSpinner.setVisibility(View.GONE);
    }
}
