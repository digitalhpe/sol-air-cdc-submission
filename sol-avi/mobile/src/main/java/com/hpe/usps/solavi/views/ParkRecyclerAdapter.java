package com.hpe.usps.solavi.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hpe.usps.solavi.R;
import com.hpe.usps.solavilibrary.models.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chishobr on 8/9/2017.
 */

public class ParkRecyclerAdapter extends RecyclerView.Adapter<ParkRecyclerAdapter.ComparisonViewHolder> {

    private List<Location> comparisonList;
    private Context context;

    public class ComparisonViewHolder extends RecyclerView.ViewHolder {
        public TextView name, percentage;

        public ComparisonViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.location_name_comparison);
            percentage = (TextView) view.findViewById(R.id.location_park_percentage_comparison);
        }
    }

    public ParkRecyclerAdapter(ArrayList<Location> comparisonLocations, Context activity) {
        this.context = activity;
        this.comparisonList = comparisonLocations;

    }

    @Override
    public ComparisonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View comparisonView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.comparison_layout, parent, false);
        return new ComparisonViewHolder(comparisonView);
    }

    @Override
    public void onBindViewHolder(ComparisonViewHolder holder, int position) {
        Location location = comparisonList.get(position);
        holder.name.setText(location.getName());
        holder.percentage.setText(location.getPark()+"%");
    }

    @Override
    public int getItemCount() {
        return comparisonList.size();
    }
}
