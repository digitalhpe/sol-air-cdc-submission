package com.hpe.usps.solavi.fragments;


import android.Manifest;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.greysonparrelli.permiso.Permiso;
import com.hpe.usps.solavi.BaseApplication;
import com.hpe.usps.solavi.Constants;
import com.hpe.usps.solavi.Injector;
import com.hpe.usps.solavi.R;
import com.hpe.usps.solavi.activities.MainActivity;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;
import com.hpe.usps.solavilibrary.helpers.LocationHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;
import com.hpe.usps.solavi.helpers.ResourceHelper;
import com.hpe.usps.solavilibrary.models.Location;
import com.hpe.usps.solavilibrary.models.LocationSummary;
import com.hpe.usps.solavilibrary.models.Parks.ParkLatLng;
import com.hpe.usps.solavilibrary.models.Parks.ParkLocation;
import com.hpe.usps.solavilibrary.models.Parks.ParkResult;
import com.hpe.usps.solavilibrary.models.googlelocation.Result;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by chishobr on 12/9/2015.
 */
public class LocationMapFragment extends Fragment implements OnMapReadyCallback {

    @Inject
    @Named("airNowService")
    AirNowService airNowService;

    @Inject
    @Named("epaService")
    EPAService epaService;

    @Inject
    @Named("solaviService")
    SolaviService solaviService;

    @Inject
    @Named("placesService")
    PlacesService placesService;

    @Inject
    @Named("geoHelper")
    GeoHelper geoHelper;

    @Inject
    @Named("preferenceHelper")
    PreferenceHelper preferenceHelper;

    private MapView mapView;
    private GoogleMap googleMap;
    private MainActivity context;
    private LocationHelper locationHelper;
    private EditText zipCodeEditText;
    private String geoCodeResultName = "";

    private CompositeSubscription subscriptions = new CompositeSubscription();
    private ArrayList<Marker> tempMarkerList;
    private ArrayList<Marker> parkMarkers;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.location_map_fragment, container,
                false);
        setViews(v);
        mapView.onCreate(savedInstanceState);
        tempMarkerList = new ArrayList<>();
        parkMarkers = new ArrayList<>();
        context = (MainActivity) getActivity();

        Injector.inject(this);

        locationHelper = new LocationHelper(preferenceHelper, geoHelper, epaService, airNowService, solaviService, placesService);

        try {
            MapsInitializer.initialize(context.getApplicationContext());
        } catch (Exception e) {
            Log.e(this.getClass().getName(), e.getMessage(), e);
            Toast.makeText(getActivity(),R.string.error_initializing_map, Toast.LENGTH_LONG).show();
        }

        mapView.getMapAsync(this);

        // Perform any camera updates here
        setHasOptionsMenu(true);
        return v;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_map, menu);
        menu.findItem(R.id.home_item).getIcon().setColorFilter(ContextCompat.getColor(getActivity(), R.color.icon_gray), PorterDuff.Mode.SRC_ATOP);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home_item) {
            ((MainActivity)getActivity()).viewLocations();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setInfoWindowClickAdapter() {
        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                String locationId = null;
                if (marker.getSnippet() != null) {
                    locationId = marker.getSnippet();
                }
                context.callInfoPage(locationId, marker.getPosition().latitude, marker.getPosition().longitude, marker.getTitle()==null||marker.getTitle().isEmpty()?geoCodeResultName:marker.getTitle());
            }
        });
    }

    private void setInfoWindowAdapter(){
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            private Marker currentMarker;
            private boolean indexesLoaded;
            private String aqiIndex;
            private String uvIndex;

            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                currentMarker = marker;

                View infoLayout;

                infoLayout = getLayoutInflater(getArguments()).inflate(R.layout.map_info_window, null);

                TextView textViewLocationName = (TextView)infoLayout.findViewById(R.id.location_name);
                final TextView textViewOzoneTitle = (TextView)infoLayout.findViewById(R.id.ozone_title);
                textViewOzoneTitle.setText(Html.fromHtml("O<sub><small><small>3</small></small></sub>"));
                final TextView textViewAqi = (TextView)infoLayout.findViewById(R.id.location_aqi_text);
                final TextView textViewUvi = (TextView)infoLayout.findViewById(R.id.location_uv_text);

                textViewLocationName.setText(marker.getTitle());

                double lat = marker.getPosition().latitude;
                double lng = marker.getPosition().longitude;

                if (!indexesLoaded) {
                    subscriptions.add(
                            locationHelper.getIndexesForLocation(lat, lng)
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Subscriber<Location>() {
                                        @Override
                                        public final void onCompleted() {
                                            //unused
                                        }

                                        @Override
                                        public final void onError(Throwable e) {
                                            aqiIndex = getString(R.string.not_available);
                                            uvIndex = getString(R.string.not_available);
                                            indexesLoaded = true;
                                            if (currentMarker != null && currentMarker.isInfoWindowShown()) {
                                                currentMarker.showInfoWindow();
                                            }
                                        }

                                        @Override
                                        public final void onNext(Location response) {
                                            aqiIndex = String.valueOf(response.getOzone());
                                            uvIndex = String.valueOf(response.getUv());
                                            indexesLoaded = true;
                                            if (currentMarker != null && currentMarker.isInfoWindowShown()) {
                                                currentMarker.showInfoWindow();
                                            }
                                        }
                                    })
                    );
                } else {
                    textViewAqi.setText(aqiIndex);
                    if (!aqiIndex.equals(getString(R.string.not_available))) {
                        textViewAqi.setBackgroundColor(ContextCompat.getColor(getContext(), ResourceHelper.getAqiPastelColorIdByIndex(Integer.parseInt(aqiIndex))));
                    }
                    textViewUvi.setText(uvIndex);
                    if (!uvIndex.equals(getString(R.string.not_available))) {
                        textViewUvi.setBackgroundColor(ContextCompat.getColor(getContext(), ResourceHelper.getUVPastelColorIdByIndex(Integer.parseInt(uvIndex))));
                    }

                    indexesLoaded = false;
                }
                infoLayout.setPadding((int)getResources().getDimension(R.dimen.one_dp)*(-5), 0, (int)getResources().getDimension(R.dimen.one_dp)*(-5), 0);
                return infoLayout;

            }
        });
    }

    private void setClickHandler() {
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                final Geocoder geocoder = new Geocoder(getActivity());
                try {
                    List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addresses != null && !addresses.isEmpty()) {
                        Address address = addresses.get(0);
                        // Use the address as needed
                        MarkerOptions markerOptions = new MarkerOptions();

                        // Setting the position for the marker
                        markerOptions.position(latLng);


                        geoCodeResultName = address.getLocality()+", "+geoHelper.getStateAbbv(address.getAdminArea());
                        markerOptions.title(geoCodeResultName);
                        for (Marker tempMarker : tempMarkerList) {
                            tempMarker.remove();
                        }

                        markerOptions.icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

                        // Animating to the touched position
                        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                        // Placing a marker on the touched position
                        Marker marker = googleMap.addMarker(markerOptions);
                        tempMarkerList.add(marker);

                        marker.showInfoWindow();
                        loadParkPlaces(latLng.latitude, latLng.longitude);
                    }
                } catch (IOException e) {
                    Log.e(this.getClass().getName(), e.getMessage(), e);
                    Toast.makeText(getActivity(), R.string.error_geocode_zipcode, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void setViews(View v) {
        mapView = (MapView) v.findViewById(R.id.mapView);
        zipCodeEditText = (EditText) v.findViewById(R.id.zipEdit);


        zipCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //unused
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int zip = getValidZipCode(s);
                if (zip > 0) {
                    AddMarkerFromZip(zip);
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //unused
            }
        });

    }

    private int getValidZipCode(CharSequence s) {
        //TODO: make this better
        int zip = -1;
        if (s.length() == 5){
            zip = Integer.parseInt(s.toString());
        }
        return zip;
    }

    private void AddMarkerFromZip(int zipCode) {
        final Geocoder geocoder = new Geocoder(getActivity());
        try {
            List<Address> addresses = geocoder.getFromLocationName(String.valueOf(zipCode), 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);
                // Use the address as needed
                LatLng latLng = new LatLng(address.getLatitude(),address.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                String addressLine = address.getLocality() + ", " + geoHelper.getStateAbbv(address.getAdminArea());
//                if (addressLine != null && !addressLine.isEmpty() && addressLine.lastIndexOf(' ') >= 0){
//                    addressLine = addressLine.substring(0,addressLine.lastIndexOf(' '));
//                }
                markerOptions.title(addressLine);
                geoCodeResultName = addressLine;
                markerOptions.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

                // Animating to the touched position
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                // Placing a marker on the touched position
                Marker marker = googleMap.addMarker(markerOptions);
                marker.showInfoWindow();
                loadParkPlaces(latLng.latitude, latLng.longitude);
            }
        } catch (IOException e) {
            Log.e(this.getClass().getName(), e.getMessage(), e);
            Toast.makeText(getActivity(),R.string.error_geocode_zipcode, Toast.LENGTH_LONG).show();
        }

    }

    private String getLocationAddress(Double latitude, Double longitude) {
        final Geocoder geocoder = new Geocoder(context);
        String result = context.getResources().getString(R.string.you_are_here);
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                String addressLine = address.getAddressLine(Math.max(address.getMaxAddressLineIndex()-1,0));
                if (addressLine != null && !addressLine.isEmpty()){
                    int lastSpace = addressLine.lastIndexOf(' ');
                    addressLine = addressLine.substring(0,lastSpace>0?lastSpace:0);
                    return addressLine;
                }



            }
        } catch (IOException e) {
            Log.e(this.getClass().getName(), e.getMessage(), e);
        }
        return result;
    }

    //private void addCustomInfoLayout()

    private void setCurrentLocationMarker() {
        Permiso.getInstance().setActivity(getActivity());

        Permiso.getInstance().requestPermissions(new Permiso.IOnPermissionResult() {
            @Override
            public void onPermissionResult(Permiso.ResultSet resultSet) {
                if (resultSet.areAllPermissionsGranted()) {
                    android.location.Location lastKnownLocation = ((MainActivity)getActivity()).getLastKnownLocation();
                    double latitude, longitude;
                    int zoomLevel;
                    if (lastKnownLocation==null) {
                        return;
                    }else{
                        loadParkPlaces(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
                        latitude = lastKnownLocation.getLatitude();
                        longitude = lastKnownLocation.getLongitude();
                        LatLng position = new LatLng(latitude,longitude);
                        zoomLevel = 12;

                        MarkerOptions markerOptions = new MarkerOptions();

                        // Setting the position for the marker
                        markerOptions.position(position);
                        // Setting the title for the marker.
                        // This will be displayed on taping the marker
                        markerOptions.title(getLocationAddress(latitude, longitude));
                        markerOptions.icon(BitmapDescriptorFactory
                                .fromResource(android.R.drawable.ic_menu_mylocation));

                        // Placing a marker on the touched position
                        googleMap.addMarker(markerOptions);
                    }
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(latitude,longitude)).zoom(zoomLevel).build();
                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                }
            }

            @Override
            public void onRationaleRequested(Permiso.IOnRationaleProvided callback, String... permissions) {
                Permiso.getInstance().showRationaleInDialog(getString(R.string.location_permission), getString(R.string.location_permission_desc), null, callback);
            }
        }, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void loadParkPlaces(double latitude, double longitude) {
        removeOldParks();
        com.hpe.usps.solavilibrary.models.googlelocation.LatLng ltln = new com.hpe.usps.solavilibrary.models.googlelocation.LatLng(latitude, longitude);
        subscriptions.add(
                locationHelper.getParkLocations(ltln, 10000)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<ParkLocation>() {
                            @Override
                            public final void onCompleted() {
                                //unused
                            }

                            @Override
                            public final void onError(Throwable e) {

                            }

                            @Override
                            public final void onNext(ParkLocation response) {
                                placeParkMarkers(response);
                            }
                        })
        );

    }

    private void removeOldParks() {
        for (Marker oldParkMarker : parkMarkers) {
            oldParkMarker.remove();
        }
        parkMarkers.clear();
    }

    private void placeParkMarkers(ParkLocation response) {
        for(ParkResult park : response.getResults()){
            ParkLatLng parkLocation = park.getGeometry().getLocation();
            double latitude = parkLocation.getLat();
            double longitude = parkLocation.getLng();
            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude))
                    .title(park.getName());
            // Changing marker icon
            int height = 100;
            int width = 80;
            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.park_pin);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
            marker.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));

            // adding marker
            Marker parkMarker = googleMap.addMarker(marker);
            parkMarkers.add(parkMarker);
        }
    }



    private void populateMarkers(List<Location> locations) {

        for (Location loc : locations) {
            double latitude = loc.getLatitude();
            double longitude = loc.getLongitude();
            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude))
                    .title(loc.getName())
                    .snippet(loc.getLocationid());
            // Changing marker icon
            marker.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

            // adding marker
            googleMap.addMarker(marker);
        }
    }

    private void fetchData(String pushToken) {
        List<Location> savedLocations = BaseApplication.getInstance().getSavedLocations();

        if (savedLocations == null) {
            subscriptions.add(
                    solaviService.getLocationSummary(pushToken)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<LocationSummary>() {
                                @Override
                                public final void onCompleted() {
                                    //unused
                                }

                                @Override
                                public final void onError(Throwable e) {
                                    Toast.makeText(BaseApplication.getInstance(), R.string.failed_to_load_location_details, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public final void onNext(LocationSummary serverLocationSummary) {
                                    if (serverLocationSummary.getResult().equals(Constants.RESPONSE_OK)) {
                                        BaseApplication.getInstance().setSavedLocations(serverLocationSummary.getLocations());
                                        populateMarkers(serverLocationSummary.getLocations());
                                    } else {
                                        Toast.makeText(BaseApplication.getInstance(), R.string.failed_to_load_location_details, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
            );
        } else {
            populateMarkers(savedLocations);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        subscriptions.unsubscribe();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        String pushToken = preferenceHelper.getPushToken();

        if (!pushToken.isEmpty()) {
            fetchData(pushToken);
        }

        setClickHandler();
        setInfoWindowAdapter();
        setInfoWindowClickAdapter();
        setCurrentLocationMarker();
    }
}


