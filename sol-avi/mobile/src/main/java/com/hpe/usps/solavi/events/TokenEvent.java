package com.hpe.usps.solavi.events;

/**
 * Created by chishobr
 */
public class TokenEvent{
    private String token;

    public String getToken(){
        return token;
    }

    public TokenEvent(String token) {
        this.token = token;
    }
}
