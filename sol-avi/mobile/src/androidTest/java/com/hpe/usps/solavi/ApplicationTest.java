package com.hpe.usps.solavi;

import android.content.pm.PackageInfo;
import android.test.ApplicationTestCase;
import android.test.MoreAsserts;
import android.test.suitebuilder.annotation.MediumTest;

import com.hpe.usps.solavi.helpers.rest.modules.TestModule;

import javax.inject.Inject;

import retrofit.client.Client;

/**
 * Created by andrjone on 12/17/2015.
 */
public class ApplicationTest extends ApplicationTestCase<BaseApplication> {

    @Inject
    protected Client mClient;

    private BaseApplication application;

    public ApplicationTest() {
        super(BaseApplication.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Injector.add(new TestModule());
        Injector.inject(this);
        createApplication();
        application = getApplication();

    }

    @MediumTest
    public void testCorrectVersion() throws Exception {
        PackageInfo info = application.getPackageManager().getPackageInfo(application.getPackageName(), 0);
        assertNotNull(info);
        MoreAsserts.assertMatchesRegex("\\d\\.\\d", info.versionName);
    }

}

