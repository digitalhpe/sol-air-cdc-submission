import com.cloudbees.jenkins.plugins.BitBucketTrigger
import hudson.model.Hudson

// git credentials ID can be obtained by clicking on the "Credentials" link within jenkins and using the "ID" for the appropriate ssh key
static final String GIT_CREDENTIALS_ID = 'b54ebdf2-6b83-49d7-bf77-edb32b0e1cc8'
static final String GIT_URL = 'git@bitbucket.org:digitalhpe/sol-avi-v2-android.git'
static final String BUILD_FAILURE_RECIPIENTS = 'brian.dil.chisholm@dxc.com'
static final String SLACK_TEAM_DOMAIN = 'digitalservicesmobile'
static final String SLACK_TOKEN = '1NomBckbbxGIX3ZVjDK0CVuw'
static final String SLACK_CHANNEL = '#jenkins'
static final boolean SLACK_MESSAGE_ON_BUILD_START = false;
static final boolean SLACK_MESSAGE_ON_BUILD_SUCCESS = false;
static final boolean SLACK_MESSAGE_ON_BUILD_FAILURE = false;

enum BuildType {
    DEVELOPMENT('Development', 'Development - Logging enabled, debug functionality and signed with the debug key.'),
    RELEASE('Release', 'Release - Intended for publishing to the Play Store or Enterprise Deployment.  Logging disabled, debugging functionality disabled, and signed with the release key.  Publishes to Hockey App.'),

    final String id;
    final String desc;
    static Map map

    static {
        map = [:] as TreeMap
        // this requires script approval for the following signature in jenkins: "method java.lang.Object clone"
        // Manage Jenkins --> In-process Script Approval
        values().each{ buildType ->
            map.put(buildType.id, buildType)
        }
    }

    private BuildType(String id, String desc) {
        this.id = id
        this.desc = desc;
    }

    static BuildType getBuildType(String id) {
        map[id]
    }
}

// this defines the parameterized build properties for the jenkins project.  this can be done manually within jenkins,
// but configuring it here makes migration easier
properties([[$class: 'ParametersDefinitionProperty', parameterDefinitions:
        [[$class: 'ChoiceParameterDefinition',
          choices: [
                  BuildType.DEVELOPMENT.id,
                  BuildType.RELEASE.id
          ].join('\n'),
          description: [
                  BuildType.DEVELOPMENT.desc,
                  BuildType.RELEASE.desc
          ].join('\n'),
          name: 'buildType']
        ]
            ]])

// Build when a change is pushed to BitBucket
// this requires script approval for the following signatures in jenkins:
// "new com.cloudbees.jenkins.plugins.BitBucketTrigger",
// "staticMethod hudson.model.Hudson getInstance",
// "method org.jenkinsci.plugins.workflow.job.WorkflowJob addTrigger hudson.triggers.Trigger"
// Manage Jenkins --> In-process Script Approval
Hudson.instance.getItem("cdcsolair").addTrigger(new BitBucketTrigger())

// this job runs on the "android" build slave
node('android') {
    withEnv(["PATH=/usr/local/bin:/usr/bin"]) {
        try {
            if (SLACK_MESSAGE_ON_BUILD_START) {
                slackSend channel: SLACK_CHANNEL, color: 'good', message: "Build Started: ${env.JOB_NAME} #${env.BUILD_NUMBER}: ${env.BUILD_URL}", teamDomain: SLACK_TEAM_DOMAIN, token: SLACK_TOKEN
            }

            stage('Git Checkout') {
                git credentialsId: GIT_CREDENTIALS_ID, url: GIT_URL
                setWorkspace()
                setExecutePermissions()
            }

            BuildType paramBuildType

            // parameter definitions will not exist the first time this jenkins build is run.  this catch is added to set the default build type to 'development'
            try {
                paramBuildType = BuildType.getBuildType("${buildType}")
            } catch (err) {
                paramBuildType = BuildType.DEVELOPMENT
            }

            echo "Build type: ${paramBuildType}"

            currentBuild.result = 'SUCCESS'

            switch (paramBuildType) {
                case BuildType.DEVELOPMENT:
                    stage('Compile') {
//                        gradle 'clean assembleDebug assembleIntegrationTest'
                        gradle 'clean assembleDebug'
                    }
//                    stage('Lint') {
//                        gradle 'lintDebug'
//                        step([$class: 'LintPublisher', pattern: 'app/build/outputs/lint-results*.xml'])
//                    }
//                    stage('Unit Tests') {
//                        try {
//                            gradle 'testDebugUnitTest'
//                            step([$class: 'JUnitResultArchiver', testResults: 'app/build/test-results/*/TEST-*.xml'])
//                        } catch (err) {
//                            step([$class: 'JUnitResultArchiver', testResults: 'app/build/test-results/*/TEST-*.xml'])
//                            throw err
//                        }
//                    }
////                    stage('Code Coverage') {
////                        gradle 'testDebugUnitTestCoverage'
////                    }
//                    stage('Sonar') {
//                        gradle 'sonarqube'
//                    }
                    break
                case BuildType.RELEASE:
                    stage('Compile') {
                        gradle 'clean assembleRelease'
                    }
                    stage('HockeyApp') {
                        gradle 'uploadReleaseToHockeyApp'
                    }
                    break
                default:
                    echo "Unsupported build type!"
                    currentBuild.result = 'FAILURE'
                    break
            }
        } catch (err) {
            currentBuild.result = 'FAILURE'
            if (SLACK_MESSAGE_ON_BUILD_FAILURE) {
                slackSend channel: SLACK_CHANNEL, color: 'danger', message: "Build Failed: ${env.JOB_NAME} #${env.BUILD_NUMBER}: ${env.BUILD_URL}", teamDomain: SLACK_TEAM_DOMAIN, token: SLACK_TOKEN
            }
            throw err //rethrow exception to prevent the build from proceeding
        } finally {
            if (SLACK_MESSAGE_ON_BUILD_SUCCESS && currentBuild.result == 'SUCCESS') {
                slackSend channel: SLACK_CHANNEL, color: 'good', message: "Build Succeeded: ${env.JOB_NAME} #${env.BUILD_NUMBER}: ${env.BUILD_URL}", teamDomain: SLACK_TEAM_DOMAIN, token: SLACK_TOKEN
            }
            step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: BUILD_FAILURE_RECIPIENTS, sendToIndividuals: false])
        }
    }
}

// work-around: workspace environment variable is not available within jenkins pipeline projects
// this is needed for setting "sonar.projectBaseDir" property in app-level build.gradle
void setWorkspace() {
    env.WORKSPACE = pwd()
    echo "${env.WORKSPACE}"
}

// ensure gradle wrapper has execute permissions
void setExecutePermissions() {
    dir ('sol-avi') {
        sh 'chmod +x gradlew'
    }
}

void gradle(String tasks, String switches = null) {
    String gradleCommand = '';
    gradleCommand += './gradlew '
    gradleCommand += tasks

    if(switches != null) {
        gradleCommand += ' '
        gradleCommand += switches
    }

    dir ('sol-avi') {
        sh gradleCommand.toString()
    }
}