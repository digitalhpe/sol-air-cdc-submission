package com.hpe.usps.solavilibrary.models.aggregate;

/**
 * Created by chishobr on 6/15/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Measure {

    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("er")
    @Expose
    private Float er;
    @SerializedName("hospital")
    @Expose
    private Float hospital;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Float getEr() {
        return er;
    }

    public void setEr(Float er) {
        this.er = er;
    }

    public Float getHospital() {
        return hospital;
    }

    public void setHospital(Float hospital) {
        this.hospital = hospital;
    }

}

