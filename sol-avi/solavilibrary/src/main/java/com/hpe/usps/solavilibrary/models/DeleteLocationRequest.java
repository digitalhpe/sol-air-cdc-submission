package com.hpe.usps.solavilibrary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by chishobr
 */
public class DeleteLocationRequest {
    @SerializedName("locationid")
    @Expose
    private String mLocationid;

    public DeleteLocationRequest(String locationid){
        this.mLocationid = locationid;
    }

    public String getLocationid() {
        return mLocationid;
    }

    public void setLocationid(String locationid) {
        this.mLocationid = locationid;
    }
}
