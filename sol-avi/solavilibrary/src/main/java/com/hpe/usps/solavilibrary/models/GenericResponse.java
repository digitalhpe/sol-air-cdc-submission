package com.hpe.usps.solavilibrary.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericResponse {

    @SerializedName("result")
    @Expose
    private String mResult;
    @SerializedName("message")
    @Expose
    private String mMessage;

    /**
     *
     * @return
     * The mResult
     */
    public String getResult() {
        return mResult;
    }

    /**
     *
     * @param result
     * The mResult
     */
    public void setResult(String result) {
        this.mResult = result;
    }

    /**
     *
     * @return
     * The mMessage
     */
    public String getMessage() {
        return mMessage;
    }

    /**
     *
     * @param message
     * The mMessage
     */
    public void setMessage(String message) {
        this.mMessage = message;
    }

}