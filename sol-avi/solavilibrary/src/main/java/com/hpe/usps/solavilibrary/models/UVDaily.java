package com.hpe.usps.solavilibrary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UVDaily {

    @SerializedName("ZIP_CODE")
    @Expose
    private Integer mZipCode;
    @SerializedName("UV_INDEX")
    @Expose
    private Integer mUvIndex;
    @SerializedName("UV_ALERT")
    @Expose
    private Integer mUvAlert;

    /**
     *
     * @return
     * The mZipCode
     */
    public Integer getZipCode() {
        return mZipCode;
    }

    /**
     *
     * @param zipCode
     * The ZIP_CODE
     */
    public void setZipCode(Integer zipCode) {
        this.mZipCode = zipCode;
    }

    /**
     *
     * @return
     * The mUvIndex
     */
    public Integer getUvIndex() {
        return mUvIndex;
    }

    /**
     *
     * @param uvIndex
     * The UV_INDEX
     */
    public void setUvIndex(Integer uvIndex) {
        this.mUvIndex = uvIndex;
    }

    /**
     *
     * @return
     * The mUvAlert
     */
    public Integer getUvAlert() {
        return mUvAlert;
    }

    /**
     *
     * @param uvAlert
     * The UV_ALERT
     */
    public void setUvAlert(Integer uvAlert) {
        this.mUvAlert = uvAlert;
    }

}