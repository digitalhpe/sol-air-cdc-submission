package com.hpe.usps.solavilibrary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hpe.usps.solavilibrary.models.aggregate.AggregateResult;
import com.hpe.usps.solavilibrary.models.aggregate.Measure;

import java.util.List;

public class Location extends GenericResponse {

    @SerializedName("locationid")
    @Expose
    private String mLocationId;
    @SerializedName("uv")
    @Expose
    private Integer mUv;
    @SerializedName("ozone")
    @Expose
    private Integer mOzone;
    @SerializedName("uvalert")
    @Expose
    private Boolean mUvAlert;
    @SerializedName("aqialert")
    @Expose
    private Boolean mAqiAlert;
    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("latitude")
    @Expose
    private String mLatitude;
    @SerializedName("longitude")
    @Expose
    private String mLongitude;
    @SerializedName("zip")
    @Expose
    private String mZip;
    private boolean isCurrent = false;
    @SerializedName("park")
    @Expose
    private Double park;
    @SerializedName("flood")
    @Expose
    private Double flood;
    @SerializedName("measures")
    @Expose
    private List<Measure> measures = null;
    @SerializedName("state")
    @Expose
    private String mStateId;
    @SerializedName("county")
    @Expose
    private String mCounty;


    public String getStateId() {
        return mStateId;
    }

    /**
     *
     * @param stateId
     * The State Id
     */
    public void setStateId(String stateId) {
        this.mStateId = stateId;
    }

    public String getCounty() {
        return mCounty;
    }

    public void setCounty(String county) {
        this.mCounty = county;
    }

    public List<Measure> getMeasures(){
        return measures;
    }

    public void setMeasures(List<Measure> measures){
        this.measures = measures;
    }

    public Double getPark(){
        return park;
    }

    public void setFlood(double flood){
        this.flood = flood;
    }

    public Double getFlood(){
        return flood;
    }

    public void setPark(double park){
        this.park = park;
    }

    /**
     *
     * @return
     *     The mLocationId
     */
    public String getLocationid() {
        return mLocationId;
    }

    /**
     *
     * @param locationid
     *     The mLocationId
     */
    public void setLocationid(String locationid) {
        this.mLocationId = locationid;
    }

    /**
     *
     * @return
     *     The mUv
     */
    public Integer getUv() {
        return mUv;
    }

    /**
     *
     * @param uv
     *     The mUv
     */
    public void setUv(Integer uv) {
        this.mUv = uv;
    }

    /**
     *
     * @return
     *     The mOzone
     */
    public Integer getOzone() {
        return mOzone;
    }

    /**
     *
     * @param ozone
     *     The mOzone
     */
    public void setOzone(Integer ozone) {
        this.mOzone = ozone;
    }

    /**
     *
     * @return
     *     The mUvAlert
     */
    public Boolean getUvalert() {
        return mUvAlert;
    }

    /**
     *
     * @param uvalert
     *     The mUvAlert
     */
    public void setUvalert(Boolean uvalert) {
        this.mUvAlert = uvalert;
    }

    /**
     *
     * @return
     *     The mAqiAlert
     */
    public Boolean getAqialert() {
        return mAqiAlert;
    }

    /**
     *
     * @param aqialert
     *     The mAqiAlert
     */
    public void setAqialert(Boolean aqialert) {
        this.mAqiAlert = aqialert;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    /**
     *
     * @return
     *     The mLatitude
     */
    public Double getLatitude() {
        if (mLatitude.isEmpty()) {
            return 0.0;
        } else {
            return Double.parseDouble(mLatitude);
        }
    }

    public String getLatitudeRaw(){
        return mLatitude;
    }

    /**
     *
     * @param latitude
     *     The mLatitude
     */
    public void setLatitude(Double latitude) {
        this.mLatitude = String.valueOf(latitude);
    }

    /**
     *
     * @return
     *     The mLongitude
     */
    public Double getLongitude() {
        if (mLongitude.isEmpty()) {
            return 0.0;
        } else {
            return Double.parseDouble(mLongitude);
        }
    }

    public String getLongitudeRaw(){
        return mLongitude;
    }

    /**
     *
     * @param longitude
     *     The mLongitude
     */
    public void setLongitude(Double longitude) {
        this.mLongitude = String.valueOf(longitude);
    }

    public String getZip() {
        return mZip;
    }

    public void setZip(String zip) {
        this.mZip = zip;
    }

    public boolean getIsCurrent() {
        return isCurrent;
    }

    public void setCurrent(){
        isCurrent = true;
    }
}