package com.hpe.usps.solavilibrary.rest;

import com.hpe.usps.solavilibrary.Constants;
import com.hpe.usps.solavilibrary.models.Parks.ParkLocation;
import com.hpe.usps.solavilibrary.models.googlelocation.LatLng;

import java.util.List;

import retrofit.http.EncodedQuery;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by chishobr on 8/14/2017.
 */

public interface PlacesService {

    @GET("/json?types=park&sensor=true&key="+ Constants.GOOGLE_GEO_KEY)
    Observable<ParkLocation> getParkLocations(@EncodedQuery("location") LatLng latLng, @Query("radius") int radius);
}
