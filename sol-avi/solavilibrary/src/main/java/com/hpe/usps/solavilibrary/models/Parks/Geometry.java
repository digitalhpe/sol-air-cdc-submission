package com.hpe.usps.solavilibrary.models.Parks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hpe.usps.solavilibrary.models.googlelocation.Viewport;

/**
 * Created by chishobr on 8/14/2017.
 */


public class Geometry {

    @SerializedName("location")
    @Expose
    private ParkLatLng location;
    @SerializedName("viewport")
    @Expose
    private Viewport viewport;

    public ParkLatLng getLocation() {
        return location;
    }

    public void setLocation(ParkLatLng location) {
        this.location = location;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setViewport(Viewport viewport) {
        this.viewport = viewport;
    }

}
