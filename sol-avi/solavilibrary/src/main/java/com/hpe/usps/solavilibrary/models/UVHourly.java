package com.hpe.usps.solavilibrary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UVHourly {

    @SerializedName("order")
    @Expose
    private Integer mOrder;
    @SerializedName("zip")
    @Expose
    private Integer mZip;
    @SerializedName("DATE_TIME")
    @Expose
    private String mDateTime;
    @SerializedName("UV_VALUE")
    @Expose
    private Integer mUvValue;

    /**
     *
     * @return
     * The mOrder
     */
    public Integer getOrder() {
        return mOrder;
    }

    /**
     *
     * @param order
     * The mOrder
     */
    public void setOrder(Integer order) {
        this.mOrder = order;
    }

    /**
     *
     * @return
     * The mZip
     */
    public Integer getZip() {
        return mZip;
    }

    /**
     *
     * @param zip
     * The mZip
     */
    public void setZip(Integer zip) {
        this.mZip = zip;
    }

    /**
     *
     * @return
     * The mDateTime
     */
    public String getDateTime() {
        return mDateTime;
    }

    /**
     *
     * @param dateTime
     * The DATE_TIME
     */
    public void setDateTime(String dateTime) {
        this.mDateTime = dateTime;
    }

    /**
     *
     * @return
     * The mUvValue
     */
    public Integer getUvValue() {
        return mUvValue;
    }

    /**
     *
     * @param uvValue
     * The UV_VALUE
     */
    public void setUvValue(Integer uvValue) {
        this.mUvValue = uvValue;
    }

}