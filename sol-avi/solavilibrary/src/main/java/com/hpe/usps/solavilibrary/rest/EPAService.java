package com.hpe.usps.solavilibrary.rest;

import com.hpe.usps.solavilibrary.models.UVDaily;
import com.hpe.usps.solavilibrary.models.UVHourly;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by Andy Jones
 *
 * Retrofit interfaces for EPA web service
 */
public interface EPAService {

    /**
     * Retrieves the hourly UV index list
     * @param zip zip code
     * @return list of hourly UV indexes
     */
    @GET("/getEnvirofactsUVHOURLY/ZIP/{zip}/JSON")
    Observable<List<UVHourly>> getHourlyUVIndex(@Path("zip") String zip);

    /**
     * Retrieves the daily UV index list
     * @param zip zip code
     * @return list of daily UV indexes
     */
    @GET("/getEnvirofactsUVDAILY/ZIP/{zip}/JSON")
    Observable<List<UVDaily>> getDailyUVIndex(@Path("zip") String zip);
}