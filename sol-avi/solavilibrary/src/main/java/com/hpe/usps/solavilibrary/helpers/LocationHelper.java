package com.hpe.usps.solavilibrary.helpers;

import com.hpe.usps.solavilibrary.Constants;
import com.hpe.usps.solavilibrary.models.CurrentObservation;
import com.hpe.usps.solavilibrary.models.Location;
import com.hpe.usps.solavilibrary.models.LocationDetail;
import com.hpe.usps.solavilibrary.models.LocationSummary;
import com.hpe.usps.solavilibrary.models.Parks.ParkLocation;
import com.hpe.usps.solavilibrary.models.UVDaily;
import com.hpe.usps.solavilibrary.models.googlelocation.LatLng;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.GoogleLocationService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by Andy Jones
 *
 * Helper functions for location related tasks
 */
public class LocationHelper {

    private static Map<String, Integer> stateValues;
    AirNowService mAirNowService;
    EPAService mEPAService;
    SolaviService mSolaviService;
    GeoHelper mGeoHelper;
    PreferenceHelper mPreferenceHelper;
    PlacesService placesService;


    /**
     * Constructor for LocationHelper.  Dependencies are added through constructor for easy mocking
     * and unit testing.
     * @param preferenceHelper
     * @param geoHelper
     * @param epaService
     * @param airNowService
     * @param solaviService
     */
    public LocationHelper(PreferenceHelper preferenceHelper, GeoHelper geoHelper,
                          EPAService epaService, AirNowService airNowService, SolaviService solaviService, PlacesService placesService) {
        mPreferenceHelper = preferenceHelper;
        mGeoHelper = geoHelper;
        mEPAService = epaService;
        mAirNowService = airNowService;
        mSolaviService = solaviService;
        this.placesService = placesService;
    }

    /**
     * This method retrieves the list of locations from the web service and ensures that the favorite
     * location is listed first.
     * @param pushToken GCM push token
     * @return Observable containing LocationSummary
     */
    public Observable<LocationSummary> getLocations(String pushToken) {
        return mSolaviService.getLocationSummary(pushToken)
                .flatMap(new Func1<LocationSummary, Observable<LocationSummary>>() {
                    @Override
                    public Observable<LocationSummary> call(final LocationSummary locationSummary) {

                        return Observable.just(locationSummary);
                    }
                });
    }

    public Observable<ParkLocation> getParkLocations(LatLng latLng, int radius) {
        return placesService.getParkLocations(latLng, radius)
                .flatMap(new Func1<ParkLocation, Observable<ParkLocation>>() {
                    @Override
                    public Observable<ParkLocation> call(final ParkLocation parkLocation) {

                        return Observable.just(parkLocation);
                    }
                });
    }

    /**
     * This method retrieves the zip code based on the lat / long and passes the results into the
     * web service calls for uv index and air quality.  All results are combined into an observable.
     * @param latitude
     * @param longitude
     * @return Observable containing Location
     */
    public Observable<Location> getIndexesForLocation(final double latitude, final double longitude) {
        return mGeoHelper.getZipFromLatLong(latitude, longitude)
                .flatMap(new Func1<String, Observable<Location>>() {
                    @Override
                    public Observable<Location> call(final String zip) {
                        return Observable.zip(
                                mEPAService.getDailyUVIndex(zip),
                                mAirNowService.getCurrentObservationByZip(zip),
                                new Func2<List<UVDaily>, List<CurrentObservation>, Location>() {
                                    @Override
                                    public Location call(List<UVDaily> uvDailies, List<CurrentObservation> currentObservations) {
                                        Location location = new Location();

                                        location.setName(zip);
                                        location.setLatitude(latitude);
                                        location.setLongitude(longitude);
                                        location.setZip(zip);
                                        location.setUv(uvDailies.get(0).getUvIndex());

                                        for (CurrentObservation currentObservation : currentObservations) {
                                            if (currentObservation.getParameterName().equals(Constants.PARAMETER_NAME_OZONE)) {
                                                location.setOzone(currentObservation.getAQI());
                                                break;
                                            }
                                        }

                                        return location;
                                    }
                                }

                        );
                    }
                });
    }

    /**
     * This method retrieves the zip code based on the lat / long and passes the results into the
     * web service call for the location detail information.  All results are combined into an observable.
     * @param latitude
     * @param longitude
     * @return Observable containing LocationDetail
     */
    public Observable<LocationDetail> getDetailForLocation(final String locationId, final double latitude, final double longitude) {
        return mGeoHelper.getZipFromLatLong(latitude, longitude)
                .flatMap(new Func1<String, Observable<LocationDetail>>() {
                    @Override
                    public Observable<LocationDetail> call(final String zip) {
                        return mSolaviService.getLocationByCoordinates(locationId, zip);
                    }
                });
    }

    /**
     * Helper function to ensure that a new / existing location is setup properly. Returns a new location
     * object if locationId == null or 0.  If a locationId is found to be a match with the
     * given location list, then the matching location in the location list is returned.
     * @param locationId
     * @param latitude
     * @param longitude
     * @param locationList
     * @return Location
     */
    public Location createLocation(String locationId, double latitude, double longitude, List<Location> locationList) {
        Location newLocation = null;

        if (locationId != null && !"0".equals(locationId)) {
            for (Location location : locationList) {
                if (location.getLocationid().equals(locationId)) {
                    newLocation = location;
                    break;
                }
            }
        } else {
            newLocation = new Location();

            newLocation.setLocationid("0");
            newLocation.setLatitude(latitude);
            newLocation.setLongitude(longitude);
        }

        return newLocation;
    }

    public static boolean isUnhealtyAqi(int ozone) {
        return ozone >= 51;
    }
    public static boolean isUnhealtyUv(int uv) {
        return uv >= 3;
    }

    public Integer getStateIDByAbbv(String stateAbbv) {
        return getStateMap().get(stateAbbv);
    }

    private static Map<String, Integer> getStateMap(){
        if (stateValues == null) {
            stateValues = new HashMap<String, Integer>();
            stateValues.put("AL", 1);
            stateValues.put("AK", 2);
            stateValues.put("AZ", 4);
            stateValues.put("AR", 5);
            stateValues.put("CA", 6);
            stateValues.put("CO", 8);
            stateValues.put("CT", 9);
            stateValues.put("DE", 10);
            stateValues.put("DC", 11);
            stateValues.put("FL", 12);
            stateValues.put("GA", 13);
            stateValues.put("HI", 15);
            stateValues.put("ID", 16);
            stateValues.put("IL", 17);
            stateValues.put("IN", 18);
            stateValues.put("IA", 19);
            stateValues.put("KS", 20);
            stateValues.put("KY", 21);
            stateValues.put("LA", 22);
            stateValues.put("ME", 23);
            stateValues.put("MD", 24);
            stateValues.put("MA", 25);
            stateValues.put("MI", 26);
            stateValues.put("MN", 27);
            stateValues.put("MS", 28);
            stateValues.put("MO", 29);
            stateValues.put("MT", 30);
            stateValues.put("NE", 31);
            stateValues.put("NV", 32);
            stateValues.put("NH", 33);
            stateValues.put("NJ", 34);
            stateValues.put("NM", 35);
            stateValues.put("NY", 36);
            stateValues.put("NC", 37);
            stateValues.put("ND", 38);
            stateValues.put("OH", 39);
            stateValues.put("OK", 40);
            stateValues.put("OR", 41);
            stateValues.put("PA", 42);
            stateValues.put("RI", 44);
            stateValues.put("SC", 45);
            stateValues.put("SD", 46);
            stateValues.put("TN", 47);
            stateValues.put("TX", 48);
            stateValues.put("UT", 49);
            stateValues.put("VT", 50);
            stateValues.put("VA", 51);
            stateValues.put("WA", 53);
            stateValues.put("WV", 54);
            stateValues.put("WI", 55);
            stateValues.put("WY", 56);
        }
        return stateValues;

    }

}
