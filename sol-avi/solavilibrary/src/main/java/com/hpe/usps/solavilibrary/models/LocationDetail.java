
package com.hpe.usps.solavilibrary.models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationDetail {

    @SerializedName("locationid")
    @Expose
    private String mLocationId;
    @SerializedName("uv")
    @Expose
    @Nullable
    private Integer mUv;
    @SerializedName("aqi")
    @Expose
    private Aqi mAqi;
    @SerializedName("notify")
    @Expose
    private Boolean mNotify;
    @SerializedName("os")
    @Expose
    private String mOs;
    @SerializedName("latitude")
    @Expose
    private String mLatitude;
    @SerializedName("longitude")
    @Expose
    private String mLongitude;
    @SerializedName("zip")
    @Expose
    private String mZip;
    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("history")
    @Expose
    private History mHistory;

    /**
     * 
     * @return
     *     The mLocationId
     */
    public String getLocationid() {
        return mLocationId;
    }

    /**
     * 
     * @param locationid
     *     The mLocationId
     */
    public void setLocationid(String locationid) {
        this.mLocationId = locationid;
    }

    /**
     * 
     * @return
     *     The mUv
     */
    public Integer getUv() {
        return mUv;
    }

    /**
     * 
     * @param uv
     *     The mUv
     */
    public void setUv(Integer uv) {
        this.mUv = uv;
    }

    /**
     * 
     * @return
     *     The mAqi
     */
    public Aqi getAqi() {
        return mAqi;
    }

    /**
     * 
     * @param aqi
     *     The mAqi
     */
    public void setAqi(Aqi aqi) {
        this.mAqi = aqi;
    }

    /**
     * 
     * @return
     *     The mLatitude
     */
    public Double getLatitude() {
        if (mLatitude.isEmpty()) {
            return 0.0;
        } else {
            return Double.parseDouble(mLatitude);
        }
    }

    /**
     * 
     * @param latitude
     *     The mLatitude
     */
    public void setLatitude(Double latitude) {
        this.mLatitude = String.valueOf(latitude);
    }

    /**
     * 
     * @return
     *     The mLongitude
     */
    public Double getLongitude() {
        if (mLongitude.isEmpty()) {
            return 0.0;
        } else {
            return Double.parseDouble(mLongitude);
        }
    }

    /**
     * 
     * @param longitude
     *     The mLongitude
     */
    public void setLongitude(Double longitude) {
        this.mLongitude = String.valueOf(longitude);
    }

    /**
     * 
     * @return
     *     The mZip
     */
    public String getZip() {
        return mZip;
    }

    /**
     * 
     * @param zip
     *     The mZip
     */
    public void setZip(String zip) {
        this.mZip = zip;
    }

    /**
     * 
     * @return
     *     The mName
     */
    public String getName() {
        return mName;
    }

    /**
     * 
     * @param name
     *     The mName
     */
    public void setName(String name) {
        this.mName = name;
    }

    /**
     * 
     * @return
     *     The mHistory
     */
    public History getHistory() {
        return mHistory;
    }

    /**
     * 
     * @param history
     *     The mHistory
     */
    public void setHistory(History history) {
        this.mHistory = history;
    }

    public Boolean getNotify() {
        return mNotify;
    }

    public void setNotify(Boolean notify) {
        this.mNotify = notify;
    }

    public String getOs() {
        return mOs;
    }

    public void setOs(String os) {
        this.mOs = os;
    }

    public class Aqi {

        @SerializedName("o3")
        @Expose
        private Integer mO3;
        @SerializedName("pm10")
        @Expose
        private Integer mPm10;
        @SerializedName("pm25")
        @Expose
        private Integer mPm25;

        /**
         *
         * @return
         *     The mO3
         */
        public Integer getO3() {
            return mO3;
        }

        /**
         *
         * @param o3
         *     The mO3
         */
        public void setO3(Integer o3) {
            this.mO3 = o3;
        }

        /**
         *
         * @return
         *     The mPm10
         */
        public Integer getPm10() {
            return mPm10;
        }

        /**
         *
         * @param pm10
         *     The mPm10
         */
        public void setPm10(Integer pm10) {
            this.mPm10 = pm10;
        }

        /**
         *
         * @return
         *     The mPm25
         */
        public Integer getPm25() {
            return mPm25;
        }

        /**
         *
         * @param pm25
         *     The mPm25
         */
        public void setPm25(Integer pm25) {
            this.mPm25 = pm25;
        }
    }

    public class History {

        @SerializedName("aqi")
        @Expose
        private Aqi mAqi;
        @SerializedName("aqiplus")
        @Expose
        private Aqi mAqiPlus;
        @SerializedName("aqiminus")
        @Expose
        private Aqi mAqiMinus;

        /**
         *
         * @return
         *     The mAqi
         */
        public Aqi getAqi() {
            return mAqi;
        }

        /**
         *
         * @param aqi
         *     The mAqi
         */
        public void setAqi(Aqi aqi) {
            this.mAqi = aqi;
        }

        /**
         *
         * @return
         *     The aqiplus
         */
        public Aqi getAqiplus() {
            return mAqiPlus;
        }

        /**
         *
         * @param aqiplus
         *     The aqiplus
         */
        public void setAqiplus(Aqi aqiplus) {
            this.mAqiPlus = aqiplus;
        }

        /**
         *
         * @return
         *     The aqiminus
         */
        public Aqi getAqiminus() {
            return mAqiMinus;
        }

        /**
         *
         * @param aqiminus
         *     The aqiminus
         */
        public void setAqiminus(Aqi aqiminus) {
            this.mAqiMinus = aqiminus;
        }

    }
}
