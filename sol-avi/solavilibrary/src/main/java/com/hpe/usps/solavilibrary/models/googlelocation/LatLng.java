package com.hpe.usps.solavilibrary.models.googlelocation;

/**
 * Created by chishobr on 6/20/2017.
 */

public class LatLng {
    private double lat;
    private double lng;

    public LatLng (Double lat, Double lng){
        this.lat = lat;
        this.lng = lng;
    }

    @Override public String toString() {
        return String.format("%.6f,%.6f", lat, lng);
    }
}
