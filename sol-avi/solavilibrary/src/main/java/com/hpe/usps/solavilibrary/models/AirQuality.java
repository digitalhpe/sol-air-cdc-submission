
package com.hpe.usps.solavilibrary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirQuality {

    @SerializedName("DateIssue")
    @Expose
    private String mDateIssue;
    @SerializedName("DateForecast")
    @Expose
    private String mDateForecast;
    @SerializedName("ReportingArea")
    @Expose
    private String mReportingArea;
    @SerializedName("StateCode")
    @Expose
    private String mStateCode;
    @SerializedName("Latitude")
    @Expose
    private Double mLatitude;
    @SerializedName("Longitude")
    @Expose
    private Double mLongitude;
    @SerializedName("ParameterName")
    @Expose
    private String mParameterName;
    @SerializedName("AQI")
    @Expose
    private Integer mAQI;
    @SerializedName("Category")
    @Expose
    private Category mCategory;
    @SerializedName("ActionDay")
    @Expose
    private Boolean mActionDay;
    @SerializedName("Discussion")
    @Expose
    private String mDiscussion;

    /**
     * 
     * @return
     *     The DateIssue
     */
    public String getDateIssue() {
        return mDateIssue;
    }

    /**
     * 
     * @param dateIssue
     *     The dateIssue
     */
    public void setDateIssue(String dateIssue) {
        this.mDateIssue = dateIssue;
    }

    /**
     * 
     * @return
     *     The DateForecast
     */
    public String getDateForecast() {
        return mDateForecast;
    }

    /**
     * 
     * @param dateForecast
     *     The dateForecast
     */
    public void setDateForecast(String dateForecast) {
        this.mDateForecast = dateForecast;
    }

    /**
     * 
     * @return
     *     The ReportingArea
     */
    public String getReportingArea() {
        return mReportingArea;
    }

    /**
     * 
     * @param reportingArea
     *     The reportingArea
     */
    public void setReportingArea(String reportingArea) {
        this.mReportingArea = reportingArea;
    }

    /**
     * 
     * @return
     *     The StateCode
     */
    public String getStateCode() {
        return mStateCode;
    }

    /**
     * 
     * @param stateCode
     *     The stateCode
     */
    public void setStateCode(String stateCode) {
        this.mStateCode = stateCode;
    }

    /**
     * 
     * @return
     *     The Latitude
     */
    public Double getLatitude() {
        return mLatitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(Double latitude) {
        this.mLatitude = latitude;
    }

    /**
     * 
     * @return
     *     The Longitude
     */
    public Double getLongitude() {
        return mLongitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(Double longitude) {
        this.mLongitude = longitude;
    }

    /**
     * 
     * @return
     *     The ParameterName
     */
    public String getParameterName() {
        return mParameterName;
    }

    /**
     * 
     * @param parameterName
     *     The parameterName
     */
    public void setParameterName(String parameterName) {
        this.mParameterName = parameterName;
    }

    /**
     * 
     * @return
     *     The AQI
     */
    public Integer getAQI() {
        return mAQI;
    }

    /**
     * 
     * @param aqi
     *     The aqi
     */
    public void setAQI(Integer aqi) {
        this.mAQI = aqi;
    }

    /**
     * 
     * @return
     *     The Category
     */
    public Category getCategory() {
        return mCategory;
    }

    /**
     * 
     * @param category
     *     The category
     */
    public void setCategory(Category category) {
        this.mCategory = category;
    }

    /**
     * 
     * @return
     *     The ActionDay
     */
    public Boolean getActionDay() {
        return mActionDay;
    }

    /**
     * 
     * @param actionDay
     *     The actionDay
     */
    public void setActionDay(Boolean actionDay) {
        this.mActionDay = actionDay;
    }

    /**
     * 
     * @return
     *     The Discussion
     */
    public String getDiscussion() {
        return mDiscussion;
    }

    /**
     * 
     * @param discussion
     *     The discussion
     */
    public void setDiscussion(String discussion) {
        this.mDiscussion = discussion;
    }

    public class Category {

        @SerializedName("Number")
        @Expose
        private Integer mNumber;
        @SerializedName("Name")
        @Expose
        private String mName;

        /**
         *
         * @return
         *     The Number
         */
        public Integer getNumber() {
            return mNumber;
        }

        /**
         *
         * @param number
         *     The number
         */
        public void setNumber(Integer number) {
            this.mNumber = number;
        }

        /**
         *
         * @return
         *     The Name
         */
        public String getName() {
            return mName;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.mName = name;
        }

    }

}
