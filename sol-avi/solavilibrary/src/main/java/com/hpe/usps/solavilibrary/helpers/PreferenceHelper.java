package com.hpe.usps.solavilibrary.helpers;

/**
 * Created by Andy Jones
 */
public interface PreferenceHelper {

    String getPushToken();

    void setPushToken(String pushToken);

}
