package com.hpe.usps.solavilibrary.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddLocationRequest {

    @SerializedName("locationid")
    @Expose
    private String mLocationid;
    @SerializedName("pushid")
    @Expose
    private String mPushid;
    @SerializedName("latitude")
    @Expose
    private String mLatitude;
    @SerializedName("longitude")
    @Expose
    private String mLongitude;
    @SerializedName("zip")
    @Expose
    private String mZip;
    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("notify")
    @Expose
    private Boolean mNotify;
    @SerializedName("os")
    @Expose
    private static final String mOs = "android";
    @SerializedName("state")
    @Expose
    private String mStateId;
    @SerializedName("county")
    @Expose
    private String mCounty;

    public AddLocationRequest(String locationid, String pushid, Double latitude, Double longitude, String zip, String name, Boolean notify,
                              String stateId, String county) {
        this.mLocationid = locationid;
        this.mPushid = pushid;
        this.mLatitude = String.valueOf(latitude);
        this.mLongitude = String.valueOf(longitude);
        this.mZip = zip;
        this.mName = name;
        this.mNotify = notify;
        this.mStateId = stateId;
        this.mCounty = county;
    }

    public String getLocationid() {
        return mLocationid;
    }

    public void setLocationid(String locationid) {
        this.mLocationid = locationid;
    }

    /**
     *
     * @return
     * The pushid
     */
    public String getPushid() {
        return mPushid;
    }

    /**
     *
     * @param pushid
     * The pushid
     */
    public void setPushid(String pushid) {
        this.mPushid = pushid;
    }

    /**
     *
     * @return
     *     The latitude
     */
    public Double getLatitude() {
        if (mLatitude.isEmpty()) {
            return 0.0;
        } else {
            return Double.parseDouble(mLatitude);
        }
    }

    /**
     *
     * @param latitude
     *     The latitude
     */
    public void setLatitude(Double latitude) {
        this.mLatitude = String.valueOf(latitude);
    }

    /**
     *
     * @return
     *     The longitude
     */
    public Double getLongitude() {
        if (mLongitude.isEmpty()) {
            return 0.0;
        } else {
            return Double.parseDouble(mLongitude);
        }
    }

    /**
     *
     * @param longitude
     *     The longitude
     */
    public void setLongitude(Double longitude) {
        this.mLongitude = String.valueOf(longitude);
    }

    /**
     *
     * @return
     * The zip
     */
    public String getZip() {
        return mZip;
    }

    /**
     *
     * @param zip
     * The zip
     */
    public void setZip(String zip) {
        this.mZip = zip;
    }

    /**
     *
     * @return
     * The stateId
     */
    public String getStateId() {
        return mStateId;
    }

    /**
     *
     * @param stateId
     * The State Id
     */
    public void setStateId(String stateId) {
        this.mStateId = stateId;
    }

    public String getCounty() {
        return mCounty;
    }

    public void setCounty(String county) {
        this.mCounty = county;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public Boolean getNotify() {
        return mNotify;
    }

    public void setNotify(Boolean notify) {
        this.mNotify = notify;
    }

    public String getOs() {
        return mOs;
    }
}