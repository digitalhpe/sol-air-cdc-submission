package com.hpe.usps.solavilibrary.rest;

import com.hpe.usps.solavilibrary.Constants;
import com.hpe.usps.solavilibrary.models.CurrentObservation;
import com.hpe.usps.solavilibrary.models.AirQuality;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Andy Jones
 *
 * Retrofit interfaces for AirNow web service
 */
public interface AirNowService {

    /**
     * Gets air quality based on zip code
     * @param zip zip code
     * @return Air quality measurements
     */
    @GET("/aq/forecast/zipCode?format=application/json&distance=25&API_KEY=" + Constants.AIRNOW_API_KEY)
    Observable<List<AirQuality>> getAirQualityByZip(@Query("zipCode") String zip);

    /**
     * Gets air quality based on zip code and a given date
     * @param zip zip code
     * @param date date (mm-dd-yyyy)
     * @return
     */
    @GET("/aq/forecast/zipCode?format=application/json&distance=25&API_KEY=" + Constants.AIRNOW_API_KEY)
    Observable<List<AirQuality>> getAirQualityByZipAndDate(@Query("zipCode") String zip, @Query("date") String date);

    /**
     * Gets air quality based on lat / lng
     * @param latitude latitude
     * @param longitude longitude
     * @return Air quality measurements
     */
    @GET("/aq/forecast/latLong?format=application/json&distance=25&API_KEY=" + Constants.AIRNOW_API_KEY)
    Observable<List<AirQuality>> getAirQualityByLatLong(@Query("latitude") double latitude, @Query("longitude") double longitude);

    /**
     * Gets air quality based on lat / lng and a given date
     * @param latitude latitude
     * @param longitude longitude
     * @param date date (mm-dd-yyyy)
     * @return Air quality measurements
     */
    @GET("/aq/forecast/latLong?format=application/json&distance=25&API_KEY=" + Constants.AIRNOW_API_KEY)
    Observable<List<AirQuality>> getAirQualityByLatLongAndDate(@Query("latitude") double latitude, @Query("longitude") double longitude, @Query("date") String date);

    /**
     * Get current observations based on zip code
     * @param zip zip code
     * @return Current observations
     */
    @GET("/aq/observation/zipCode/current?format=application/json&distance=25&API_KEY=" + Constants.AIRNOW_API_KEY)
    Observable<List<CurrentObservation>> getCurrentObservationByZip(@Query("zipCode") String zip);

    /**
     * Get current observations based on lat / lng
     * @param latitude latitude
     * @param longitude longitude
     * @return Current observations
     */
    @GET("/aq/observation/latLong/current?format=application/json&distance=25&API_KEY=" + Constants.AIRNOW_API_KEY)
    Observable<List<CurrentObservation>> getCurrentObservationByLatLong(@Query("latitude") double latitude, @Query("longitude") double longitude);
}