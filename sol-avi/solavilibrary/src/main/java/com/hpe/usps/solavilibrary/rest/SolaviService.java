package com.hpe.usps.solavilibrary.rest;

import com.hpe.usps.solavilibrary.models.AddLocationResponse;
import com.hpe.usps.solavilibrary.models.aggregate.AggregateResult;
import com.hpe.usps.solavilibrary.models.DeleteLocationRequest;
import com.hpe.usps.solavilibrary.models.GenericResponse;
import com.hpe.usps.solavilibrary.models.LocationSummary;
import com.hpe.usps.solavilibrary.models.AddLocationRequest;
import com.hpe.usps.solavilibrary.models.LocationDetail;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by Andy Jones
 */
public interface SolaviService {

    /**
     * Retrieves the list of saved locations based on the GCM push token
     * @param pushId Google Cloud Messaging push token
     * @return summary list of saved locations
     */
    @GET("/summary/{pushId}")
    Observable<LocationSummary> getLocationSummary(@Path("pushId") String pushId);

    /**
     * Returns air quality, uv indexes, and historical data based on the GCM push token
     * @param locationId location id
     * @param zip zip code
     * @return location details
     */
    @GET("/location/{locationid}/{zip}")
    Observable<LocationDetail> getLocationByCoordinates(@Path("locationid") String locationId, @Path("zip") String zip);

    /**
     * Retrieves historical location information
     * @param zip zip code
     * @param epochDate number of seconds in epoch time
     * @return location history
     */
    @GET("/location/history/{zip}/{date}")
    Observable<LocationDetail.History> getLocationHistory(@Path("zip") String zip, @Path("date") Long epochDate);

    /**
     * Saves a location to the server
     * @param addLocationRequest request containing location information
     * @return add location response
     */
    @POST("/location/add")
    Observable<AddLocationResponse> addLocation(@Body AddLocationRequest addLocationRequest);

    /**
     * Deletes a location from the server
     * @param deleteLocationRequest request containing location information
     * @return generic response
     */
    @POST("/location/delete")
    Observable<GenericResponse> deleteLocation(@Body DeleteLocationRequest deleteLocationRequest);

    @GET("/cdc/{state}/{county}")
    Observable<AggregateResult> getAggregateData(@Path("state")int stateId, @Path("county")String county);

}