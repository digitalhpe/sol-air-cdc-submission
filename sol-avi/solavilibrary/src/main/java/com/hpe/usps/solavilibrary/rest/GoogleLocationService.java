package com.hpe.usps.solavilibrary.rest;

import com.hpe.usps.solavilibrary.models.LocationDetail;
import com.hpe.usps.solavilibrary.models.googlelocation.LatLng;
import com.hpe.usps.solavilibrary.models.googlelocation.Location;

import retrofit.http.EncodedQuery;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by chishobr on 6/20/2017.
 */

public interface GoogleLocationService {

    @GET("/json")
    Observable<Location> getLocationByCoordinates(@EncodedQuery("latlng") String latlng, @Query("sensor") boolean sensor);
}
