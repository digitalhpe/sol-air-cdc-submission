package com.hpe.usps.solavilibrary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Andy Jones
 */
public class AddLocationResponse extends GenericResponse {

    @SerializedName("locationid")
    @Expose
    private String mLocationId;

    public String getLocationId() {
        return mLocationId;
    }

    public void setLocationId(String locationId) {
        this.mLocationId = locationId;
    }
}
