package com.hpe.usps.solavilibrary.models;

import java.util.Calendar;

/**
 * Created by chishobr
 */
public class DatePickerEvent {
    private Calendar mDate;

    public Calendar getDate(){
        return mDate;
    }

    public DatePickerEvent(Calendar date) {
        this.mDate = date;
    }
}
