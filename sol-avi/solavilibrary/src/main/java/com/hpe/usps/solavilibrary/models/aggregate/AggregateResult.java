package com.hpe.usps.solavilibrary.models.aggregate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by chishobr on 6/15/2017.
 */

public class AggregateResult {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("county")
    @Expose
    private String county;
    @SerializedName("park")
    @Expose
    private Double park;
    @SerializedName("flood")
    @Expose
    private Double flood;
    @SerializedName("measures")
    @Expose
    private List<Measure> measures = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Double getPark() {
        return park;
    }

    public void setPark(Double park) {
        this.park = park;
    }

    public Double getFlood() {
        return flood;
    }

    public void setFlood(Double flood) {
        this.flood = flood;
    }

    public List<Measure> getMeasures() {
        return measures;
    }

    public void setMeasures(List<Measure> measures) {
        this.measures = measures;
    }

}