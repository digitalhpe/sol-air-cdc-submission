package com.hpe.usps.solavilibrary.helpers;

import android.content.Context;

import rx.Observable;

/**
 * Created by Andy Jones
 */
public interface GeoHelper {

    Observable<String> getZipFromLatLong(double latitude, double longitude);
    Observable<String> getLocationAddress(double latitude, double longitude, boolean justLocation);
    String getStateAbbv(String stateString);

    boolean hasInternet(Context context);
}
