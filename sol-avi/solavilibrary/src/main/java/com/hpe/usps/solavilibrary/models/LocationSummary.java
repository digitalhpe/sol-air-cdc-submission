
package com.hpe.usps.solavilibrary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LocationSummary extends GenericResponse {

    @SerializedName("locations")
    @Expose
    private List<Location> mLocations = new ArrayList<>();

    /**
     * 
     * @return
     *     The mLocations
     */
    public List<Location> getLocations() {
        return mLocations;
    }

    /**
     * 
     * @param locations
     *     The mLocations
     */
    public void setLocations(List<Location> locations) {
        this.mLocations = locations;
    }

}
