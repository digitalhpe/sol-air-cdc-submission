package com.hpe.usps.solavilibrary;

/**
 * Created by chishobr
 */
public class Constants {
    //AirNow API key can be acquired here: https://docs.airnowapi.org
    public static final String AIRNOW_API_KEY = "AIRNOW_API_KEY";
    public static final String GOOGLE_GEO_KEY = "GOOGLE_GEO_KEY";
    public static final String PARAMETER_NAME_OZONE = "O3";

    private Constants() {}
}
