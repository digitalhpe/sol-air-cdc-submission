package com.hpe.solavilibrary;

import com.google.gson.Gson;
import com.hpe.solavilibrary.rest.modules.TestModule;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;
import com.hpe.usps.solavilibrary.helpers.LocationHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;
import com.hpe.usps.solavilibrary.models.CurrentObservation;
import com.hpe.usps.solavilibrary.models.Location;
import com.hpe.usps.solavilibrary.models.LocationSummary;
import com.hpe.usps.solavilibrary.models.UVDaily;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.PlacesService;
import com.hpe.usps.solavilibrary.rest.SolaviService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import retrofit.client.Client;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import rx.Observable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.when;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class LocationHelperTest {

    @Inject
    protected Client mClient;

    @Inject
    AirNowService mAirNowService;

    @Inject
    EPAService mEPAService;

    @Inject
    PlacesService mPlacesService;

    @Inject
    SolaviService mSolaviService;

    @Inject
    GeoHelper mGeoHelper;

    @Inject
    PreferenceHelper mPreferenceHelper;

    @Before
    public void setUp() throws Exception {
        Injector.init(new TestModule());
        Injector.inject(this);
    }

    @Test
    public void testGetIndexesForLocation() throws IOException {
        when(mGeoHelper.getZipFromLatLong(anyDouble(), anyDouble())).thenReturn(Observable.just("48439"));

        List<CurrentObservation> currentObservationList = new ArrayList<>();

        CurrentObservation currentObservation = new CurrentObservation();
        currentObservation.setParameterName("O3");
        currentObservation.setAQI(5);

        currentObservationList.add(currentObservation);

        String responseStringAirQuality = new Gson().toJson(currentObservationList);

        Response responseAirQuality = createResponseWithCodeAndJson(HttpURLConnection.HTTP_OK, responseStringAirQuality);

        List<UVDaily> uvDailyList = new ArrayList<>();

        UVDaily uvDaily = new UVDaily();
        uvDaily.setUvIndex(3);

        uvDailyList.add(uvDaily);

        String responseStringUVDaily = new Gson().toJson(uvDailyList);

        Response responseUVDaily = createResponseWithCodeAndJson(HttpURLConnection.HTTP_OK, responseStringUVDaily);

        when(mClient.execute(Matchers.<Request>anyObject())).thenReturn(responseUVDaily, responseAirQuality);

        LocationHelper locationHelper = new LocationHelper(mPreferenceHelper, mGeoHelper, mEPAService, mAirNowService, mSolaviService, mPlacesService);

        Location location;
        location = locationHelper.getIndexesForLocation(42.6170659, -83.2613044)
                .toBlocking()
                .first();

        assertTrue(5 == location.getOzone());
    }

    @Test
    public void testGetLocationsZeroResults() throws IOException {
        LocationSummary locationSummary = new LocationSummary();

        String responseStringLocationSummary = new Gson().toJson(locationSummary);
        Response responseLocationSummary = createResponseWithCodeAndJson(HttpURLConnection.HTTP_OK, responseStringLocationSummary);

        when(mClient.execute(Matchers.<Request>anyObject())).thenReturn(responseLocationSummary);

        LocationHelper locationHelper = new LocationHelper(mPreferenceHelper, mGeoHelper, mEPAService, mAirNowService, mSolaviService, mPlacesService);

        LocationSummary summary = locationHelper.getLocations("123")
                .toBlocking()
                .first();

        assertTrue(summary.getLocations().isEmpty());
    }

    @Test
    public void testGetLocationsOneResult() throws IOException {
        LocationSummary locationSummary = new LocationSummary();

        Location locationNonFavorite = new Location();
        locationNonFavorite.setLocationid("1");

        locationSummary.getLocations().add(locationNonFavorite);

        String responseStringLocationSummary = new Gson().toJson(locationSummary);
        Response responseLocationSummary = createResponseWithCodeAndJson(HttpURLConnection.HTTP_OK, responseStringLocationSummary);

        when(mClient.execute(Matchers.<Request>anyObject())).thenReturn(responseLocationSummary);

        LocationHelper locationHelper = new LocationHelper(mPreferenceHelper, mGeoHelper, mEPAService, mAirNowService, mSolaviService, mPlacesService);

        LocationSummary summary = locationHelper.getLocations("123")
                .toBlocking()
                .first();

        assertTrue(1 == summary.getLocations().size());
    }

    @Test
    public void testGetLocationsNoFavorite() throws IOException {
        LocationSummary locationSummary = new LocationSummary();

        Location locationFavorite = new Location();
        locationFavorite.setLocationid("1");

        Location locationNonFavorite = new Location();
        locationNonFavorite.setLocationid("2");

        locationSummary.getLocations().add(locationFavorite);
        locationSummary.getLocations().add(locationNonFavorite);

        String responseStringLocationSummary = new Gson().toJson(locationSummary);
        Response responseLocationSummary = createResponseWithCodeAndJson(HttpURLConnection.HTTP_OK, responseStringLocationSummary);

        when(mClient.execute(Matchers.<Request>anyObject())).thenReturn(responseLocationSummary);

        LocationHelper locationHelper = new LocationHelper(mPreferenceHelper, mGeoHelper, mEPAService, mAirNowService, mSolaviService, mPlacesService);

        LocationSummary summary = locationHelper.getLocations("123")
                .toBlocking()
                .first();

        assertEquals("1", summary.getLocations().get(0).getLocationid());
    }

    @Test
    public void testGetLocationsFavoriteOrderedFirst() throws IOException {
        LocationSummary locationSummary = new LocationSummary();

        Location locationFavorite = new Location();
        locationFavorite.setLocationid("1");

        Location locationNonFavorite = new Location();
        locationNonFavorite.setLocationid("2");

        locationSummary.getLocations().add(locationFavorite);
        locationSummary.getLocations().add(locationNonFavorite);

        String responseStringLocationSummary = new Gson().toJson(locationSummary);
        Response responseLocationSummary = createResponseWithCodeAndJson(HttpURLConnection.HTTP_OK, responseStringLocationSummary);

        when(mClient.execute(Matchers.<Request>anyObject())).thenReturn(responseLocationSummary);

        LocationHelper locationHelper = new LocationHelper(mPreferenceHelper, mGeoHelper, mEPAService, mAirNowService, mSolaviService, mPlacesService);

        LocationSummary summary = locationHelper.getLocations("123")
                .toBlocking()
                .first();

        assertEquals("1", summary.getLocations().get(0).getLocationid());
    }

    @Test
    public void testGetLocationsFavoriteOrderedSecond() throws IOException {
        LocationSummary locationSummary = new LocationSummary();

        Location locationNonFavorite = new Location();
        locationNonFavorite.setLocationid("1");

        Location locationFavorite = new Location();
        locationNonFavorite.setLocationid("2");

        locationSummary.getLocations().add(locationNonFavorite);
        locationSummary.getLocations().add(locationFavorite);

        String responseStringLocationSummary = new Gson().toJson(locationSummary);
        Response responseLocationSummary = createResponseWithCodeAndJson(HttpURLConnection.HTTP_OK, responseStringLocationSummary);

        when(mClient.execute(Matchers.<Request>anyObject())).thenReturn(responseLocationSummary);

        LocationHelper locationHelper = new LocationHelper(mPreferenceHelper, mGeoHelper, mEPAService, mAirNowService, mSolaviService, mPlacesService);

        LocationSummary summary = locationHelper.getLocations("123")
                .toBlocking()
                .first();

        assertEquals("2", summary.getLocations().get(0).getLocationid());
    }

    @Test
    public void testCreateLocationNullLocationId() throws IOException {
        LocationHelper locationHelper = new LocationHelper(mPreferenceHelper, mGeoHelper, mEPAService, mAirNowService, mSolaviService, mPlacesService);

        List<Location> locationList = new ArrayList<>();

        Location locationOne = new Location();
        locationOne.setLocationid("123");

        Location locationTwo = new Location();
        locationTwo.setLocationid("321");

        locationList.add(locationOne);
        locationList.add(locationTwo);

        Location location = locationHelper.createLocation(null, 0.0, 0.0, locationList);

        assertEquals("0", location.getLocationid());
    }

    @Test
    public void testCreateLocationZeroLocationId() throws IOException {
        LocationHelper locationHelper = new LocationHelper(mPreferenceHelper, mGeoHelper, mEPAService, mAirNowService, mSolaviService, mPlacesService);

        List<Location> locationList = new ArrayList<>();

        Location locationOne = new Location();
        locationOne.setLocationid("123");

        Location locationTwo = new Location();
        locationTwo.setLocationid("321");

        locationList.add(locationOne);
        locationList.add(locationTwo);

        Location location = locationHelper.createLocation("0", 0.0, 0.0, locationList);

        assertEquals("0", location.getLocationid());
    }

    @Test
    public void testCreateLocationExistingLocationId() throws IOException {
        LocationHelper locationHelper = new LocationHelper(mPreferenceHelper, mGeoHelper, mEPAService, mAirNowService, mSolaviService, mPlacesService);

        List<Location> locationList = new ArrayList<>();

        Location locationOne = new Location();
        locationOne.setLocationid("123");
        locationOne.setName("foo");

        Location locationTwo = new Location();
        locationTwo.setLocationid("321");

        locationList.add(locationOne);
        locationList.add(locationTwo);

        Location location = locationHelper.createLocation("123", 0.0, 0.0, locationList);

        assertEquals("123", location.getLocationid());
        assertEquals("foo", location.getName());
    }

    private Response createResponseWithCodeAndJson(int responseCode, String json) {
        return new Response("", responseCode, "nothing", Collections.EMPTY_LIST, new TypedByteArray("application/json", json.getBytes()));
    }
}