package com.hpe.solavilibrary;

import dagger.ObjectGraph;

/**
 * Created by Andy Jones
 */
public final class Injector {
    private static ObjectGraph objectGraph = null;

    private Injector() {

    }

    public static synchronized void init(Object... rootModules) {
        objectGraph = objectGraph == null ? ObjectGraph.create(rootModules) : objectGraph.plus(rootModules);
    }

    public static void inject(final Object target) {
        objectGraph.inject(target);
    }

    public static void add(Object... object) {
        objectGraph = ObjectGraph.create(object);
    }
}
