package com.hpe.solavilibrary.rest.modules;

import com.hpe.solavilibrary.LocationHelperTest;
import com.hpe.usps.solavilibrary.helpers.GeoHelper;
import com.hpe.usps.solavilibrary.helpers.PreferenceHelper;
import com.hpe.usps.solavilibrary.rest.AirNowService;
import com.hpe.usps.solavilibrary.rest.EPAService;
import com.hpe.usps.solavilibrary.rest.SolaviService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.client.Client;

import static org.mockito.Mockito.mock;

/**
 * Created by Andy Jones
 */

@Module(
        injects = {
                LocationHelperTest.class
        },
        overrides = true,
        library = true,
        complete = false

)

public class TestModule {

    private static final String SERVICE_ENDPOINT = "NOT_A_REAL_ADDRESS";

    @Provides
    @Singleton
    Client provideMockClient() {
        return mock(Client.class);
    }

    @Provides
    SolaviService providesSolaviService(Client client) {
        return new RestAdapter.Builder()
                .setEndpoint(SERVICE_ENDPOINT)
                .setClient(client)
                .build()
                .create(SolaviService.class);
    }

    @Provides
    EPAService providesEpaService(Client client) {
        return new RestAdapter.Builder()
                .setEndpoint(SERVICE_ENDPOINT)
                .setClient(client)
                .build()
                .create(EPAService.class);
    }

    @Provides
    AirNowService providesAirNowService(Client client) {
        return new RestAdapter.Builder()
                .setEndpoint(SERVICE_ENDPOINT)
                .setClient(client)
                .build()
                .create(AirNowService.class);
    }

    @Provides
    PreferenceHelper providesPreferenceHelper() {
        return mock(PreferenceHelper.class);
    }

    @Provides
    GeoHelper providesGeoHelper() {
        return mock(GeoHelper.class);
    }
}