#CDC Challenge Submission: Sol•Air

Sol•Air is an app for health and environment conscious individuals who would like a quick view of the current environmental quality for their favorite locations and be alerted when air quality or UVI is in unhealthy levels.

The user can save multiple locations by dropping a pin on a visual map or use their current location determined by the device’s GPS. 

Sol•Air’s location detail view provides additional information which educates and advises the user about the relevant statistics and historical data related to environmental and lifestyle quality. 

[The installable APK file](https://bitbucket.org/digitalhpe/sol-air-cdc-submission/src/aefefd1f88575e0ec870ed69360b7561bd97c0ef/cdc-challenge-solair.apk?at=master&fileviewer=file-view-default)

[Demonstration Video](https://youtu.be/SU6uneR30zY)

[The slides describing the submission](https://bitbucket.org/digitalhpe/sol-air-cdc-submission/src/aefefd1f88575e0ec870ed69360b7561bd97c0ef/cdc-challenge-solair.apk?at=master&fileviewer=file-view-default)

[Web services source code](https://bitbucket.org/digitalhpe/sol-air-cdc-service)